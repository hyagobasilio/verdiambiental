<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Solicitacao extends Model
{
    protected $table = 'solicitacoes';
    
    protected $fillable = ['titulo', 'descricao', 'prioridade', 'user_id', 'comentario_id', 'status'];    
    
    /**
	 * The rules for email field, automatic validation.
	 *
	 * @var array
	*/
	private $rules = array(
			
	);

	public function usuario()
	{
		return $this->belongsTo('App\User');
	}
}
