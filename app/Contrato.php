<?php

namespace App;
use App\Helpers\Utils;
use Illuminate\Database\Eloquent\Model;

class Contrato extends Model
{
    protected $table = 'contratos';
    
    protected $fillable = [
	    'descricao',
	    'concessao_recipiente',
	    'prazo_vigencia',
	    'segunda',
	    'terca',
	    'quarta',
	    'quinta',
	    'sexta',
	    'sabado',
	    'material', 'quantidade', 'tipo_contrato', 'valor', 'qtd_remocoes_sema', 
	    'dias', 
	    'cliente_id',
	    'data',
	    'numero_contrato',
	    'responsavel',
	    'responsavel_cpf',
	    'valor_sujo'
    ];

    public static function getDias(Contrato $contrato)
	{
		$dias = [];
		if($contrato->segunda) {
			array_push($dias, 'Segunda');
		}
		if($contrato->terca) {
			array_push($dias, 'Terça');
		}
		if($contrato->quarta) {
			array_push($dias, 'Quarta');
		}
		if($contrato->quinta) {
			array_push($dias, 'Quinta');
		}
		if($contrato->sexta) {
			array_push($dias, 'Sexta');
		}
		if($contrato->sabado) {
			array_push($dias, 'Sábado');
		}
		return $dias;
	}
    
    /**
	 * The rules for email field, automatic validation.
	 *
	 * @var array
	*/
	private $rules = array(
			
	);

	public function getValorSujoAttribute($value)
    {
        return number_format($value, 2, ',', '.');
    }
    public function setValorSujoAttribute($value)
    {
        $this->attributes['valor_sujo'] = Utils::moeda_para_bd($value);
    }
    public function getValorAttribute($value)
    {
        return number_format($value, 2, ',', '.');
    }

    public function setValorAttribute($value)
    {
        $this->attributes['valor'] = Utils::moeda_para_bd($value);
    }

	public function cliente()
	{
		return $this->belongsTo('App\Cliente');
	}
	public function enderecos(){
		return $this->hasMany('App\Endereco');
	}
	public function getDataAttribute($value)
	{
	 return Utils::data_to_br($value);
	}
	public function setDataAttribute($value)
	{
	  $this->attributes['data'] = Utils::data_to_bd($value);
	}
}
