<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\Utils;

class Movimentacao extends Model
{
    protected $table = 'movimentacoes_fc';
    protected $fillable = ['data_mov', 'valor', 'tipo_mov', 'item_id'];    


    public function item()
    {
    	return $this->belongsTo('App\ItemMovimentacao', 'item_id');
    }
    

    public function getValorAttribute($value)
    {
        return number_format($value, 2, ',', '.');
    }
    public function setValorAttribute($value)
    {
        $this->attributes['valor'] = Utils::moeda_para_bd($value);
    }

    public function getDataMovAttribute($value)
    {
        return Utils::data_to_br($value);

    }
    public function setDataMovAttribute($value)
    {
       return $this->attributes['data_mov'] = Utils::data_to_bd($value);
    }
    
}
