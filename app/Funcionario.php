<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\Utils;

class Funcionario extends Model
{
    protected $table = 'funcionarios';
    public $timestamps = false;
    
    protected $fillable = [
        'nome',
    	'cargo',
    	'matricula',
    	'telefone',
    	'email',
    	'data_nascimento',
    	'rg',
    	'cpf',
    	'data_admissao',
    	'numero_cnh',
    	'categoria_cnh',
        'validade',
    	'cargo_id',
    	'tipo' // ajudante ou motorista 
    	];

    public function cargo()
    {
        return $this->belongsTo('App\Cargo');
    }

    public function getDataNascimentoAttribute($value)
    {
        return Utils::data_to_br($value);
    }

    public function setDataNascimentoAttribute($value)
    {
        $this->attributes['data_nascimento'] = Utils::data_to_bd($value);
    }

    public function getDataAdmissaoAttribute($value)
    {
        return Utils::data_to_br($value);
    }

    public function setDataAdmissaoAttribute($value)
    {
        $this->attributes['data_admissao'] = Utils::data_to_bd($value);
    }
    public function getValidadeAttribute($value)
    {
        return Utils::data_to_br($value);
    }

    public function setValidadeAttribute($value)
    {
        $this->attributes['validade'] = Utils::data_to_bd($value);
    }
}
