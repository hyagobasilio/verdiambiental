<?php

namespace App;
use App\Helpers\Utils;
use Illuminate\Database\Eloquent\Model;

class OrdemServico extends Model
{
   	protected $table = 'ordem_servicos';

   	protected $fillable = [
        'solicitante_id',
        'ajudante_id',
        'motorista_id',
        'caminhao_id',

        'servico',
        'status',
        'tipo',
        'data_abertura',
        'prazo',
        'observacao',
        
        'numero_cupom',
        'manifesto',
        'numero_ticket',
        'peso'
   	];


    public function getPesoAttribute($value)
    {
        return number_format($value, 2, ',', '.');
    }
    public function setPesoAttribute($value)
    {
        $this->attributes['peso'] = Utils::moeda_para_bd($value);
    }

    public function getDataAberturaAttribute($value)
    {
        return Utils::data_to_br($value);
    }
    public function setDataAberturaAttribute($value)
    {
        $this->attributes['data_abertura'] = Utils::data_to_bd($value);
    }
    public function setAjudanteIdAttribute($value)
    {
        $this->attributes['ajudante_id'] = empty($value) ? null : $value;
    }
    public function getPrazoAttribute($value)
    {
        return Utils::data_to_br($value);
    }
    public function setPrazoAttribute($value)
    {
        $this->attributes['prazo'] = Utils::data_to_bd($value);
    }

    /*
     * Relacionamentos
     */
    public function itens()
    {
        return $this->hasMany('App\OrdemServicoItem', 'os_id');
    }

    public function solicitante()
    {
        return $this->belongsTo('App\User', 'solicitante_id');
    }

    public function motorista()
    {
        return $this->belongsTo('App\Funcionario', 'motorista_id');
    }
    public function ajudante()
    {
        return $this->belongsTo('App\Funcionario', 'ajudante_id');
    }
    public function caminhao()
    {
        return $this->belongsTo('App\Frota', 'caminhao_id');
    }
    
}
