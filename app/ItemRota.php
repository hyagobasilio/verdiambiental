<?php

namespace App;
use App\Helpers\Utils;
use Illuminate\Database\Eloquent\Model;

class ItemRota extends Model
{
    protected $fillable = [
	    'status',
	    'numero_cupom',
	    'manifesto',
	    'numero_ticket',
	    'peso',
	    'data_rota', 
	    'ocorrencia_id', 
	    'finalizado',
	    'estado',
	    'tipo',
	    'preco'
    ];
    
    protected $table = 'itens_rota';

    public $timestamps = false;

    public function getPesoAttribute($value)
	{
		return number_format($value, 2, ',', '.');
	}
	public function setPesoAttribute($value)
	{
		$this->attributes['peso'] = Utils::moeda_para_bd($value);
	}

	public function getDataRotaAttribute($value)
	{
	 return Utils::data_to_br($value);
	}
	public function setDataRotaAttribute($value)
	{
	  $this->attributes['data_rota'] = Utils::data_to_bd($value);
	}

	public function getPrecoAttribute($value)
    {
        return number_format($value, 2, ',', '.');
    }

    public function setPrecoAttribute($value)
    {
        $this->attributes['preco'] = Utils::moeda_para_bd($value);
    }
}
