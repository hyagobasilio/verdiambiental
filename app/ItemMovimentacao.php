<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemMovimentacao extends Model {

	protected $table = 'item_mov';
	protected $fillable = ['titulo', 'categoria_id'];

	public function categoria()
	{
		return $this->belongsTo('App\CategoriaMovimentacao');
	}

}