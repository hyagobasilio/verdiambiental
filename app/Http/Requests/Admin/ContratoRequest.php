<?php namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class ContratoRequest extends FormRequest {


	public function messages() 
	{
		return [
			'descricao.required' => 'Informe uma descrição para o contrato!',
			'descricao.min' => 'Informe uma descrição com no minimo 3 caracteres!',			
			'valor.required'	 => 'Informe um valor para o contrato!'
		];
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
            'descricao' => 'required|min:3',
            'valor' 	=> 'required'
		];
	}

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

}
