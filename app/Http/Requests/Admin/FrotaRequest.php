<?php namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class FrotaRequest extends FormRequest {


	public function messages() 
	{
		return [
			'required' => 'Preencha este campo!',
			'placa.min' => 'A placa deve conter 7 caracteres',			
			'tipo.required'	 => 'Informe o tipo do caminhão!'
		];
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
            'caminhao' => 'required|min:3',
            'placa' => 'required|min:6',
            'tipo' 	=> 'required'
		];
	}

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

}
