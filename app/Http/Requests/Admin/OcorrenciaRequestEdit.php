<?php 
namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class OcorrenciaRequestEdit extends FormRequest {

	public function messages() {
		return [
			    'required' => 'O campo :attribute é requerido.',
			    'unique' => 'Já existe um cadastro aberto com este :attribute'
			];
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'endereco' => 'required',
			'data' => 'required',
			'bairro' => 'required',
			//'numero_container' => 'required|unique:ocorrencias,numero_container,NULL,id,status,1',
			//'email' => 'unique:users,email_address,NULL,id,account_id,1'
		];
	}


	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

}
