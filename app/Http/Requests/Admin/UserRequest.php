<?php namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest {

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
            'name' 		=> 'required|min:3',
            'username' 		=> 'required|unique:users|min:3',
            'email' 	=> 'required|email|unique:users',
            'password' 	=> 'required|confirmed|min:5',
		];
	}


	public function messages() {
		return [
		'name.required'     => 'O campo nome é obrigatório.',
		'username.required'     => 'O campo nickname é obrigatório.',
		'email.required'     => 'O campo email é obrigatório.',
		'password.required'     => 'O campo senha é obrigatório.',
		'senha.required'     => 'O campo senha é obrigatório.',
		'password.min'          => 'O campo Senha deve ter pelo menos 5 caracteres.',
		'username.min'          => 'O campo Nickname deve ter pelo menos 3 caracteres.',
		'email.unique'       => 'Já existe uma conta com este email!',
		'username.unique'       => 'Este Nickname já está em uso!'
		];
	}

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

}
