<?php 
namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class EnderecoRequest extends FormRequest {

	public function messages() {
		return [
			    'required' => 'O campo :attribute é requerido.'
			];
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'contrato_id' => 'required',
			'cliente_id' => 'required',
		];
	}


	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

}
