<?php namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class FuncionarioRequest extends FormRequest {


	public function messages() 
	{
		return [
			'required' => 'Preencha este campo!'
		];
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
            'nome' => 'required',
            'tipo' => 'required'
		];
	}

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

}
