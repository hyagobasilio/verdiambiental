    <?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::model('endereco', 'App\Endereco');
Route::model('itemrota', 'App\ItemRota');
Route::model('ordemservicoitem', 'App\OrdemServicoItem');
Route::model('cliente', 'App\Cliente');
Route::model('funcionario', 'App\Funcionario');
Route::model('frota', 'App\Frota');
Route::model('CategoriaMovimentacao', 'App\CategoriaMovimentacao');
Route::model('itemmovimentacao', 'App\ItemMovimentacao');
Route::model('cargo', 'App\Cargo');
Route::model('movimentacao', 'App\Movimentacao');
Route::model('container', 'App\Container');
Route::model('ocorrencia', 'App\Ocorrencia');
Route::model('ordemservico', 'App\OrdemServico');
Route::model('solicitacao', 'App\Solicitacao');
Route::model('contrato', 'App\Contrato');
Route::model('user', 'App\User');
Route::get('/', function () {
    if (Auth::check()) {
        return view('admin.ocorrencias.index');
    }
    return view('auth.login');
    });

    Route::controllers([
        'auth' => 'Auth\AuthController',
        'password' => 'Auth\PasswordController',
    ]);
    /*Route::get('admin', function() {
    return view('admin_template');
});*/
Route::get('home', function() {
    return redirect('auth/login');
});



Route::group(['prefix' => 'admin', 'middleware' => 'auth', 'namespace' => 'Admin'], function() {
    Route::pattern('id', '[0-9]+');
    Route::pattern('id2', '[0-9]+');
    # Admin Dashboard
    Route::get('dashboard', 'DashboardController@index');
    
    Route::get('cadastro', function(){
        return view('admin.users.create_edit');
    });

    # Cidades
    Route::get('cidades', 'ClienteController@getCidades');
    Route::get('pdf', function() {
        return date('H:i:s');
    });

    # Endereco
    Route::resource('endereco', 'EnderecoController');
    Route::get('endereco/{endereco}/delete', 'EnderecoController@delete');
    Route::get('endereco/cliente/{idCliente}', 'EnderecoController@getEnderecosByCliente');

    # Funcionario
    Route::resource('funcionario', 'FuncionarioController');
    Route::put('funcionario/{funcionario}/edit', 'FuncionarioController@update');
    Route::get('funcionario/{funcionario}/delete', 'FuncionarioController@delete');

    # CategoriaMovimentacao
    Route::put('categoriamovimentacao/{categoriamovimentacao}/edit', 'CategoriaMovimentacaoController@update');
    Route::get('categoriamovimentacao/{categoriamovimentacao}/delete', 'CategoriaMovimentacaoController@delete');
    Route::get('categoriamovimentacao/data', 'CategoriaMovimentacaoController@data');
    Route::get('categoriamovimentacao/all', 'CategoriaMovimentacaoController@getCategorias');
    Route::resource('categoriamovimentacao', 'CategoriaMovimentacaoController');

    # Item Movimentacao
    Route::put('itemmovimentacao/{itemmovimentacao}/edit', 'ItemMovimentacaoController@update');
    Route::get('itemmovimentacao/{itemmovimentacao}/delete', 'ItemMovimentacaoController@delete');
    Route::get('itemmovimentacao/data', 'ItemMovimentacaoController@data');
    Route::get('itemmovimentacao/categoria/{idCategoria}', 'ItemMovimentacaoController@getItensByCategoria');
    Route::resource('itemmovimentacao', 'ItemMovimentacaoController');

    # Cargo
    Route::resource('cargo', 'CargoController');
    Route::put('cargo/{cargo}/edit', 'CargoController@update');
    Route::get('cargo/{cargo}/delete', 'CargoController@delete');
    Route::get('cargos/data', 'CargoController@data');

    # Movimentacao
    Route::resource('movimentacao', 'MovimentacaoController');
    Route::put('movimentacao/{movimentacao}/edit', 'MovimentacaoController@update');
    Route::get('movimentacao/{movimentacao}/delete', 'MovimentacaoController@delete');

    # Frota
    Route::resource('frota', 'FrotaController');
    Route::put('frota/{frota}/edit', 'FrotaController@update');
    Route::get('frota/{frota}/delete', 'FrotaController@delete');

    # Container para OSI
    Route::get('container/disponiveis', 'ContainerController@getContainersDisponiveis');
    Route::get('container/locado/{idCliente}', 'ContainerController@getContainersLocadoByCliente');
    # Container
    Route::get('container/dados', 'ContainerController@dados');
    Route::resource('container', 'ContainerController');
    Route::put('container/{container}/edit', 'ContainerController@update');
    Route::get('container/{container}/delete', 'ContainerController@delete');

    # Contrato
    Route::resource('contrato', 'ContratoController');
    Route::get('contrato/{contrato}/pdf', 'ContratoController@pdf');
    Route::get('contrato/{contrato}/delete', 'ContratoController @delete');
    Route::put('contrato/{contrato}/edit', 'ContratoController@update');

    # Solicitacao
    Route::get('solicitacao/{idSolicitacao}/comentarios', 'SolicitacaoController@comentarios');
    Route::post('solicitacao/{idSolicitacao}/comentar', 'SolicitacaoController@salvarComentario');
    Route::get('solicitacao', 'SolicitacaoController@index');
    Route::get('solicitacao/{solicitacao}/delete', 'SolicitacaoController@delete');
    Route::resource('solicitacao', 'SolicitacaoController');
    Route::put('solicitacao/{solicitacao}/edit', 'SolicitacaoController@update');
    Route::get('solicitacao/{solicitacao}/remover-comentario', 'SolicitacaoController@removerComentario');

    # Item de Rotas
    Route::get('itemrota/{idOS}/create', 'ItemRotaController@create');
    Route::get('itemrota/{itemrota}/edit', 'ItemRotaController@edit');
    Route::put('itemrota/{itemrota}', 'ItemRotaController@update');
    Route::post('itemrota', 'ItemRotaController@store');
    Route::get('itemrota/data/{idOS}', 'ItemRotaController@data');
    Route::get('itemrota/{itemrota}/delete', 'ItemRotaController@delete');
    Route::delete('itemrota/{itemrota}', 'ItemRotaController@destroy');

    # Ordem Servico Item
    Route::get('ordemservicoitem/data/{idOS}', 'OrdemServicoItemController@data');
    Route::get('ordemservicoitem/{idOS}/create', 'OrdemServicoItemController@create');
    Route::get('ordemservicoitem/{ordemservicoitem}/edit', 'OrdemServicoItemController@edit');
    Route::put('ordemservicoitem/{ordemservicoitem}', 'OrdemServicoItemController@update');
    Route::post('ordemservicoitem', 'OrdemServicoItemController@store');
    Route::get('ordemservicoitem/{ordemservicoitem}/delete', 'OrdemServicoItemController@delete');
    Route::get('ordemservicoitem/endereco/cliente/{idCliente}', 'OrdemServicoItemController@getEnderecosByCliente');
    Route::delete('ordemservicoitem/{ordemservicoitem}', 'OrdemServicoItemController@destroy');
    
    # Order Serviço
    Route::resource('ordemservico', 'OrdemServicoController');
    Route::put('ordemservico/{ordemservico}/edit', 'OrdemServicoController@update');
    Route::get('ordemservico/{ordemservico}/delete', 'OrdemServicoController@delete');
    

    # Ocorrencia
    Route::get('ocorrencia/container', 'OcorrenciaController@getContainer');
    Route::get('ocorrencia/filtro/', 'OcorrenciaController@index');
    Route::get('ocorrencia/alertas', 'OcorrenciaController@getAlertas');
    Route::get('ocorrencia/desativaralertas', 'OcorrenciaController@dasativarAlertas');
    Route::get('ocorrencia/data', 'OcorrenciaController@data');
    Route::get('ocorrencia/{ocorrencia}/show', 'OcorrenciaController@show');
    Route::get('ocorrencia/{ocorrencia}/edit', 'OcorrenciaController@edit');
    Route::get('ocorrencia/{ocorrencia}/delete', 'OcorrenciaController@delete');
    Route::resource('ocorrencia', 'OcorrenciaController');
    Route::put('ocorrencia/{ocorrencia}/edit', 'OcorrenciaController@update');
    Route::get('ocorrencia/{ocorrencia}/pdf', 'OcorrenciaController@getPdf');
    
    # Cliente
    Route::get('cliente/{cliente}/pdf', 'ClienteController@pdf');
    Route::get('cliente/dados', 'ClienteController@getClientes');
    Route::get('cliente/dados/os', 'ClienteController@getClientesParaOS');
    Route::get('cliente/data', 'ClienteController@data');
    Route::get('cliente/datatable', 'ClienteController@data');
    Route::get('cliente/{cliente}/show', 'ClienteController@show');
    Route::get('cliente/{cliente}/edit', 'ClienteController@edit');
    Route::get('cliente/{cliente}/delete', 'ClienteController@delete');
    Route::resource('cliente', 'ClienteController');

    # Users
    Route::get('user', 'UserController@index');
    Route::get('user/create', 'UserController@getCreate');
    Route::post('user', 'UserController@postCreate');
    Route::get('user/{user}/edit', 'UserController@getEdit');
    Route::post('user/{user}/edit', 'UserController@postEdit');
    Route::get('user/{user}/delete', 'UserController@getDelete');
    Route::post('user/{user}/delete', 'UserController@postDelete');
    Route::get('user/data', 'UserController@data');
    //Route::resource('user', 'UserController');




});