<?php 
namespace App\Http\Controllers\Admin;

use Datatables;
use App\Ocorrencia;
use App\Helpers\Utils;
use App\Cidade;
use App\Funcionario;
use App\Cliente;
use App\Container;
use App\ItemRota;
use App\Services\OcorrenciaService;
use App\Http\Controllers\AdminController;
use App\Http\Requests\Admin\OcorrenciaRequest;
use App\Http\Requests\Admin\OcorrenciaRequestEdit;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use DB;
use PDF;
use App;    

class OcorrenciaController extends AdminController
{

    private $service;

    public function __construct(OcorrenciaService $service)
    {
        $this->service = $service;
        //view()->share('type', 'ocorrencia');
    }
    public function getContainer()
    {
       return $this->service->getContainer();
    }
    public function pdfBranco(Ocorrencia $os)
    {
        return "HYAGO BASÍLIO";
        $database = \Config::get('database.connections.mysql');
        $output = public_path() . '/reports/'.time().'_verdi';
        
        $ext = "pdf";
       
        $pasta_reports = public_path('reports/os/');
 
        \JasperPHP::process(
            public_path() . '/reports/branco.jasper', 
            $output, 
            array($ext),
            array(),
            $database,
            false,
            false
        )->execute();
 
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename='.time().'_verdi.'.$ext);
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-Length: ' . filesize($output.'.'.$ext));
        flush();
        readfile($output.'.'.$ext);
        unlink($output.'.'.$ext); // deletes the temporary file
        
        return redirect('/admin/ocorrencia');
       
    }

    public function getPdf(Ocorrencia $os)
    {
        $database = \Config::get('database.connections.mysql');
        $output = public_path() . '/reports/os/'.time().'_verdi';
        
        $ext = "pdf";
        $logo = public_path('reports/os/verdi-ambiental.png');
        $pasta_reports = public_path('reports/os/');
 
        \JasperPHP::process(
            public_path() . '/reports/os/os.jasper', 
            $output, 
            array($ext),
            array('os_id' => $os->id, 'caminho_publico' => $pasta_reports, 'logo' => $logo),
            $database,
            false,
            false
        )->execute();
 
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename='.time().'_verdi.'.$ext);
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-Length: ' . filesize($output.'.'.$ext));
        flush();
        readfile($output.'.'.$ext);
        unlink($output.'.'.$ext); // deletes the temporary file
        
        return redirect('/admin/ocorrencia');
       
    }

    public function pdf(Ocorrencia $os)
    {
        //dd($os);
        $cliente = Cliente::find($os->cliente_id);
        $os->cliente = $cliente;
        //$os->data =  Utils::data_to_br($os->data);
        $os->motorista = Funcionario::find($os->id_motorista);
       
        //return view('admin.ocorrencias.print',compact('os'));

        $pdf = PDF::loadView('admin.ocorrencias.print',compact('os'));
        return $pdf->download('invoice.pdf');
        //return $pdf->stream();
    }
   
    /*
    * Display a listing of the resource.
    *
    * @return Response
    */
    public function indexFilter()
    {
        // Show the page
        return view('admin.ocorrencias.index');
    }
 
    public function index(Request $request)
    {        
        $cliente = $request->get('cliente_search');
        $inicio = $request->get('inicio');
        $fim = $request->get('fim');

        $oss = DB::table('ocorrencias')
            ->select([
                'ocorrencias.id',
                'ocorrencias.servico',
                'ocorrencias.tipo',
                'ocorrencias.data',
                'clientes.nome',
                'clientes.nome_fantasia',
                'ocorrencias.valor_estimado',
                'ocorrencias.valor_final',
                'ocorrencias.numero_container',
                'ocorrencias.status'
                ])
            ->join('clientes', 'clientes.id', '=', 'ocorrencias.cliente_id' );
        
        if ($request->has('cliente_search')) 
        {
            $oss = $oss->where('nome', 'like' , $cliente.'%')
                ->orWhere('nome_fantasia', 'like' , $cliente.'%');
            /*$oss = $oss->whereExists(function ($query) {
                    $query->select(DB::raw(1))
                      ->from('clientes')
                      ->whereRaw('orders.user_id = users.id');
            })*/
        }
        if ($request->has('tipo')) 
        {
            $oss = $oss->where('ocorrencias.tipo', $request->get('tipo'));
        }
        if (($inicio) && ($fim))
        {
            $oss = $oss->whereBetween('ocorrencias.data', array(Utils::data_to_bd($inicio), Utils::data_to_bd($fim)));
        }
        if ($request->has('status'))
        {
            $oss = $oss->where('ocorrencias.status', $request->get('status'));
        }
        $oss = $oss->orderBy('ocorrencias.id', 'DESC')->paginate(15);

        

        // Show the page
        return view('admin.ocorrencias.indexFilter', compact('oss'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $lista = Funcionario::select(['id', 'nome'])->where('cargo_id',2)->get();
        $motoristas = array('0' => 'Selecione');
        foreach ($lista as $value) {
            $motoristas[$value->id] = $value->nome;
            
        }
       
   
        
        return view('admin.ocorrencias.create_edit', compact('motoristas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(OcorrenciaRequest $request)
    {
        $dados = $request->all();
        $this->service->create($dados);
        
        return redirect('/admin/ocorrencia');
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $ocorrencia
     * @return Response
     */
    public function edit(Ocorrencia $ocorrencia)
    {
        $lista = Funcionario::select(['id', 'nome'])->where('cargo_id',2)->get();
        $motoristas = array('0' => 'Selecione');
        foreach ($lista as $value) {
            $motoristas[$value->id] = $value->nome;
            
        }
        if(isset($ocorrencia->container_id)) {
            
            $ocorrencia->container = Container::findOrFail($ocorrencia->container_id);
        }
        //dd($ocorrencia);

        //$ocorrencia->data = Utils::data_to_br($ocorrencia->data);
        /*$itens = ItemRota::select('data_rota')->where('ocorrencia_id', $ocorrencia->id)
        ->orderBy('data_rota', 'ASC')->get();
        $rotas = [];
        foreach ($itens as $value) {
            $rotas[] = $value->data_rota;
        }*/

       // dd(json_encode($rotas));
       // $ocorrencia->rotas = json_encode($rotas);
        return view('admin.ocorrencias.create_edit', compact('ocorrencia', 'motoristas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $ocorrencia
     * @return Response
     */
    public function update(OcorrenciaRequestEdit $request, Ocorrencia $ocorrencia)
    {
        $dados = $request->all();
        //$dados['data'] = Utils::data_to_bd($dados['data']);
        // altera ocorrenciaa para concluído
        //$ocorrencia->status = 1;
        //dd($dados);
        //
        $ocorrencia->update($dados);

        /*ItemRota::where('ocorrencia_id', $ocorrencia->id)->delete();
        $itens = json_decode($dados['itens_rota']);
        $dados = [];
        foreach ($itens as $item) {
            $dados[] = ['data_rota' => $item, 'ocorrencia_id' => $ocorrencia->id];
        }
        ItemRota::insert($dados);*/
            
        return redirect('/admin/ocorrencia');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $ocorrencia
     * @return Response
     */

    public function delete(Ocorrencia $ocorrencia)
    {
        return view('admin.ocorrencias.delete', compact('ocorrencia'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $ocorrencia
     * @return Response
     */
    public function destroy(Ocorrencia $ocorrencia)
    {
        $ocorrencia->delete();
        return "deletado";
    }

    /**
     * Show a list of all the languages posts formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function data()
    {
        $selects = array(
            'ocorrencias.id', 
            'servico', 
            'tipo', 
            'data', 
            'cliente', 
            //DB::raw('CONCAT(clientes.nome, clientes.nome_fantasia) as cliente'), 
            'valor_estimado', 
            'valor_final',
            'numero_container',
            'status',
            'ocorrencias.cidade',
            );
        
        $ocorrencias = Ocorrencia::select($selects)
        //->join('clientes', 'clientes.id', '=', 'ocorrencias.cliente_id' )
        ->orderBy('ocorrencias.id', 'DESC');

        return Datatables::of($ocorrencias)
           ->remove_column('cidade')
            //->add_column('data', '{{{ App\Helpers\Utils::data_to_br($data[\'data\']) }}}')
            ->add_column('status', '@if($status == 1) <span class="label label-success">Concluído</span> @else <span class="label label-warning">Aberto</span>@endif')
            ->add_column('actions', '<a href="{{{ URL::to(\'admin/ocorrencia/\' . $id . \'/edit\' ) }}}" class="btn btn-success btn-xs " ><span class="glyphicon glyphicon-pencil"></span></a>
                    <a href="{{{ URL::to(\'admin/ocorrencia/\' . $id . \'/delete\' ) }}}" class="btn btn-xs btn-danger iframe"><span class="glyphicon glyphicon-trash"></span></a>
                ')
            ->make();
    }


    public function getAlertas() {
        $query = " SELECT o.tipo, o.data, o.cliente, o.prazo "
            ." FROM ocorrencias o"
            ." where DATEDIFF(now(), o.data) >= o.prazo "
            ." and o.mostrar = 1; ";
        $dados = DB::select($query);
        $info = ['qtd' => count($dados), 'dados' => $dados];
        return response()->json($info);
    }

    public function dasativarAlertas() {
        $dados = DB::select(" update ocorrencias "
            ."  ocorrencias  "
            ." set mostrar=0 "
            ." where DATEDIFF(now(), data) >= prazo "
            ." and mostrar = 1; ");
    }
    
}
