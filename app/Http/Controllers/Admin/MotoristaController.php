<?php 
namespace App\Http\Controllers\Admin;

use Datatables;
use App\Contrato;
use App\Motorista;
use App\Http\Controllers\AdminController;
use App\Http\Requests\Admin\ItemRotaRequest;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\MotoristaRequest;
use DB;
use PDF;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class MotoristaController extends AdminController
{
	

    public function index(Request $request)
    {
       
        $motoristas = new Motorista();

        if ($request->has('nome')) 
        {
            $motoristas = $motoristas->where('nome', 'like' , $request->get('nome').'%');
        }
        if ($request->has('cpf')) {
            $motoristas = $motoristas->where('cpf', 'like', $request->get('cpf').'%');
        }
        if ($request->has('tipo')) {
            $motoristas = $motoristas->where('tipo',  $request->get('tipo'));
        }
        $motoristas = $motoristas->paginate(15);

        return view('admin.motorista.index', compact('motoristas'));
    }

    public function show(Motorista $motorista)
    {
        return view('admin.motorista.show', compact('motorista'));
    }

	public function create()
	{
		
        return view('admin.motorista.create_edit');
    }

    public function edit(Motorista $motorista)
	{
        
        return view('admin.motorista.create_edit', compact('motorista'));
    }

    public function update(Request $request, Motorista $solicitacao)
    {
        $solicitacao->update($request->all());
        return redirect('admin/motorista');
    }

    public function store()
    {

    	Motorista::create(Input::all());
        return redirect('admin/motorista');
    }
 
    public function delete(Motorista $motorista)
    {
    	return view('admin.motorista.delete', compact('motorista'));
    }

    public function destroy(Motorista $motorista)
    {
    	$motorista->delete();
    }

}