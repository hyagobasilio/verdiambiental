<?php 
namespace App\Http\Controllers\Admin;

use Datatables;
use App\ItemMovimentacao;
use App\CategoriaMovimentacao;
use App\Ocorrencia;
use App\Http\Controllers\AdminController;
use App\Http\Requests\Admin\ItemMovimentacaoRequest;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;

class ItemMovimentacaoController extends AdminController
{

    public function __construct()
    {
        view()->share('type', 'itemmovimentacao');
    }
	
    public function getItensByCategoria($idCategoria)
    {
        
        $categorias = ItemMovimentacao::where('categoria_id', $idCategoria)->lists('id','titulo')->toArray();
        
        
        return response()->json($categorias);    
    }

    public function index()
    {
       
        return view('admin.item_movimentacao.index');
    }
	public function create()
	{
        $categorias = CategoriaMovimentacao::lists('titulo', 'id');
		
        return view('admin.item_movimentacao.create_edit', compact('categorias'));
    }

    public function edit(ItemMovimentacao $item)
	{
        $categorias = CategoriaMovimentacao::lists('titulo', 'id');
        return view('admin.item_movimentacao.create_edit', compact('item', 'categorias'));
    }

    public function update(ItemMovimentacaoRequest $request, ItemMovimentacao $item)
    {
        $item->update($request->all());
    }

    public function store(ItemMovimentacaoRequest $request)
    {
    	ItemMovimentacao::create($request->all());
    }
 
    public function delete(ItemMovimentacao $item)
    {
    	return view('admin.item_movimentacao.delete', compact('item'));
    }

    public function destroy(ItemMovimentacao $item_movimentacao)
    {
    	$item_movimentacao->delete();
    }
    public function data()
    {
         $selects = array(
            'item_mov.id', 
            'item_mov.titulo',
            'categorias_mov.titulo as categoria'

            );
            
        $items = ItemMovimentacao::select($selects)
        ->join('categorias_mov', 'categorias_mov.id', '=', 'categoria_id');

        return Datatables::of($items)
            
            ->add_column('actions', '<a href="{{{ url(\'admin/itemmovimentacao/\' . $id . \'/edit\' ) }}}" class="btn btn-success btn-xs iframe" ><span class="glyphicon glyphicon-pencil"></span></a>'
                    .'<a href="{{{ url(\'admin/itemmovimentacao/\' . $id . \'/delete\' ) }}}" class="btn btn-xs btn-success iframe"><span class="glyphicon glyphicon-trash"></span></a>'
                )
            ->make();
    }
}