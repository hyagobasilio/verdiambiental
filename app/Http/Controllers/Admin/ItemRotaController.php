<?php 
namespace App\Http\Controllers\Admin;

use Datatables;
use App\ItemRota;
use App\Ocorrencia;
use App\Http\Controllers\AdminController;
use App\Http\Requests\Admin\ItemRotaRequest;
use Illuminate\Support\Facades\Input;

class ItemRotaController extends AdminController
{
	

	public function create($idOS)
	{
		
        return view('admin.itemrota.create_edit', compact('idOS'));
    }

    public function edit(ItemRota $itemrota)
	{
		
        return view('admin.itemrota.create_edit', compact('itemrota'));
    }

    public function update(ItemRotaRequest $request, ItemRota $itemrota)
    {
        $itemrota->update($request->all());
    }

    public function store(ItemRotaRequest $request)
    {
    	ItemRota::create($request->all());
    }
 
    public function delete(ItemRota $itemrota)
    {
    	return view('admin.itemrota.delete', compact('itemrota'));
    }

    public function destroy(ItemRota $itemrota)
    {
    	$itemrota->delete();
    }
    /**
     * Show a list of all the languages posts formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function data($idOS)
    {
        $itemrotas = ItemRota::select(
            [
                'itens_rota.id',
                'itens_rota.data_rota',
                'itens_rota.tipo',
                'itens_rota.estado',
                'itens_rota.numero_cupom',
                'itens_rota.manifesto',
                'itens_rota.numero_ticket',
                'itens_rota.peso',
                'itens_rota.preco',
                'itens_rota.status'
            ])
        ->where('itens_rota.ocorrencia_id', $idOS);
        return Datatables::of($itemrotas)
        	->add_column('statuss', '@if($status == \'concluida\') <span class="label label-success">Concluída</span> @else <span class="label label-warning">Pendente</span>@endif')
            
            ->add_column('actions', '<a href="{{{ URL::to(\'admin/itemrota/\' . $id . \'/edit\' ) }}}" class="btn btn-xs btn-success btn-sm iframe" ><span class="glyphicon glyphicon-pencil"></span>  Editar</a>
                    <a href="{{{ URL::to(\'admin/itemrota/\' . $id . \'/delete\' ) }}}" class="btn btn-sm btn-danger btn-xs iframe"><span class="glyphicon glyphicon-trash"></span> Deletar</a>
                ')
            ->remove_column('status')
            ->remove_column('id')
            ->make();
    }

}