<?php 
namespace App\Http\Controllers\Admin;

use Datatables;
use App\Helpers\Utils;
use App\Movimentacao;
use App\CategoriaMovimentacao;
use App\ItemMovimentacao;
use App\Http\Controllers\AdminController;
use App\Http\Requests\Admin\MovimentacaoRequest;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;

class MovimentacaoController extends AdminController
{

    public function __construct()
    {
        
    }
	

    public function index(Request $request)
    {

        $categorias = CategoriaMovimentacao::lists('titulo', 'id')->toArray();
        $categorias = array('' => 'Selecione')+$categorias;
        
        $itens = ItemMovimentacao::lists('titulo', 'id')->toArray();
        $itens = array('' => 'Selecione')+$itens;
        
        $movimentacoes = Movimentacao::select([
            'movimentacoes_fc.id', 
            'movimentacoes_fc.valor',
            'movimentacoes_fc.tipo_mov',
            'movimentacoes_fc.data_mov',
            'categorias_mov.titulo AS categoria',
            'item_mov.titulo AS item'
            ])
        ->join('item_mov', 'movimentacoes_fc.item_id', '=', 'item_mov.id')
        ->join('categorias_mov', 'item_mov.categoria_id', '=', 'categorias_mov.id');

        if  ($request->has('categoria'))
        {
            $movimentacoes = $movimentacoes->where('categorias_mov.id', $request->get('categoria'));
        }

        if  ($request->has('item'))
        {
            $movimentacoes = $movimentacoes->where('movimentacoes_fc.item_id', $request->get('item'));
        }

        if  ($request->has('tipo'))
        {
            $movimentacoes = $movimentacoes->where('movimentacoes_fc.tipo_mov', $request->get('tipo'));
        }

        if ($request->has('inicio') && $request->has('fim'))
        {
            $movimentacoes = $movimentacoes->whereBetween('movimentacoes_fc.data_mov',
                [
                    Utils::data_to_bd($request->get('inicio')), 
                    Utils::data_to_bd($request->get('fim'))
                ]);
        }

        $movimentacoes = $movimentacoes->orderBy('movimentacoes_fc.data_mov', 'DESC')
        ->paginate(15);
       
        return view('admin.movimentacao.index', compact('movimentacoes', 'categorias', 'itens'));
    }
	public function create()
	{
		$categorias = CategoriaMovimentacao::lists('titulo', 'id')->toArray();
        $categorias = array('' => 'Selecione')+$categorias;
        return view('admin.movimentacao.create_edit', compact('categorias'));
    }

    public function edit(Movimentacao $movimentacao)
	{
        
		$categorias = CategoriaMovimentacao::lists('titulo', 'id')->toArray();
        $categorias = array('' => 'Selecione')+$categorias;
        
        return view('admin.movimentacao.create_edit', compact('movimentacao', 'categorias'));
    }

    public function update(MovimentacaoRequest $request, Movimentacao $movimentacao)
    {
        $movimentacao->update($request->all());
    }

    public function store(MovimentacaoRequest $request)
    {
    	Movimentacao::create($request->all());
    }
 
    public function delete(Movimentacao $movimentacao)
    {
    	return view('admin.movimentacao.delete', compact('movimentacao'));
    }

    public function destroy(Movimentacao $movimentacao)
    {
    	$movimentacao->delete();
    }
    
}