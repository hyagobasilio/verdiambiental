<?php 
namespace App\Http\Controllers\Admin;

use Datatables;
use App\Contrato;
use App\Funcionario;
use App\Http\Controllers\AdminController;
use App\Http\Requests\Admin\ItemRotaRequest;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\FuncionarioRequest;
use DB;
use PDF;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class FuncionarioController extends AdminController
{
	

    public function index(Request $request)
    {
       
        $funcionarios = new Funcionario();

        if ($request->has('nome')) 
        {
            $funcionarios = $funcionarios->where('nome', 'like' , $request->get('nome').'%');
        }
        if ($request->has('cpf')) {
            $funcionarios = $funcionarios->where('cpf', 'like', $request->get('cpf').'%');
        }
        if ($request->has('tipo')) {
            $funcionarios = $funcionarios->where('tipo',  $request->get('tipo'));
        }
        $funcionarios = $funcionarios->paginate(15);

        
        $cargos =  DB::table('cargos')->orderBy('descricao', 'asc')->lists('descricao','id');

        return view('admin.funcionario.index', compact('funcionarios','cargos'));
    }

    public function show(Funcionario $funcionario)
    {
        return view('admin.funcionario.show', compact('funcionario'));
    }

	public function create()
	{
		 $cargos =  DB::table('cargos')->orderBy('descricao', 'asc')->lists('descricao','id');
        return view('admin.funcionario.create_edit', compact('cargos'));
    }

    public function edit(Funcionario $funcionario)
	{
        $cargos =  DB::table('cargos')->orderBy('descricao', 'asc')->lists('descricao','id');
        return view('admin.funcionario.create_edit', compact('funcionario', 'cargos'));
    }

    public function update(Request $request, Funcionario $solicitacao)
    {
        $solicitacao->update($request->all());
        return redirect('admin/funcionario');
    }

    public function store()
    {

    	Funcionario::create(Input::all());
        return redirect('admin/funcionario');
    }
 
    public function delete(Funcionario $funcionario)
    {
    	return view('admin.funcionario.delete', compact('funcionario'));
    }

    public function destroy(Funcionario $funcionario)
    {
    	$funcionario->delete();
    }

}