<?php 
namespace App\Http\Controllers\Admin;

use Datatables;
use App\Cliente;
use App\OrdemServico;
use App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\Input;

class DashboardController extends AdminController
{

	public function index() {
		$numero_clientes = Cliente::all()->count();
		$total_os = OrdemServico::select('id')->count();
		$os_abertas = OrdemServico::select('id')->where('status', false)->count();
        return view('admin.dashboard.index', compact('numero_clientes', 'total_os', 'os_abertas'));
    }

}