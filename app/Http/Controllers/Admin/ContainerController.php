<?php 
namespace App\Http\Controllers\Admin;

use Datatables;
use App\Container;
use App\Cliente;
use App\Http\Controllers\AdminController;
use App\Http\Requests\Admin\ItemRotaRequest;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\ContainerRequest;
use DB;
use PDF;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ContainerController extends AdminController
{
	

    public function index(Request $request)
    {
       
        $containers = new Container();

        if ($request->has('numero')) 
        {
            $containers = $containers->where('numero', 'like' , $request->get('numero').'%');
        }
        if ($request->has('status')) {
            $containers = $containers->where('status', $request->get('status'));
        }
        $containers = $containers->orderBy('numero', 'ASC')->paginate(10);

        return view('admin.container.index', compact('containers'));
    }


	public function create()
	{
		
        return view('admin.container.create_edit');
    }

    public function edit(Container $container)
	{        
        return view('admin.container.create_edit', compact('container'));
    }

    public function update(Request $request, Container $container)
    {
        $container->update($request->all());
        return redirect('admin/container');
    }

    public function store(ContainerRequest $request)
    {
    	Container::create($request->all());
        return redirect('admin/container');
    }
 
    public function delete(Container $container)
    {
    	return view('admin.container.delete', compact('container'));
    }

    public function destroy(Container $container)
    {
    	$container->delete();
    }
    public function dados()
    {
        $containers = Container::where('numero', 'like', Input::get('query').'%');
        $containers = Container::where('status', 'disponivel')
        ->where('numero', 'like', Input::get('query').'%')
        ->limit(5)->get();
        return response()->json($containers);
    }
    public function getContainersDisponiveis()
    {
        $containers = Container::where('status', 'disponivel')->get();
        return response()->json($containers);
    }
    public function getContainersLocadoByCliente($idCliente)
    {
        $containers = Container::where('status', 'locado')->where('cliente_id', $idCliente)
        ->get();
        return response()->json($containers);
    }



}