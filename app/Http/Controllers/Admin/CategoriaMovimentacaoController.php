<?php 
namespace App\Http\Controllers\Admin;

use Datatables;
use App\CategoriaMovimentacao;
use App\Ocorrencia;
use App\Http\Controllers\AdminController;
use App\Http\Requests\Admin\CategoriaMovimentacaoRequest;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;

class CategoriaMovimentacaoController extends AdminController
{

    public function __construct()
    {
        view()->share('type', 'categoriamovimentacao');
    }

    public function index()
    {
       
        return view('admin.categoria_movimentacao.index');
    }
    public function getCategorias()
    {
        
        $categorias = CategoriaMovimentacao::lists('id','titulo')->toArray();
        
        
        return response()->json($categorias);    
    }
	public function create()
	{
		
        return view('admin.categoria_movimentacao.create_edit');
    }

    public function edit(CategoriaMovimentacao $categoria)
	{

        return view('admin.categoria_movimentacao.create_edit', compact('categoria'));
    }

    public function update(CategoriaMovimentacaoRequest $request, CategoriaMovimentacao $categoria)
    {
        $categoria->update($request->all());
    }

    public function store(CategoriaMovimentacaoRequest $request)
    {
    	CategoriaMovimentacao::create($request->all());
    }
 
    public function delete(CategoriaMovimentacao $categoria_movimentacao)
    {
    	return view('admin.categoria_movimentacao.delete', compact('categoria_movimentacao'));
    }

    public function destroy(CategoriaMovimentacao $categoria_movimentacao)
    {
    	$categoria_movimentacao->delete();
    }
    public function data()
    {
         $selects = array(
            'categorias_mov.id', 
            'categorias_mov.titulo'
            );
            
        $categorias = CategoriaMovimentacao::select($selects);

        return Datatables::of($categorias)
            
            ->add_column('actions', '<a href="{{{ url(\'admin/categoriamovimentacao/\' . $id . \'/edit\' ) }}}" class="btn btn-success btn-xs iframe" ><span class="glyphicon glyphicon-pencil"></span></a>'
                    .'<a href="{{{ url(\'admin/categoriamovimentacao/\' . $id . \'/delete\' ) }}}" class="btn btn-xs btn-success iframe"><span class="glyphicon glyphicon-trash"></span></a>'
                )
            ->make();
    }
}