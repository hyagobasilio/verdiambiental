<?php 
namespace App\Http\Controllers\Admin;

use Datatables;
use App\OrdemServicoItem;
use App\Endereco;
use App\OrdemServico;
use App\Container;
use App\Http\Controllers\AdminController;
use App\Http\Requests\Admin\OrdemServicoItemRequest;
use Illuminate\Support\Facades\Input;
use DB;

class OrdemServicoItemController extends AdminController
{
	
    public function getEnderecosByCliente($idCliente)
    {
        $enderecos = [];
        
        $ends = Endereco::
        select(['enderecos.id', 'enderecos.cep', 'enderecos.endereco', 
            'enderecos.numero', 'enderecos.bairro', 'cidades.nome as cidade', 
            'cidades.uf' ])
        ->leftJoin('cidades', 'cidades.id', '=', 'enderecos.cidade_id')
        ->where('cliente_id', $idCliente)->get();
        
        foreach($ends as $end) {
            $cep =  (!empty($end->cep)) ? ', CEP: '. $end->cep : '';
             
            $titulo = $end->endereco . ' '. $end->numero
                    . ', bairro: ' . $end->bairro .', '
                    . $end->cidade . ' - ' . $end->uf. $cep;
            $enderecos[$end->id] = $titulo;
        }
        return response()->json($enderecos);
    }
	public function create($idOS)
	{
        
        $cxRemocao = [null => 'Selecione'] + Container::lists('numero','id')->toArray();
        
        $cxEntrega = [null => 'Selecione'] + Container::lists('numero','id')->toArray();
        
        
        return view('admin.ordemservico_item.create_edit', compact('idOS', 'cxRemocao', 'cxEntrega'));
    }

    public function edit(OrdemServicoItem $item)
	{
        
        $cxRemocao = [null => 'Selecione'] + Container::lists('numero','id')->toArray();
        
        $cxEntrega = [null => 'Selecione'] + Container::lists('numero','id')->toArray();
        
        
        return view('admin.ordemservico_item.create_edit', compact('item','cxRemocao', 'cxEntrega'));
    
    }

    public function update(OrdemServicoItemRequest $request, OrdemServicoItem $item)
    {
        $this->atualizarContainer($request);
        $item->update($request->all());
    }

    public function store(OrdemServicoItemRequest $request)
    {
        
        $this->atualizarContainer($request);
    	OrdemServicoItem::create($request->all());
    }
 
    public function delete(OrdemServicoItem $item)
    {
        if(!empty($item->caixa_id)) {
            $cx = Container::findOrFail($item->caixa_id);
            $cx->status = 'disponivel';
            $cx->save();
        }
    	return view('admin.ordemservico_item.delete', compact('item'));
    }

    public function destroy(OrdemServicoItem $itemrota)
    {

        if(!empty($itemrota->remove_cx_id))
        {
            $caixa = \App\Container::findOrFail($itemrota->remove_cx_id);
            $caixa->status = 'disponivel';
            $caixa->cliente_id = null;
            $caixa->save();
        }
        if(!empty($itemrota->caixa_id)) 
        {
            $caixa = \App\Container::findOrFail($itemrota->caixa_id);
            $caixa->status = 'disponivel';
            $caixa->cliente_id = null;
            $caixa->save();
        }
    	$itemrota->delete();
    }
    /**
     * Show a list of all the languages posts formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function data($idOS)
    {
        $itens = OrdemServicoItem::select(['ordem_servico_item.id',
                DB::raw('concat(clientes.nome, clientes.nome_fantasia) as nome'),
                DB::raw('concat(clientes.cpf, clientes.cnpj) as cpf_cnpj'),
                DB::raw('concat(containers.numero, \' - \' ,containers.tamanho) as cx_entrega'),
                DB::raw('concat(cxrm.numero, \' - \' ,cxrm.tamanho) as cx_rm'),
                'ordem_servico_item.numero_cupom',
                'ordem_servico_item.manifesto',
                'ordem_servico_item.tipo_entulho',
                'ordem_servicos.tipo',
                'ordem_servicos.servico' 
                ])
        ->join('ordem_servicos', 'ordem_servicos.id', '=', 'ordem_servico_item.os_id')
        ->join('clientes', 'clientes.id', '=', 'ordem_servico_item.cliente_id')
        ->leftJoin('containers', 'containers.id', '=', 'ordem_servico_item.caixa_id')
        ->leftJoin('containers as cxrm', 'cxrm.id', '=', 'ordem_servico_item.remove_cx_id')
        ->where('ordem_servico_item.os_id', $idOS);

        return Datatables::of($itens)
            
            ->remove_column('id')
            ->add_column('actions', '<a href="{{{ url(\'admin/ordemservicoitem/\' . $id . \'/edit?tipo=\' . $tipo.\'&servico=\'. $servico) }}}" class="btn btn-xs btn-success btn-sm iframe" ><span class="glyphicon glyphicon-pencil"></span>  Editar</a>
                    <a href="{{{ url(\'admin/ordemservicoitem/\' . $id . \'/delete\' ) }}}" class="btn btn-sm btn-danger btn-xs iframe"><span class="glyphicon glyphicon-trash"></span> Deletar</a>
                ')
            ->remove_column('servico')
            ->remove_column('tipo')
            ->make();
    }

    private function atualizarContainer($request)
    {

        if($request->has('remove_cx_id'))
        {
            $caixa = \App\Container::findOrFail($request->get('remove_cx_id'));
            $caixa->status = 'disponivel';
            $caixa->cliente_id = null;
            $caixa->save();
        }
        if($request->has('caixa_id')) 
        {
            $caixa = \App\Container::findOrFail($request->get('caixa_id'));
            $caixa->status = 'locado';
            $caixa->cliente_id = $request->get('cliente_id');
            $caixa->save();
        }
    }

    private function equals($texto1 , $texto2)
    {
        if(strcasecmp($texto1, $texto2) == 0){
            return true;
        }
        return false;
    }

}