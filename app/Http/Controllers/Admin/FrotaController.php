<?php 
namespace App\Http\Controllers\Admin;

use Datatables;
use App\Frota;
use App\Cliente;
use App\Http\Controllers\AdminController;
use App\Http\Requests\Admin\ItemRotaRequest;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\FrotaRequest;
use DB;
use PDF;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class FrotaController extends AdminController
{
	

    public function index(Request $request)
    {
       
        $frota = new Frota();

        if ($request->has('caminhao')) 
        {
            $frota = $frota->where('caminhao', 'like' , $request->get('caminhao').'%');
        }
        if ($request->has('placa')) 
        {
            $frota = $frota->where('placa', 'like' , $request->get('placa').'%');
        }
        if ($request->has('tipo')) {
            $frota = $frota->where('tipo', $request->get('tipo'));
        }
        $frota = $frota->paginate(10);

        return view('admin.frota.index', compact('frota'));
    }


	public function create()
	{
		
        return view('admin.frota.create_edit');
    }

    public function edit(Frota $frota)
	{        
        return view('admin.frota.create_edit', compact('frota'));
    }

    public function update(Request $request, Frota $frota)
    {
        $frota->update($request->all());
        return redirect('admin/frota');
    }

    public function store(FrotaRequest $request)
    {
    	Frota::create($request->all());
        return redirect('admin/frota');
    }
 
    public function delete(Frota $frota)
    {
    	return view('admin.frota.delete', compact('frota'));
    }

    public function destroy(Frota $frota)
    {
    	$frota->delete();
    }

}