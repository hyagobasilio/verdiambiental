<?php 
namespace App\Http\Controllers\Admin;

use Datatables;
use App\Solicitacao;
use App\Ocorrencia;
use App\Http\Controllers\AdminController;
use App\Http\Requests\Admin\ItemRotaRequest;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;

class SolicitacaoController extends AdminController
{
	

    public function index()
    {
        $solicitacoes = Solicitacao::with('usuario')->paginate(10);

        $solicitacoes = DB::table('solicitacoes')
            ->select(['solicitacoes.id', 'solicitacoes.prioridade', 'solicitacoes.status', 'solicitacoes.titulo', 'users.name','solicitacoes.user_id'])
            ->leftJoin('users', 'solicitacoes.user_id', '=', 'users.id')
            ->whereNull('solicitacoes.comentario_id')
            ->orderBy('solicitacoes.id', 'DESC')
            ->paginate(7);
        return view('admin.solicitacao.index', compact('solicitacoes'));
    }
	public function create()
	{
		
        return view('admin.solicitacao.create_edit');
    }

    public function edit(Solicitacao $solicitacao)
	{
		
        return view('admin.solicitacao.create_edit', compact('solicitacao'));
    }

    public function update(Request $request, Solicitacao $solicitacao)
    {
        $solicitacao->update($request->all());
    }

    public function store(Request $request)
    {
    	Solicitacao::create($request->all());
    }
 
    public function delete(Solicitacao $solicitacao)
    {
    	return view('admin.solicitacao.delete', compact('solicitacao'));
    }

    public function destroy(Solicitacao $solicitacao)
    {
    	$solicitacao->delete();
    }
    public function show(Solicitacao $solicitacao)
    {
        return view('admin.solicitacao.show', compact('solicitacao'));
    }

    public function comentarios($idSolicitacao)
    {
        $solicitacoes = DB::table('solicitacoes')
            ->select(['solicitacoes.id', 'solicitacoes.descricao', 'users.name','solicitacoes.user_id', 'users.filename', 'solicitacoes.created_at'])
            ->leftJoin('users', 'solicitacoes.user_id', '=', 'users.id')
            ->where('solicitacoes.comentario_id', $idSolicitacao)
            ->get();

        return response()->json($solicitacoes);

    }
    public function salvarComentario(Request $request, $idSolicitacao)
    {

        $dados = [
            'descricao' => $request->get('descricao'),
            'user_id' => Auth::user()->id,
            'comentario_id' => $idSolicitacao
        ];

        Solicitacao::create($dados);
    }
    public function removerComentario(Solicitacao $solicitacao) 
    {
        $solicitacao->delete();
    }

}