<?php 
namespace App\Http\Controllers\Admin;

use Datatables;
use App\Cliente;
use App\Endereco;
use App\Cidade;
use App\Contrato;
use App\Services\ClienteService;
use App\Http\Controllers\AdminController;
use App\Http\Requests\Admin\ClienteRequest;
use Illuminate\Support\Facades\Input;
use PDF;
use Illuminate\Http\Request;

class ClienteController extends AdminController
{

    private $service;

    public function __construct(ClienteService $service)
    {
        $this->service = $service;
        //view()->share('type', 'cliente');
    }

    
    public function getCidades() {
        
        
        $query = Input::get('query');
        $cidades = Cidade::select(['id', 'nome', 'uf'])
        ->where('nome', 'like', $query . '%')->take(5)->get();
        return response()->json($cidades);
    }

    public function getClientes() {
        $query = Input::get('q');
        $clientes = Cliente::select(['clientes.id', 'clientes.nome', 'clientes.nome_fantasia', 'clientes.endereco',
            'cidades.nome as cidade', 'clientes.cidade_id', 'clientes.cep', 'clientes.bairro', 'cidades.uf'])
        ->leftJoin('cidades', 'cidades.id', '=', 'clientes.cidade_id' )
        ->where('clientes.nome', 'like', '%'.$query.'%')
        ->orWhere('clientes.nome_fantasia', 'like', '%'.$query.'%')
        ->get();

        

        return response()->json($clientes);
    }

    public function getClientesParaOS(Request $request)
    {
        return $this->service->getClientesParaOS($request);
    }

    /*
    * Display a listing of the resource.
    *
    * @return Response
    */
    public function index()
    {
        $nome = Input::get('nome');
        $clientes = Cliente::where('nome', 'like', $nome.'%')
            ->orWhere('nome_fantasia', 'like', $nome.'%')
            ->orderBy('nome_fantasia', 'ASC')
            ->orderBy('nome', 'ASC')
            ->paginate(10);
        return view('admin.clientes.index', compact('clientes'));
    }

    public function pdf(Cliente $cliente)
    {
        $cidade = Cidade::findOrFail($cliente->cidade_id);
        $cliente->cidade = $cidade;

        $contratos = Contrato::select(['id', 'descricao'])->where('cliente_id', $cliente->id)->get();

        $pdf = PDF::loadView('admin.clientes.show',compact('cliente', 'contratos'));
        return $pdf->stream('cliente.pdf');
    }

    public function show(Cliente $cliente) 
    {
        $cidade = Cidade::findOrFail($cliente->cidade_id);
        $cliente->cidade = $cidade;
        $enderecos = Endereco::where('cliente_id', $cliente->id)->get();

        $contratos = Contrato::select(['id', 'descricao'])->where('cliente_id', $cliente->id)->get();
        return view('admin.clientes.show', compact('cliente', 'contratos', 'enderecos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.clientes.create_edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(ClienteRequest $request)
    {
        
        return $this->service->create($request->all());
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $cliente
     * @return Response
     */
    public function edit(Cliente $cliente)
    {
        $end = Endereco::where('cliente_id', $cliente->id)
        ->where('contrato_id', null)->first();
        
        if (!empty($end)){
            if (!empty($end->cidade->nome)){
                $cliente->cidade = $end->cidade->nome . ' - ' . $end->cidade->uf;
            }
            $cliente->endereco = $end->endereco;
            $cliente->numero = $end->numero;
            $cliente->cep = $end->cep;
            $cliente->cidade_id = $end->cidade_id;
            $cliente->bairro = $end->bairro;
            $cliente->complemento = $end->complemento;
            $cliente->cidade_id = $end->cidade_id;
        }

        
        return view('admin.clientes.create_edit', compact('cliente', 'end'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $cliente
     * @return Response
     */
    public function update(ClienteRequest $request, Cliente $cliente)
    {
        $end = Endereco::where('cliente_id', $cliente->id)
        ->where('contrato_id', null)->first();
        $end->update($request->all());
        
        $cliente->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $cliente
     * @return Response
     */

    public function delete(Cliente $cliente)
    {
        return view('admin.clientes.delete', compact('cliente'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $cliente
     * @return Response
     */
    public function destroy(Cliente $cliente)
    {
        $cliente->delete();
        return "deletado";
    }

    /**
     * Show a list of all the languages posts formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function data()
    {
        $selects = array(
            'clientes.id', 
            'clientes.nome', 
            'clientes.tipo_plano', 
            'clientes.cpf', 
            'clientes.cnpj', 
            'clientes.nome_fantasia', 
            'clientes.email', 
            'clientes.telefone1',
            'clientes.telefone2');
            //'clientes.created_at'
        $clientes = Cliente::select($selects);

        return Datatables::of($clientes)
            ->remove_column('id')
            ->add_column('actions', '<a href="{{{ URL::to(\'admin/cliente/\' . $id . \'/edit\' ) }}}" class="btn btn-success btn-xs iframe" ><span class="glyphicon glyphicon-pencil"></span></a>'
                    .'<a href="{{{ URL::to(\'admin/cliente/\'.$id ) }}}" class="btn btn-xs btn-success iframe"><span class="glyphicon glyphicon-eye-open"></span></a>'
                    .'<a href="{{{ URL::to(\'admin/cliente/\' . $id . \'/delete\' ) }}}" class="btn btn-xs btn-success iframe"><span class="glyphicon glyphicon-trash"></span></a>'
                    .'<a target="_blank" href="{{{ URL::to(\'admin/cliente/\' . $id . \'/pdf\' ) }}}" class="btn btn-xs btn-success "><span class="fa fa-file-pdf-o"></span></a>
                ')
            ->make();
    }

}
