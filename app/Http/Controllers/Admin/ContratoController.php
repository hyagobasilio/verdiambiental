<?php 
namespace App\Http\Controllers\Admin;

use Datatables;
use App\Contrato;
use App\Cliente;
use App\Http\Controllers\AdminController;
use App\Http\Requests\Admin\ItemRotaRequest;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\ContratoRequest;
use DB;
use PDF;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ContratoController extends AdminController
{
	

    public function index(Request $request)
    {
       
        $contratos = Contrato::select(['contratos.id', 'contratos.descricao', 'clientes.nome', 'clientes.nome_fantasia'])
            ->leftJoin('clientes', 'contratos.cliente_id', '=', 'clientes.id');

        if ($request->has('cliente_search')) 
        {
            $contratos = $contratos->where('nome', 'like' , $request->get('cliente_search').'%')
                ->orWhere('nome_fantasia', 'like' , $request->get('cliente_search').'%');
        }
        if ($request->has('descricao')) {
            $contratos = $contratos->where('descricao', 'like', $request->get('descricao').'%');
        }
        $contratos = $contratos->paginate(10);

        return view('admin.contrato.index', compact('contratos'));
    }

    public function pdf(Contrato $contrato)
    {
        $contrato = $contrato->with('cliente')->first();
        $pdf = PDF::loadView('admin.contrato.show', compact('contrato'));
        return $pdf->stream('contrato.pdf');
    }
    public function show(Contrato $contrato)
    {
        return view('admin.contrato.show', compact('contrato'));
    }

	public function create()
	{
		
        return view('admin.contrato.create_edit');
    }

    public function edit(Contrato $contrato)
	{
        if(!empty($contrato->cliente_id)){
		    $cliente = Cliente::findOrFail($contrato->cliente_id);
            $contrato->cliente = $cliente;
        }
        return view('admin.contrato.create_edit', compact('contrato'));
    }

    public function update(Request $request, Contrato $solicitacao)
    {
        $solicitacao->update($request->all());
        return redirect('admin/contrato');
    }

    public function store(ContratoRequest $request)
    {
    	$contrato = Contrato::create($request->all());
        return redirect('admin/contrato/'.$contrato->id.'/edit');
    }
 
    public function delete(Contrato $contrato)
    {
    	return view('admin.contrato.delete', compact('contrato'));
    }

    public function destroy(Contrato $contrato)
    {
    	$contrato->delete();
    }

}