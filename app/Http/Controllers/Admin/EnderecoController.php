<?php 
namespace App\Http\Controllers\Admin;

use Datatables;
use App\Endereco;
use App\Http\Controllers\AdminController;
use App\Http\Requests\Admin\EnderecoRequest;
use Illuminate\Support\Facades\Input;
use DB;

class EnderecoController extends AdminController
{
	

	public function create()
	{
        return view('admin.enderecos.create_edit');
    }

    public function store(EnderecoRequest $request)
    {
    	Endereco::create($request->all());
        
    }
    public function edit(Endereco $endereco)
    {
        
        
        return view('admin.enderecos.create_edit', compact('endereco'));
    }

    public function update(EnderecoRequest $request, Endereco $endereco)
    {
        
        $endereco->update($request->all());
    }

 
    public function delete(Endereco $endereco)
    {
    	return view('admin.enderecos.delete', compact('endereco'));
    }

    public function destroy(Endereco $endereco)
    {
    	$endereco->delete();
    }
    /**
     * Show a list of all the languages posts formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function getEnderecosByContrato($idContrato)
    {
        $enderecos = OrdemServicoItem::select(['enderecos.id',
                DB::raw('concat(cidades.nome, \' - \', cidades.uf) as cidade'),
                'enderecos.endereco',
                'enderecos.numero',
                'enderecos.bairro',
                'enderecos.cep',
                'enderecos.complemento'
                ])
        ->join('cidades', 'cidades.id', '=', 'enderecos.cidade_id')
        ->where('enderecos.cliente_id', $idContrato);
        return Datatables::of($enderecos)
            
            ->add_column('actions', '<a href="{{{ url(\'admin/endereco/\' . $id . \'/edit\' ) }}}" class="btn btn-xs btn-success btn-sm iframe" ><span class="glyphicon glyphicon-pencil"></span>  Editar</a>
                    <a href="{{{ url(\'admin/endereco/\' . $id . \'/delete\' ) }}}" class="btn btn-sm btn-danger btn-xs iframe"><span class="glyphicon glyphicon-trash"></span> Deletar</a>
                ')
            ->remove_column('id')
            ->make();
    }

    public function getEnderecosByCliente($idCliente)
    {
        $enderecos = Endereco::select(['enderecos.id',
                DB::raw('concat(cidades.nome, \' - \', cidades.uf) as cidade'),
                'enderecos.endereco',
                'enderecos.numero',
                'enderecos.bairro',
                'enderecos.cep',
                'enderecos.complemento'
                ])
        ->leftJoin('cidades', 'cidades.id', '=', 'enderecos.cidade_id')
        ->where('enderecos.cliente_id', $idCliente);
        return Datatables::of($enderecos)
            
            ->add_column('actions', '<a href="{{{ url(\'admin/endereco/\' . $id . \'/edit\' ) }}}" class="btn btn-xs btn-success btn-sm iframe" ><span class="glyphicon glyphicon-pencil"></span>  Editar</a>
                    <a href="{{{ url(\'admin/endereco/\' . $id . \'/delete\' ) }}}" class="btn btn-sm btn-danger btn-xs iframe"><span class="glyphicon glyphicon-trash"></span> Deletar</a>
                ')
            ->remove_column('id')
            ->make();
    }


}