<?php 
namespace App\Http\Controllers\Admin;

use Datatables;
use App\OrdemServico;
use App\Helpers\Utils;
use App\Cidade;
use App\Funcionario;
use App\Cliente;
use App\Container;
use App\ItemRota;
use App\Services\OrdemServicoService;
use App\OrdemServicoItem;
use App\Http\Controllers\AdminController;
use App\Http\Requests\Admin\OrdemServicoRequest;
use App\Http\Requests\Admin\OcorrenciaRequestEdit;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use DB;
use PDF;
use App;    

class OrdemServicoController extends AdminController
{

    private $service;

    public function __construct(OrdemServicoService $service)
    {
        $this->service = $service;
        //view()->share('type', 'ocorrencia');
    }

    public function show(OrdemServico $ordemservico)
    {
        $itens = OrdemServicoItem::select(['ordem_servico_item.id',
                DB::raw('concat(clientes.nome, clientes.nome_fantasia) as nome'),
                DB::raw('concat(clientes.cpf, clientes.cnpj) as cpf_cnpj'),
                DB::raw('concat(containers.numero, \' - \' ,containers.tamanho) as cx_entrega'),
                DB::raw('concat(cxrm.numero, \' - \' ,cxrm.tamanho) as cx_rm'),
                'ordem_servico_item.numero_cupom',
                'ordem_servico_item.manifesto',
                DB::raw('concat(cidades.nome, \' - \' ,cidades.uf) as cidades'),
                'enderecos.endereco',
                'enderecos.numero',
                'enderecos.bairro',
                'enderecos.cep',
                'enderecos.complemento',
                'ordem_servico_item.tipo_entulho',
                 ])
        ->join('ordem_servicos', 'ordem_servicos.id', '=', 'ordem_servico_item.os_id')
        ->join('clientes', 'clientes.id', '=', 'ordem_servico_item.cliente_id')
        ->leftJoin('enderecos', 'enderecos.id', '=', 'ordem_servico_item.endereco_id')
        ->leftJoin('cidades', 'cidades.id', '=', 'enderecos.cidade_id')
        ->leftJoin('containers', 'containers.id', '=', 'ordem_servico_item.caixa_id')
        ->leftJoin('containers as cxrm', 'cxrm.id', '=', 'ordem_servico_item.remove_cx_id')
        ->where('ordem_servico_item.os_id', $ordemservico->id)
        ->get();
        $ordemservico->itens = $itens;
        if(!empty(Input::get('imprimir'))){

            $pdf = PDF::loadView('admin.ordem_servico.show', compact('ordemservico'));
            return $pdf->stream($ordemservico->id.'-ordem_servico.pdf');
        }
        return view('admin.ordem_servico.show', compact('ordemservico'));
    }
    public function getContainer()
    {
       return $this->service->getContainer();
    }
    
    public function index(Request $request)
    {
        $oss = new OrdemServico();
        $oss = $oss->orderBy('id','DESC')->get()->  all();
        return view('admin.ordem_servico.index', compact('oss'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $lista = Funcionario::select(['id', 'nome'])->where('cargo_id',2)->get();
        $motoristas = array('' => 'Selecione');
        foreach ($lista as $value) {
            $motoristas[$value->id] = $value->nome;
            
        }
        $ajudantes = [null => 'Selecione'] + Funcionario::where('cargo_id',3)->lists('nome','id')->toArray();
        $carros = [null => 'Selecione'] + App\Frota::lists('placa','id')->toArray();

        return view('admin.ordem_servico.create_edit', compact('motoristas', 'ajudantes', 'carros'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(OrdemServicoRequest $request)
    {
        $dados = $request->all();
        $this->service->create($dados);
        return redirect('/admin/ordemservico');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $ocorrencia
     * @return Response
     */
    public function edit(OrdemServico $ordemservico)
    {
         $lista = Funcionario::select(['id', 'nome'])->where('cargo_id',2)->get();
        $motoristas = array('' => 'Selecione');
        foreach ($lista as $value) {
            $motoristas[$value->id] = $value->nome;
            
        }
        $ajudantes = [null => 'Selecione'] + Funcionario::where('cargo_id',3)->lists('nome','id')->toArray();
        $carros = [null => 'Selecione'] + App\Frota::lists('placa','id')->toArray();

        
        
        return view('admin.ordem_servico.create_edit', compact('ordemservico', 'motoristas', 'ajudantes', 'carros'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $ocorrencia
     * @return Response
     */
    public function update(OrdemServicoRequest $request, OrdemServico $ocorrencia)
    {
        $dados = $request->all();
        //dd($dados);
        $ocorrencia->update($dados);
            
        return redirect('/admin/ordemservico');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $ocorrencia
     * @return Response
     */

    public function delete(OrdemServico $ordemservico)
    {
        return view('admin.ordem_servico.delete', compact('ordemservico'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $ocorrencia
     * @return Response
     */
    public function destroy(OrdemServico $ordemservico)
    {
        $ordemservico->delete();
        return url('admin/ordemservico');
    }

    /**
     * Show a list of all the languages posts formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function data()
    {
        $selects = array(
            'ocorrencias.id', 
            'servico', 
            'tipo', 
            'data', 
            'cliente', 
            //DB::raw('CONCAT(clientes.nome, clientes.nome_fantasia) as cliente'), 
            'valor_estimado', 
            'valor_final',
            'numero_container',
            'status',
            'ocorrencias.cidade',
            );
        
        $ocorrencias = Ocorrencia::select($selects)
        //->join('clientes', 'clientes.id', '=', 'ocorrencias.cliente_id' )
        ->orderBy('ocorrencias.id', 'DESC');

        return Datatables::of($ocorrencias)
           ->remove_column('cidade')
            //->add_column('data', '{{{ App\Helpers\Utils::data_to_br($data[\'data\']) }}}')
            ->add_column('status', '@if($status == 1) <span class="label label-success">Concluído</span> @else <span class="label label-warning">Aberto</span>@endif')
            ->add_column('actions', '<a href="{{{ URL::to(\'admin/ocorrencia/\' . $id . \'/edit\' ) }}}" class="btn btn-success btn-xs " ><span class="glyphicon glyphicon-pencil"></span></a>
                    <a href="{{{ URL::to(\'admin/ocorrencia/\' . $id . \'/delete\' ) }}}" class="btn btn-xs btn-danger iframe"><span class="glyphicon glyphicon-trash"></span></a>
                ')
            ->make();
    }


    public function getAlertas() {
        $query = " SELECT o.tipo, o.data, o.cliente, o.prazo "
            ." FROM ocorrencias o"
            ." where DATEDIFF(now(), o.data) >= o.prazo "
            ." and o.mostrar = 1; ";
        $dados = DB::select($query);
        $info = ['qtd' => count($dados), 'dados' => $dados];
        return response()->json($info);
    }

    public function dasativarAlertas() {
        $dados = DB::select(" update ocorrencias "
            ."  ocorrencias  "
            ." set mostrar=0 "
            ." where DATEDIFF(now(), data) >= prazo "
            ." and mostrar = 1; ");
    }
    
}
