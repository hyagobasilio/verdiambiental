<?php 
namespace App\Http\Controllers\Admin;

use Datatables;
use App\Cargo;
use App\Ocorrencia;
use App\Http\Controllers\AdminController;
use App\Http\Requests\Admin\CargoRequest;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;

class CargoController extends AdminController
{

    public function __construct()
    {
        view()->share('type', 'cargos');
    }
	

    public function index()
    {
       
        return view('admin.cargo.index');
    }
	public function create()
	{
		
        return view('admin.cargo.create_edit');
    }

    public function edit(Cargo $cargo)
	{
		
        return view('admin.cargo.create_edit', compact('cargo'));
    }

    public function update(CargoRequest $request, Cargo $cargo)
    {
        $cargo->update($request->all());
    }

    public function store(CargoRequest $request)
    {
    	Cargo::create($request->all());
    }
 
    public function delete(Cargo $cargo)
    {
    	return view('admin.cargo.delete', compact('cargo'));
    }

    public function destroy(Cargo $cargo)
    {
    	$cargo->delete();
    }
    public function data()
    {
         $selects = array(
            'cargos.id', 
            'cargos.descricao'
            );
            
        $cargos = Cargo::select($selects);

        return Datatables::of($cargos)
            
            ->add_column('actions', '<a href="{{{ url(\'admin/cargo/\' . $id . \'/edit\' ) }}}" class="btn btn-success btn-xs iframe" ><span class="glyphicon glyphicon-pencil"></span></a>'
                    .'<a href="{{{ url(\'admin/cargo/\' . $id . \'/delete\' ) }}}" class="btn btn-xs btn-success iframe"><span class="glyphicon glyphicon-trash"></span></a>'
                )
            ->make();
    }
}