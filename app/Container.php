<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Container extends Model
{
    protected $table = 'containers';
    protected $fillable = ['numero', 'status', 'tamanho', 'cliente_id'];    
    public $timestamps = false;
    
    public function cliente()
    {
    	return $this->belongsTo('App\Cliente');
    }
}
