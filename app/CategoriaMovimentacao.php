<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoriaMovimentacao extends Model {

	protected $table = 'categorias_mov';
	protected $fillable = ['titulo', 'id'];

}