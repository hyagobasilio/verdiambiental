<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Frota extends Model
{
    protected $table = 'frota';
    protected $fillable = ['caminhao', 'placa', 'tipo'];    
    public $timestamps = false;
    
}
