<?php

namespace App;
use App\Helpers\Utils;
use Illuminate\Database\Eloquent\Model;

class Ocorrencia extends Model
{
   	protected $table = 'ocorrencias';

   	protected $fillable = [
        'servico',
        'mostrar',
        'cliente',
        'cliente_id',
        'prazo',
        'observacao',
        'tipo',
        'status',
        'numero_container',
        'id_motorista',
        'data',
        'valor_estimado',
        'valor_final',
        'cidade',
        'endereco',
        'bairro',
        'cep',
        'cidade_id',
        'numero_cupom',
        'manifesto',
        'numero_ticket',
        'peso',
        'container_id'
   	];


    public function getPesoAttribute($value)
    {
        return number_format($value, 2, ',', '.');
    }
    public function setPesoAttribute($value)
    {
        $this->attributes['peso'] = Utils::moeda_para_bd($value);
    }

    public function getValorEstimadoAttribute($value)
    {
        return number_format($value, 2, ',', '.');
    }
    public function getValorFinalAttribute($value)
    {
        return number_format($value, 2, ',', '.');
    }

    public function setValorEstimadoAttribute($value)
    {
        $this->attributes['valor_estimado'] = Utils::moeda_para_bd($value);
    }
    public function setValorFinalAttribute($value)
    {
        $this->attributes['valor_final'] = Utils::moeda_para_bd($value);
    }
    public function getDataAttribute($value)
    {
        return Utils::data_to_br($value);

    }
    public function setDataAttribute($value)
    {
        $this->attributes['data'] = Utils::data_to_bd($value);
    }
    public function cidade()
    {
        return $this->belongsTo('App\Cidade');
    }
    public function cliente()
    {
        return $this->belongsTo('App\Cliente');
    }
    public function motorista()
    {
        return $this->belongsTo('App\Funcionario');
    }
    public function container()
    {
    	return $this->belongsTo('App\Container');
    }
    
}
