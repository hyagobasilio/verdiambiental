<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Endereco extends Model
{
    protected $table = 'enderecos';

    protected $fillable = ['endereco', 'numero', 'bairro', 'cep', 'complemento',
     'cidade_id','cliente_id','contrato_id'];


    public $timestamps = false;
    public function cidade()
    {
    	return $this->belongsTo('App\Cidade');
    }
}
