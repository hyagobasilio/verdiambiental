<?php 
namespace App\Services;
use App\Repositories\OrdemServicoRepository;
use App\Helpers\Utils;
use App\Cliente;
use App\ItemRota;
use App\Container;
use Illuminate\Support\Facades\Auth;
class OrdemServicoService {

	private $repository;

	public function __construct(OrdemServicoRepository $repo) {
		$this->repository = $repo;
	}

	public function create($data)
	{
		$data = $this->vazioToNull($data);
		$data['solicitante_id'] = Auth::user()->id;
		return $this->repository->create($data);
	}

	private function vazioToNull($data) 
	{
		foreach ($data as $key => $value) {
			$data[$key] = empty($value) && $key != 'status' ? null : $value;
		}
		return $data;
	}

}