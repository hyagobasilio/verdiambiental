<?php 
namespace App\Services;
use App\Repositories\OcorrenciaRepository;
use App\Helpers\Utils;
use App\Cliente;
use App\ItemRota;
use App\Container;
class OcorrenciaService {

	private $ocorrenciaRepository;

	public function __construct(OcorrenciaRepository $repo) {
		$this->ocorrenciaRepository = $repo;
	}

	public function create($data)
	{
		//$data['data'] = Utils::data_to_bd($data['data']);
		$data['mostrar'] = 1;

		$os = $this->ocorrenciaRepository->create($data);

		/*$itens = json_decode($data['itens_rota']);

		$dados = [];
		foreach ($itens as $item) {
			$dados[] = ['data_rota' => $item, 'ocorrencia_id' => $os->id];
		}
		ItemRota::insert($dados);*/
	}

	public function getContainer()
	{
		$containers = Container::where('status', 'disponivel')->limit(5)->get();
		return response()->json($containers);

	}

}