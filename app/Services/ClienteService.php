<?php 
namespace App\Services;
use App\Repositories\ClienteRepository;
use App\Helpers\Utils;
use App\Cliente;
use App\Endereco;
use Illuminate\Http\Request;
use DB;
class ClienteService {

	private $clienteRepository;

	public function __construct(ClienteRepository $repo) {
		$this->clienteRepository = $repo;
	}

	public function create($data)
	{
		
		$erros = [];
		if( strcasecmp('juridica', $data['tipo-pessoa']) == 0)
		{

			if (empty($data['nome_fantasia'])) {
				$erros['error'][] = ['campo' => 'nome_fantasia', 'mensagem' => 'Digite o nome fantasia!'];
			}

			if ( !Utils::valida_cnpj($data['cnpj']))
			{
				$erros['error'][] = ['campo' => 'cnpj', 'mensagem' => 'CNPJ inválido!'];
			}

			if (!empty($data['cnpj'])) 
			{

				if ( !empty(Cliente::where('cnpj', '11')->first() ) ) 
				{
					$erros['error'][] = ['campo' => 'cnpj', 'mensagem' => 'CNPJ já cadastrado!'];
				}
			}

		}
		else if( strcasecmp('fisica', $data['tipo-pessoa']) == 0)
		{
			if (!Utils::valida_cpf($data['cpf']))
			{
				$erros['error'][] = ['campo' => 'cpf', 'mensagem' => 'CPF inválido!'];
			}
			//valida se cpf ja está cadastrado
			if (!empty($data['cpf']) &&
					!empty(Cliente::where(['cpf' => $data['cpf']])->first()) )
			{
				$erros['error'][] = ['campo' => 'cpf', 'mensagem' => 'CPF já cadastrado!'];
			}
			if (empty($data['nome'])) {
				$erros['error'][] = ['campo' => 'nome', 'mensagem' => 'Digite um nome!'];
			}

		}

		if ( empty($data['email']) || (!empty($data['email']) && !filter_var($data['email'], FILTER_VALIDATE_EMAIL)) ){
			$erros['error'][] = ['campo' => 'email', 'mensagem' => 'Email inválido!'];
		}

		if ( empty($data['telefone1'])) {
			$erros['error'][] = ['campo' => 'telefone1', 'mensagem' => 'Cadastre um número de telefone!'];
		}
		if ( empty($data['tipo_plano'])) {
			$erros['error'][] = ['campo' => 'tipo_plano', 'mensagem' => 'Cadastre um número de telefone!'];
		}

		
		

		if (empty($data['endereco'])) {
			$erros['error'][] = ['campo' => 'endereco', 'mensagem' => 'Digite um endereço!'];
		}
		if (empty($data['bairro'])) {
			$erros['error'][] = ['campo' => 'bairro', 'mensagem' => 'Digite um bairro!'];
		}

		
		if (!empty($erros))
		{
			return response()->json($erros, 401);
		}
		$cliente = $this->clienteRepository->create($data);
		$data['cliente_id'] = $cliente->id;
		Endereco::create($data);


	}

	public function validaCampos($campos)
	{
		$erros = [];
		foreach ($campos as $key => $value) {
			if ( empty($value)) {
				$erros[] = ['campo' => $key, 'mensagem' => strtoupper($key).' inválido!'];
			}
		}
		return $erros;
	}


	public function getClientesParaOS(Request $request) {
        $nome = $request->get('q');
        
        $clientes = DB::table('clientes')
        ->select(['clientes.id', 'clientes.nome', 'clientes.nome_fantasia', 'clientes.endereco',
            'cidades.nome as cidade', 'clientes.cidade_id', 'clientes.cep', 'clientes.bairro', 'cidades.uf'])
        ->leftJoin('cidades', 'cidades.id', '=', 'clientes.cidade_id' )
        ->where(function ($query) use ($nome) 
        {
        	  $query->where('clientes.nome_fantasia', 'like', '%'.$nome.'%')
        			->orWhere('clientes.nome', 'like', '%'.$nome.'%');
        })
        ->whereExists(function ($query) {
                $query->select(DB::raw(1))
                      ->from('contratos')
                      ->whereRaw('contratos.cliente_id = clientes.id');
        })
        
        ->get();

        

        return response()->json($clientes);
    }

}