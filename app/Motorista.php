<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\Utils;

class Motorista extends Model
{
    protected $table = 'motoristas';
    public $timestamps = false;
    
    protected $fillable = [
    	'nome',
    	'matricula',
    	'telefone',
    	'email',
    	'data_nascimento',
    	'rg',
    	'cpf',
    	'data_admissao',
    	'numero_cnh',
    	'categoria_cnh',
    	'validade',
    	'tipo' // ajudante ou motorista 
    	];

    public function getDataNascimentoAttribute($value)
    {
        return Utils::data_to_br($value);
    }

    public function setDataNascimentoAttribute($value)
    {
        $this->attributes['data_nascimento'] = Utils::data_to_bd($value);
    }

    public function getDataAdmissaoAttribute($value)
    {
        return Utils::data_to_br($value);
    }

    public function setDataAdmissaoAttribute($value)
    {
        $this->attributes['data_admissao'] = Utils::data_to_bd($value);
    }
    public function getValidadeAttribute($value)
    {
        return Utils::data_to_br($value);
    }

    public function setValidadeAttribute($value)
    {
        $this->attributes['validade'] = Utils::data_to_bd($value);
    }
}
