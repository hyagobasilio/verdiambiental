<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrdemServicoItem extends Model
{
    protected $table = 'ordem_servico_item';

    protected $fillable = ['numero_cupom', 'manifesto', 
        'cliente_id', 'caixa_id', 'os_id', 'endereco_id',
        'remove_cx_id',
        'tipo_entulho'
    ];

    public $timestamps = false;

    public function caixa()
    {
        return $this->belongsTo('App\Container', 'caixa_id');
    }

    public function caixaretirada()
    {
        return $this->belongsTo('App\Container', 'remove_cx_id');
    }
    public function cliente()
    {
    	return $this->belongsTo('App\Cliente');
    }

    public function endereco()
    {
        return $this->belongsTo('App\Endereco');
    }

    public function os()
    {
    	return $this->belongsTo('App\OrdemServico');
    }
}
