<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $table = 'clientes';
    protected $fillable = [
    'cpf', 'cnpj', 'nome_fantasia', 'razao_social','cidade', 'tipo_plano',
    'nome', 'telefone1', 'telefone2', 'email', 'endereco', 'bairro', 'cidade_id', 'cep'];    
    
    /**
	 * The rules for email field, automatic validation.
	 *
	 * @var array
	*/
	private $rules = array(
			
	);

	public function cidade()
	{
		return $this->belongsTo('App\Cidade');
	}
}
