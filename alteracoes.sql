ALTER TABLE `verdi`.`clientes` 
CHANGE COLUMN `nome` `nome` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL ,
CHANGE COLUMN `telefone2` `telefone2` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL ,
CHANGE COLUMN `email` `email` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL ,
DROP INDEX `clientes_email_unique` ,
DROP INDEX `clientes_telefone1_unique` ;
