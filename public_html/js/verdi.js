$(function () {
    $('input.cpf').inputmask("999.999.999-99");
    $("input.telefone").inputmask("(99) 9999-9999[9]");
    $("input#cnpj").inputmask("99.999.999/9999-99");
    $("input#cep").inputmask("99999-999");
    //$("input#cep").inputmask("99999-999");

   
    $("input#data, .data").datepicker({
        format: 'dd/mm/yyyy' ,
    	use24hours: true,
    	regional 	: 'pt-br'
    });
    
    $(".valor").maskMoney({
        /*symbol:'R$ ', 
        showSymbol:true, */
        thousands:'.', 
        decimal:',', 
        symbolStay: true
    });
    //$("input#data").inputmask('99/99/9999');

});