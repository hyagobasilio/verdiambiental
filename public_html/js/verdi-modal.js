$(function(){
	$.get(publicPath+'/admin/ocorrencia/alertas',function(d){
		if(d.qtd > 0) {
			var trs = "";
			$.each(d.dados, function(i,dado){
				trs = trs + "</tr>"
				+ "<td>" + dado.tipo + "</td>"
				+ "<td>" + dado.cliente + "</td>"
				+ "<td>" + dado.data + "</td>"
				+ "<td>" + dado.prazo + "</td>"
				+ "</tr>";
			});
			$('#body-tb-modal').html(trs);
			$('.modal').modal('toggle');
		}
	});

	$('.ok-entendi').click(function(){
		$.get(publicPath+'/admin/ocorrencia/desativaralertas',function() {
			$('.modal').modal('toggle');
		});
	});
});