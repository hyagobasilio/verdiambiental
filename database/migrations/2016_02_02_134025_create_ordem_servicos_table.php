<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdemServicosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ordem_servicos', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();

            $table->integer('solicitante_id')->unsigned()->nullable();
            $table->foreign('solicitante_id')
              ->references('id')->on('users')
              ->onDelete('set null');

            $table->integer('motorista_id')->unsigned()->nullable();
            $table->foreign('motorista_id')
              ->references('id')->on('funcionarios')
              ->onDelete('set null');

            $table->integer('ajudante_id')->unsigned()->nullable();
            $table->foreign('ajudante_id')
              ->references('id')->on('funcionarios')
              ->onDelete('set null');

            $table->enum('servico', ['entulho', 'lixo'])->nullable();
            $table->boolean('status')->dafault(0);
            $table->enum('tipo', ['remocao', 'retirada', 'entrega'])->nullable();
            $table->date('data_abertura')->nullable();
            $table->date('prazo')->nullable();
            $table->string('observacao')->nullable();

            $table->string('numero_cupom', 45)->nullable();
            $table->string('manifesto', 45)->nullable();
            $table->string('numero_ticket', 45)->nullable();
            $table->double('peso', 10,2)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ordem_servicos')
    }
}
