<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsTableItensRota extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('itens_rota', function (Blueprint $table) {
            $table->integer('numero_cupom')->nullable();
            $table->string('manifesto')->nullable();
            $table->integer('numero_ticket')->nullable();
            $table->double('peso', 10,2)->nullable();
            $table->enum('status',['pendente', 'concluida'])->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ocorrencias', function (Blueprint $table) {
            $table->removeColumn('numero_cupom');
            $table->removeColumn('manifesto');
            $table->removeColumn('numero_ticket');
            $table->removeColumn('peso');
            $table->removeColumn('status');
        });
    }
}
