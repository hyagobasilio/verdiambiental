<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameTableMotoristaToFuncionario extends Migration
{
    /**
     * Run the migrations.
     *
     * ALTER TABLE `motoristas` RENAME `funcionarios`
     * @return void
     */
    public function up()
    {
        Schema::rename('motoristas', 'funcionarios');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
