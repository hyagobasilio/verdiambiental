<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableEnderecos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enderecos', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('endereco');
            $table->integer('numero')->nullable();
            $table->string('bairro');
            $table->string('complemento')->nullable();
            $table->string('cep')->nullable();

            $table->unsignedInteger('cidade_id')->nullable();
            $table->foreign('cidade_id')
              ->references('id')->on('cidades')
              ->onDelete('set null');
            $table->unsignedInteger('cliente_id')->nullable();
            $table->foreign('cliente_id')
              ->references('id')->on('clientes')
              ->onDelete('set null');
            $table->unsignedInteger('contrato_id')->nullable();
            $table->foreign('contrato_id')
              ->references('id')->on('contratos')
              ->onDelete('set null');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
