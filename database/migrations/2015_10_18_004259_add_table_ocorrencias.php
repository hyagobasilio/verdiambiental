<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableOcorrencias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ocorrencias', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            
            $table->enum('tipo',['renovacao', 'retirada', 'entrega'])->nullable();
            $table->integer('numero_container');
            $table->integer('id_motorista')->nullable();
            $table->date('data')->nullable();
            $table->decimal('valor_estimado',8,2)->nullable();
            $table->decimal('valor_final'8,2)->nullable();
            $table->string('cidade')->nullable();
            $table->string('endereco');
            $table->string('bairro')->nullable();
            $table->string('cep')->nullable();
            $table->unsignedInteger('cidade_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ocorrencias');
    }
}
