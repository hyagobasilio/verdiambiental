<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldEnderecoIntoOsitem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ordem_servico_item', function (Blueprint $table) {
           

            $table->unsignedInteger('endereco_id')->nullable();
            $table->foreign('endereco_id')
              ->references('id')->on('enderecos')
              ->onDelete('set null');

           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
