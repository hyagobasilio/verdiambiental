<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdemServicoItem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ordem_servico_item', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('numero_cupom');
            $table->string('manifesto');

            $table->unsignedInteger('cliente_id')->nullable();
            $table->foreign('cliente_id')
              ->references('id')->on('clientes')
              ->onDelete('set null');

            $table->unsignedInteger('caixa_id')->nullable();
            $table->foreign('caixa_id')
              ->references('id')->on('containers')
              ->onDelete('set null');

            $table->unsignedInteger('os_id')->nullable();
            $table->foreign('os_id')
              ->references('id')->on('ordem_servicos')
              ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ordem_servico_item');
    }
}
