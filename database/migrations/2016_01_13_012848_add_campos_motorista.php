<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCamposMotorista extends Migration
{
    /**
     * Run the migrations.
     * 1 CADASTRADO DE MOTORISTA COM INFORMAÇÕES BÁSICAS E DATA DE ANIVERSARIO, VALIDADE DE HABILITAÇÃO, DATA DE ENTRADA DA EMPRESA.
     * $table->string('nome');
      *      $table->string('cpf')->nullable();
       *     $table->string('matricula')->nullable();
     * @return void
     */
    public function up()
    {
        Schema::table('motoristas', function (Blueprint $table) {
           $table->string('rg')->nullable();
           $table->date('data_nascimento')->nullable();
           $table->string('email')->nullable();
           $table->string('telefone')->nullable();
           $table->date('data_admissao')->nullable();
           
           $table->string('numero_cnh')->nullable();
           $table->string('categoria_cnh', 3)->nullable();
           $table->date('validade')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
