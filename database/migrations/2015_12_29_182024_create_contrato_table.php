<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContratoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contratos', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('descricao')->nullable();
            $table->enum('material', ['lixo', 'entulho'])->nullable();
            $table->string('quantidade')->nullable();

            $table->string('concessao_recipiente', 15)->nullable();
            
            $table->enum('tipo_contrato', ['mensal', 'remocao'])->nullable();
            $table->double('valor', 8,2)->nullable();
            $table->tinyInteger('qtd_remocoes_sema')->nullable();

            $table->boolean('segunda')->default(false);
            $table->boolean('terca')->default(false);
            $table->boolean('quarta')->default(false);
            $table->boolean('quinta')->default(false);
            $table->boolean('sexta')->default(false);
            $table->boolean('sabado')->default(false);

            $table->tinyInteger('prazo_vigencia')->nullable();

            $table->integer('cliente_id')->unsigned()->nullable();
            $table->foreign('cliente_id')
              ->references('id')->on('clientes')
              ->onDelete('set null');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contratos');
    }
}
