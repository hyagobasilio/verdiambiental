<?php
    
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsOnItensRotaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('itens_rota', function (Blueprint $table) {
           $table->enum('estado', ['limpo', 'sujo'])->nullable();
           $table->enum('tipo', ['renovacao', 'retirada', 'entrega'])->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('itens_rota', function (Blueprint $table) {
           $table->removeColumn('estado');
           $table->removeColumn('tipo');
        });
    }
}
