<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMovimentacaoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movimentacoes_fc', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->date('data_mov');
            $table->double('valor', 10, 2);
            $table->enum('tipo_mov', ['S', 'E']);

            $table->integer('item_id')->unsigned()->nullable();
            $table->foreign('item_id')
              ->references('id')->on('item_mov')->nullable()
              ->onDelete('set null');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('movimentacoes_fc');
    }
}
