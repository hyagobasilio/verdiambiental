<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AdicionarCampoTabelaOcorrencia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ocorrencias', function (Blueprint $table) {
            $table->integer('prazo')->nullable();
            $table->string('cliente')->nullable();
            $table->integer('cliente_id')->nullable();
            $table->string('observacao')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ocorrencias', function (Blueprint $table) {
            $table->removeColumn('prazo');
            $table->removeColumn('cliente');
            $table->removeColumn('cliente_id');
            $table->removeColumn('observacao');
        });
    }
}
