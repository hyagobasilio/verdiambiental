<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldRemoveCxIdIntoOsitem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ordem_servico_item', function (Blueprint $table) {
           

            $table->unsignedInteger('remove_cx_id')->nullable();
            $table->foreign('remove_cx_id')
              ->references('id')->on('containers')
              ->onDelete('set null');

           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
