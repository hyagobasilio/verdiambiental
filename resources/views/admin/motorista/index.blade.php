@extends('adminlte')
@section('content-header')
<h1>
    Motorista
    <small>visualização</small>
</h1>

<!-- You can dynamically generate breadcrumbs here -->
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Motorista</a></li>
    <li class="active">Visualização</li>
</ol>
@endsection
{{-- Content --}}
@section('content')
    
<br>
<div class="row">
    <div class="col-md-12">
        
        <div class="box box-primary">
            <div class="box-header ui-sortable-handle" style="cursor: move;">
                <!-- <i class="ion ion-clipboard"></i> -->

                <h3 class="box-title">Motoristas</h3>
                <div class="pull-right">
                    <a href="{{{ url('admin/motorista/create') }}}"
                        class="btn btn-sm  btn-success"><span
                            class="glyphicon glyphicon-plus-sign"></span> Novo Motorista</a>
                </div>
                
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                {!! Form::open( array('url' => url('admin/motorista'), 'method' => 'get', 'class' => 'bf')) !!}
                    <div class="row">
                    
                       <!-- Nome -->
                       <div class="col-sm-3">
                           <div class="form-group {{ $errors->has('nome') ? 'has-error' : '' }}">
                               {!! Form::label('nome','Nome', array('class' => 'control-label')) !!}
                               {!! Form::text('nome', Input::get('nome'), array('class' => 'form-control input-sm')) !!}
                               <span class="help-block">{{ $errors->first('nome', ':message') }}</span>
                           </div>
                       </div>
                       
                       <!-- CPF -->
                       <div class="col-sm-3">
                           <div class="form-group {{ $errors->has('cpf') ? 'has-error' : '' }}">
                               {!! Form::label('cpf','CPF', array('class' => 'control-label')) !!}
                               {!! Form::text('cpf', Input::get('cpf'), array('class' => 'form-control input-sm')) !!}
                               <span class="help-block">{{ $errors->first('cpf', ':message') }}</span>
                           </div>
                       </div>
                       <!-- Tipo -->
                       <?php $opcoes = ['' => 'Selecione', 'motorista' => 'Motorista','ajudante' => 'Ajudante']; ?>
                       <div class="col-sm-3">
                           <div class="form-group {{ $errors->has('tipo') ? 'has-error' : '' }}">
                               {!! Form::label('tipo','Tipo', array('class' => 'control-label')) !!}
                               {!! Form::select('tipo', $opcoes, null, ['class' => 'form-control input-sm']) !!}
                               <span class="help-block">{{ $errors->first('tipo', ':message') }}</span>
                           </div>
                       </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3">
                
                            <button type="submit" class="btn btn-success btn-sm">
                                <span class="glyphicon glyphicon-search"></span> 
                                Pesquisar
                            </button>
                        </div>
                    </div>


                </form>

                <table class="table">
                    <thead>
                        <tr>
                            <th>Cód.</th>
                            <th>matricula</th>
                            <th>Nome</th>
                            <th>Telefone</th>
                            <th>Tipo</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($motoristas as $motorista)
                        <tr>
                            <td>{{ $motorista->id }}</td>
                            <td>{{ $motorista->matricula }}</td>
                            <td>{{ $motorista->nome }}</td>
                            <td>{{ $motorista->telefone }}</td>
                            <td>{{ $motorista->tipo }}</td>
                            <td>
                                <a href="{{{ url('admin/motorista/' . $motorista->id ) }}}" class="iframe btn btn-success btn-xs">
                                    <i class="fa fa-eye"></i>
                                    Visualizar
                                </a>
                                <a href="{{{ url('admin/motorista/' . $motorista->id . '/edit' ) }}}" class="btn btn-success btn-xs">
                                    <i class="fa fa-edit"></i>
                                    Editar
                                </a>
                            
                            <a href="{{{ url('admin/motorista/' . $motorista->id . '/delete' ) }}}" class="iframe btn btn-danger btn-xs">
                                <i class="fa fa-trash-o"></i>
                                Remover
                            </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix no-border">
                <div class="box-tools pull-right">
                    <?php echo $motoristas->appends(Request::except('page'))->render(); ?>
                </div>
                
            </div>
          </div>
    </div>
</div>
@endsection

{{-- Scripts --}}
@section('scripts')

<script>
    
    $(document).ready(function(){


        $(".iframe").colorbox({
            iframe: true,
            width: "80%",
            height: "80%"
        });
    });
</script>
   
@endsection
