{{-- @extends('admin.layouts.modal2') --}}

@section('content')


		<div class="box box-success">
	        <div class="box-header with-border">
	          <h3 class="box-title">Cadastro de Cargo</h3>
	        </div><!-- /.box-header -->
	        <!-- form start -->
	    
	      	@if (isset($cargo))
			{!! Form::model($cargo, array('url' => url('admin/cargo/' . $cargo->id . '/edit'), 'method' => 'put', 'class' => 'form-horizontal bf', 'files'=> true)) !!}
			@else
			{!! Form::open(array('url' => url('admin/cargo'), 'method' => 'post', 'class' => 'form-horizontal bf', 'files'=> true)) !!}
			@endif
			

			
	          <div class="box-body">


                <div class="form-group  {{ $errors->has('descricao') ? 'has-error' : '' }}">
                 {!! Form::label('descricao','Função', array('class' => 'col-sm-2 control-label')) !!}
                  <div class="col-sm-10">
                    {!! Form::text('descricao', null, array('class' => 'form-control input-sm')) !!}
                     <span class="help-block">{{ $errors->first('descricao', ':message') }}</span>
                  </div>
                </div>

	          	</div><!-- /.box-body -->
	          	<div class="box-footer">
	          		<a href="{{ url('/admin/cargo') }}" class="btn btn-sm btn-danger close_popup">
						<span class="glyphicon glyphicon-ban-circle"></span> 
					Cancelar
					</a>
					<button type="reset" class="btn btn-sm btn-default">
						<span class="glyphicon glyphicon-remove-circle"></span> 
						Limpar
					</button>

		            <button type="submit" class="btn btn-success pull-right">
						<span class="glyphicon glyphicon-ok-circle"></span> 
						    @if	(isset($cargo))
						        Alterar
						    @else
						        Salvar
						    @endif
					</button>
	          </div><!-- /.box-footer -->
	        </form>
	      </div><!-- /.box -->

@stop
