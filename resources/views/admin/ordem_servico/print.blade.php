@extends('admin.layouts.modal')

{{-- Content --}}
@section('content')


<div class="row">
<div class="col-md-12">
<table id="table" class="table table-bordered" cellpadding="0" cellspacing="0">
    
    </thead>
        <tbody>
            <tr>   
                <td>
                    <b>Tipo</b>
                </td>
                <td>{{ $os->tipo }}</td>
                <td><b>Cliente</b></td>
                <td>{{$os->cliente->id . ' - '. $os->cliente->nome . $os->cliente->nome_fantasia }}</td>
                <td><b>CPF / CNPJ</b></td>
                <td>{{ $os->cliente->cpf . $os->cliente->cnpj }}</td>
            </tr>
            <tr>   
                <td><b>Motorista</b></td>
                <td>{{ $os->motorista->id . ' - '. $os->motorista->nome }}</td>
                <td><b>Valor Estimado</b></td>
                <td>{{ $os->valor_estimado }}</td>
                <td><b>Valor Final</b></td>
                <td>{{ $os->valor_final }}</td>
            </tr>
            <tr>
                <td><b>Número Container</b></td>
                <td>{{ $os->numero_container }}</td>
                <td><b>Data</b></td>
                <td>{{ $os->data }}</td>   
                <td><b>Prazo</b></td>
                <td>{{ $os->prazo }}</td>
            </tr>
            <tr>   
                <td><b>Cidade</b></td>
                <td>{{ $os->cidade }}</td>
                <td><b>Endereço</b></td>
                <td>{{ $os->endereco }}</td>
                <td><b>Bairro</b></td>
                <td>{{ $os->valor_final }}</td>
            </tr>
            <tr>
                <td><b>CEP</b></td>
                <td>{{ $os->cep }}</td>
                <td><b>Observação</b></td>
                <td colspan="3">{{ $os->observacao }}</td>
            </tr>
        </tbody>
    </table>
</div>
</div>

<button class="btn btn-success btn-print-now"><i class="fa fa-download"></i> Gerar PDF</button>



@endsection


