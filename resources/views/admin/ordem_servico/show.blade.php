{{-- @extends('admin.layouts.modal2') --}}
@section('styles')
<style>
	#table tbody  tr td:first-child {
		font-weight: bold;
		width: 15%;
	}
</style>
@endsection
@section('content')


		<div class="box box-success">
	        <div class="box-header with-border">
	        	<img width="55" height="50" src="{{ asset('/assets/images/verdi-ambiental.png') }}" alt="Verdi Ambiental">
	          <h3 class="box-title">OS</h3>
	          <div class="pull-right" style="text-align:right">
	          	{{ date('d/m/Y') }} <br> {{ date('h:i:s') }}
	          </div>

	        </div><!-- /.box-header -->

          	<div class="box-body">

			<table id="table" class="table table-bordered table-condensed">
				<tbody>
					<tr>
						<td>Cód.</td>
						<td>{{ $ordemservico->id}}</td>
					</tr>
					<tr>
						<td>Data Abertura</td>
						<td>{{ $ordemservico->data_abertura}}</td>
					</tr>
					<tr>
						<td>Serviço</td>
						<td>{{ $ordemservico->servico}}</td>
					</tr>
					<tr>
						<td>Tipo</td>
						<td>{{ $ordemservico->tipo}}</td>
					</tr>
					<tr>
						<td>Prazo</td>
						<td>{{ $ordemservico->prazo}}</td>
					</tr>
					<tr>
						<td>Motorista</td>
						<td>{{ $ordemservico->motorista->nome}}</td>
					</tr>
					<tr>
						<td>Ajudante</td>
						<td>@if(isset($ordemservico->ajudante)){{ $ordemservico->ajudante->nome}}@endif</td>
					</tr>
					<tr>
						<td>Solicitante</td>
						<td>@if(isset($ordemservico->solicitante)){{ $ordemservico->solicitante->nome}}@endif</td>
					</tr>
					<tr>
						<td>Caminhão</td>
						<td>{{ $ordemservico->caminhao->caminhao . ' - ' . $ordemservico->caminhao->placa}}</td>
					</tr>
					<tr>
						<td>Observação</td>
						<td>{{ $ordemservico->observacao }}</td>
					</tr>
					<tr>
						<td>Nº do Ticket</td>
						<td>{{ $ordemservico->numero_ticket}}</td>
					</tr>
					<tr>
						<td>Peso Kg.</td>
						<td>{{ $ordemservico->peso}}</td>
					</tr>
					<tr>
						<td>Status da OS</td>
						<td>@if($ordemservico->status == 1) <span class="label label-success">Concluído</span> @else <span class="label label-warning">Aberto</span>@endif</td>
					</tr>
				</tbody>
			</table>
			<hr>

			<div class="box box-success">
	            <div class="box-header">
	              <h3 class="box-title">Itens</h3>
	            </div>
	            <!-- /.box-header -->
	            <div class="box-body no-padding">
	                <table id="tabela-rotas" class="table table-striped">
	                    <thead>
	                        <tr>
	                            <th>Cliente</th>
	                            <th>CPF/CNPJ</th>
	                            <th>Endereço</th>
	                            <th>Caixa Retirada</th>
	                            <th>Caixa Entrega</th>
	                            <th>Nº do cupom</th>
	                            <th>Manifesto</th>
	                            <th>Tipo Entulho</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                    @foreach($ordemservico->itens as $item)
	                    <tr>
	                    	<td>{{ $item->nome }}</td>
	                    	<td>{{ $item->cpf_cnpj }}</td>
	                    	<td>{{ !empty($item->cidade) ? $item->cidade . '<br>' : '' }} 
	                    	{{ $item->endereco . ' - ' . $item->bairro }} @if(!empty($item->cep)) <b>CEP:</b> {{ $item->cep }}@endif <br>
	                    	{{$item->complemento}}
	                    	</td>
	                    	<td>{{ $item->cx_rm }}</td>
	                    	<td>{{ $item->cx_entrega }}</td>
	                    	<td>{{ $item->numero_cupom }}</td>
	                    	<td>{{ $item->manifesto }}</td>
	                    	<td>{{ $item->tipo_entulho }}</td>
	                    </tr>
	                    @endforeach
	                    </tbody>
	                </table>
	            </div>
	            <!-- /.box-body -->
	          </div>
	            


          	</div><!-- /.box-body -->
          	<div class="box-footer">
          		@if(empty(Input::get('imprimir')))
				<a href="?imprimir=ok" class="btn btn-xs btn-success" target="_blank"><span class="glyphicon glyphicon-save"></span> PDF</a>
          		
				@endif

	            
          	</div><!-- /.box-footer -->
	        
	      </div><!-- /.box -->

@stop
{{-- Scripts --}}
@section('scripts')

<script>

</script>
@parent

@endsection
