{{-- @extends('adminlte') --}}

@section('head')
<style>
    .lista-rotas {
        border: 1px solid #eee;
        padding: 0;
    }
    .lista-rotas > li {
        padding: 3px 10px;
        list-style: none;
    }
</style>
@endsection
@section('content-header')
<h1>
   .
</h1>

<!-- You can dynamically generate breadcrumbs here -->
<ol class="breadcrumb">
    <li><a href="{{ url('admin/ordemservico') }}"><i class="fa fa-dashboard"></i> Ordem de serviço</a></li>
    <li class="active">{{ isset($ordemservico) ? "Editar" : "Cadastro" }}</li>
</ol>

@endsection
@section('content')



<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Cadastro de Ordem de Serviço</h3>
    </div><!-- /.box-header -->
    <!-- form start -->
    @if (isset($ordemservico))
	{!! Form::model($ordemservico, array('url' => url('admin/ordemservico/' . $ordemservico->id . '/edit'), 'method' => 'put', 'class' => 'bf', 'files'=> true)) !!}
	@else
	{!! Form::open(array('url' => url('admin/ordemservico'), 'method' => 'post', 'class' => ' bf', 'files'=> true)) !!}
	@endif

		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
        <input type="hidden" name="itens_rota" id="itens_rota" value="{{ isset($ordemservico) ? $ordemservico->rotas : ''}}{{ old('itens_rota') }}">
        <div class="box-body">
				       

            <div class="row">

                <div class="col-md-2">
                    <?php $opcoes = ['entulho' => 'Entulho','lixo' => 'Lixo']; ?>
                    <div class="form-group {{ $errors->has('servico') ? 'has-error' : '' }}">
                        {!! Form::label('servico','Serviço', array('class' => 'control-label')) !!}
                        {!! Form::select('servico', $opcoes, null, ['class' => 'form-control input-sm']) !!}
                        <span class="help-block">{{ $errors->first('servico', ':message') }}</span>          
                    </div>
                </div>

                <!-- Entulho -->
                <div class="entulho">
                    
                    <!-- Tipo -->
                    <div class="col-md-2">
                        <?php $opcoes = ['' => 'Selecione','entrega' => 'Entrega', 'remocao' => 'Remoção', 'retirada' => 'Retirada']; ?>
                        <div class="form-group {{ $errors->has('tipo') ? 'has-error' : '' }}">
                            {!! Form::label('tipo','Tipo', array('class' => 'control-label')) !!}
                            {!! Form::select('tipo', $opcoes, null, ['class' => 'form-control input-sm']) !!}
                            <span class="help-block">{{ $errors->first('tipo', ':message') }}</span>          
                        </div>
                    </div>

                    <!-- Prazo -->
                    <div class="col-sm-2 prazo">
                        <div class="form-group {{ $errors->has('prazo') ? 'has-error' : '' }}">
                            {!! Form::label('prazo','Prazo', array('class' => 'control-label')) !!}
                            {!! Form::text('prazo', null, array('class' => 'form-control input-sm data')) !!}
                            <span class="help-block">{{ $errors->first('prazo', ':message') }}</span>
                        </div>
                    </div>
                </div>

                <!-- Motoristas -->
                <div class="col-sm-3">
                    <div class="form-group {{ $errors->has('motorista_id') ? 'has-error' : '' }}">
                        {!! Form::label('motorista_id','Motorista', array('class' => 'control-label')) !!}
                        {!! Form::select('motorista_id', $motoristas, null, ['class' => 'form-control input-sm']) !!}
                        <span class="help-block">{{ $errors->first('motorista_id', ':message') }}</span>
                    </div>
                </div>
            
                <!-- Ajudantes -->
                <div class="col-sm-3">
                    <div class="form-group {{ $errors->has('ajudante_id') ? 'has-error' : '' }}">
                        {!! Form::label('ajudante_id','Ajudante', array('class' => 'control-label')) !!}
                        {!! Form::select('ajudante_id', $ajudantes, null, ['class' => 'form-control input-sm']) !!}
                        <span class="help-block">{{ $errors->first('ajudante_id', ':message') }}</span>
                    </div>
                </div>

                
            </div>
            <div class="row">
                
                <!-- Ajudantes -->
                <div class="col-sm-3">
                    <div class="form-group {{ $errors->has('caminhao_id') ? 'has-error' : '' }}">
                        {!! Form::label('caminhao_id','Caminhão', array('class' => 'control-label')) !!}
                        {!! Form::select('caminhao_id', $carros, null, ['class' => 'form-control input-sm']) !!}
                        <span class="help-block">{{ $errors->first('caminhao_id', ':message') }}</span>
                    </div>
                </div>
                
                <?php $dataAbertura = !isset($ordemservico) ? date('d/m/Y'): null; ?>

                <!-- Data Abertura -->
                <div class="col-sm-2">
                    <div class="form-group {{ $errors->has('data_abertura') ? 'has-error' : '' }}">
                        {!! Form::label('data_abertura','Data Abertura', array('class' => 'control-label')) !!}
                        {!! Form::text('data_abertura', $dataAbertura, array('class' => 'form-control input-sm')) !!}
                        <span class="help-block">{{ $errors->first('data_abertura', ':message') }}</span>
                    </div>
                </div>

            

                <div class="col-sm-4">
                    <div class="form-group  {{ $errors->has('observacao') ? 'has-error' : '' }}">
                        {!! Form::label('observacao','Observação', array('class' => 'control-label')) !!}
                        {!! Form::text('observacao', null, array('class' => 'form-control input-sm')) !!}
                        <span class="help-block">{{ $errors->first('observacao', ':message') }}</span>
                    </div>
                </div>

                <!-- Multiple Radios (inline) -->
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="" for="radios">Status da OS</label>
                        <br>              
                        <label class="radio-inline" for="radios-0">
                          <input type="radio" name="status" id="radios-0" value="0" @if(!isset($ordemservico) || (isset($ordemservico) && $ordemservico->status == 0))checked="checked"@endif>
                          Aberto
                        </label> 
                        <label class="radio-inline" for="radios-1">
                          <input type="radio" name="status" id="radios-1" value="1" @if( isset($ordemservico) && $ordemservico->status == 1) checked="checked"@endif >
                          Concluída
                        </label>
                            
                      
                    </div>
                </div>
                </div>
                <div class="row">
                
                <!-- Número Ticket -->
                <div class="col-md-3">
                    <div class="form-group  {{ $errors->has('numero_ticket') ? 'has-error' : '' }}">
                        {!! Form::label('numero_ticket', 'Número ticket', array('class' => 'control-label')) !!}
                        {!! Form::text('numero_ticket', null, array('class' => 'form-control')) !!}
                        <span class="help-block">{{ $errors->first('numero_ticket', ':message') }}</span>
                    </div>
                </div>

                <!-- Peso -->
                <div class="col-md-3">
                    <div class="form-group  {{ $errors->has('peso') ? 'has-error' : '' }}">
                        {!! Form::label('peso', 'Peso em kg', array('class' => 'control-label')) !!}
                        {!! Form::text('peso', null, array('class' => 'valor form-control')) !!}
                        <span class="help-block">{{ $errors->first('peso', ':message') }}</span>
                    </div>
                </div>
                <!-- Peso -->
                <div class="col-md-3">
                    <div class="form-group  {{ $errors->has('solicitante') ? 'has-error' : '' }}">
                        {!! Form::label('solicitante', 'Solicitante', array('class' => 'control-label')) !!}
                        {!! Form::text('solicitante', Auth::user()->name, array('disabled' => 'true', 'class' => 'form-control')) !!}
                        <span class="help-block">{{ $errors->first('solicitante', ':message') }}</span>
                    </div>
                </div>

                <!-- Lixo -->
                <div class="lixo col-sm-12">
                    
                    <!-- Rotas -->
                    <div class="col-sm-6">
                        <div class="form-group @if(!isset($ordemservico)) has-warning @endif">
                            <?php 
                            /*
                            * Apenas permite salvar Rotas se a OS já estiver salva
                            */
                            ?>
                            @if(!isset($ordemservico))
                            <label>Para inserir rotas é necessario salvar esta OS primeiro!</label>
                            @else
                            
                            <div class="input-group">
                                <a href="{{ url('admin/ordemservicoitem/'.$ordemservico->id.'/create'.'?tipo='.$ordemservico->tipo.'&servico='.$ordemservico->servico)   }}" class="btn btn-success btn-sm iframe">Adicionar Item</a>
                            </div>
                            @endif
                            
                        </div>
                    </div>

                    <div class="col-md-12">


                    <!-- .Box -->  
                    <div class="box box-success">
                        <div class="box-header">
                          <h3 class="box-title">Rotas</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body no-padding">
                            <table id="tabela-rotas" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Cliente</th>
                                        <th>CPF/CNPJ</th>
                                        <th>Caixa Entrega</th>
                                        <th>Caixa Remoção</th>
                                        <th>Nº do cupom</th>
                                        <th>Manifesto</th>
                                        <th>Tipo Entulho</th>
                                        <th>Ações</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                      </div>
                        
                    </div>


                <!-- ./lixo -->
            </div>
			<!-- fim dos campos -->
        </div><!-- /.box-body -->
        <div class="box-footer">
			

            <button type="submit" class="btn btn-success btn-sm pull-right">
				<span class="glyphicon glyphicon-ok-circle"></span> 
				    @if	(isset($ocorrencia))
				        Editar
				    @else
				        Salvar
				    @endif
			</button>
        </div><!-- /.box-footer -->
    </form>
</div><!-- /.box -->

@section('scripts')
<script>
function habilitaResiduoOrLixo(){
    $('.lixo').show();
    $('.entulho').show();
    if($('#servico').val() == 'entulho') {
        $('.lixo').hide();
    }else {
        $('.entulho').hide();
    }
}

function dataToView(data) {
    d = data.split('-');
    return d[0]+'/'+d[1]+'/'+d[2];

}
function montaItensRota() {
    rotas = $('#itens_rota').val();
    if (rotas != '' ) {
        dados = JSON.parse(rotas);
        $.each(dados, function(i, iten) {

            $('<option>', {
                value: iten,
                html: dataToView(iten)
            }).appendTo('#lista-rotas');

        });
    }
}


function removerItensRota()
{
    var selecionados = $("#lista-rotas option:selected");
    for(i = 0; i < selecionados.length; i++) {
        selecionados[i].remove();
    }
}
function adicionarDataRota() {
    el = $('#data-rota')
    data = el.val();
    el.val('');
    dataFormatoBanco = dataToBd(data);
    if(data != undefined && data != '') {

       
        if(!contains(dataFormatoBanco, "#lista-rotas option")) {
            
            $('<option>', {
                value: dataFormatoBanco,
                html: data
            }).appendTo('#lista-rotas');
        }
        
    }
}
function contains(valor, selector) {
    lista = $(selector);
    for(i = 0; i < lista.length; i++) {
        if(lista[i].value == valor) {
            return true;
        }
    }
    return false;
}
function dataToBd(data) {
    //console.log(data);
    d = data.split('/');
    return d[2]+'-'+d[1]+'-'+d[0];
}
$(function() {

    $('#servico').on('change', function(){
        selected = $('#servico option:selected').val();
        if($('#servico').val() == 'lixo') {
            $('#prazo').val('');
            $('.prazo').hide({deley: 1000});
        }else {
            $('.prazo').show({deley: 1000});
        }
    });

    $('#cliente-os').typeahead({
         source: function(q, process) {
            objects = [];
            map = {};
            // requisição ajax
            

            return $.get("{{ url('admin/cliente/dados/os') }}", 
                { 
                    query: q 
                }, function (data) {
                    objects = [];
                    $.each(data, function(i, object) {
                        var chave = object.nome + " " + object.nome_fantasia;
                        map[chave] = object;
                        objects.push(chave);
                    });
                    return process(objects);
                });
            // fim ajax
            
           
            process(objects);
        },
        updater: function(item) {
            
            $("#cliente_id").val(map[item].id);
            $("#cidade_id").val(map[item].cidade_id);
            $("#cidade").val(map[item].cidade + ' - ' + map[item].uf);
            $("#bairro").val(map[item].bairro);
            $("#cep").val(map[item].cep);
            $("#endereco").val(map[item].endereco);
            return item;
        }
    });

    $(".iframe").colorbox({
        iframe: true,
        width: "80%",
        height: "80%"
    });
    
    
    @if(isset($ordemservico))
        var oTable;
        oTable = $('#tabela-rotas').DataTable({
            "sDom": "<'row'<'col-md-6'l><'col-md-6'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
            "sPaginationType": "full_numbers",
            "pageLength": 5,
            "oLanguage": {
                "sProcessing": "Processando",
                "sLengthMenu": "",
                "sZeroRecords": "Nenhum resultado",
                "sInfo": "",
                "sEmptyTable": "Tabela vazia",
                "sInfoEmpty": "Vazio",
                "sInfoFiltered": "Filtro",
                "sInfoPostFix": "",
                "sSearch": "Pesquisar:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Início",
                    "sPrevious": "Anterior",
                    "sNext": "Próximo",
                    "sLast": "Último"
                }
            },
            "processing": true,
            "serverSide": true,
            "ajax": "{{ url('admin/ordemservicoitem/data/'.$ordemservico->id)}}",
            "fnDrawCallback": function (oSettings) {
                $('.paginate_button').addClass('btn-sm');
            
                $(".iframe").colorbox({
                    iframe: true,
                    width: "80%",
                    height: "80%",
                    onClosed: function () {
                        oTable.ajax.reload();
                    }
                });
                
            }
        });
    @endif



});
</script>
@endsection


@stop
