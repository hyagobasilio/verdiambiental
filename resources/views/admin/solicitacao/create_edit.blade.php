{{-- @extends('admin.layouts.modal2') --}}
@section('content-header')
<h1>
    Soliitação
    <small>{{ isset($solicitacao) ? "Editar" : "Cadastro" }}</small>
</h1>

<!-- You can dynamically generate breadcrumbs here -->
<ol class="breadcrumb">
    <li><a href="{{ URL::to('admin/users') }}"><i class="fa fa-dashboard"></i> Usuário</a></li>
    <li class="active">{{ isset($user) ? "Editar" : "Cadastro" }}</li>
</ol>
@endsection
@section('content')


		<div class="box box-success">
	        <div class="box-header with-border">
	          <h3 class="box-title">Cadastro de Solicitação</h3>
	        </div><!-- /.box-header -->
	        <!-- form start -->
	    
	      	@if (isset($solicitacao))
			{!! Form::model($solicitacao, array('url' => URL::to('admin/solicitacao/' . $solicitacao->id . '/edit'), 'method' => 'put', 'class' => 'form-horizontal bf', 'files'=> true)) !!}
			@else
			{!! Form::open(array('url' => URL::to('admin/solicitacao'), 'method' => 'post', 'class' => 'form-horizontal bf', 'files'=> true)) !!}
			@endif
			<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />

			
	          <div class="box-body">	
	          <!-- <input type="hidden" name="_token" value="{{{ csrf_token() }}}" /> -->

	       		

	            <div class="form-group  {{ $errors->has('titulo') ? 'has-error' : '' }}">
                 {!! Form::label('titulo','Nome', array('class' => 'col-sm-2 control-label')) !!}
                  <div class="col-sm-10">
                    {!! Form::text('titulo', null, array('class' => 'form-control')) !!}
                     <span class="help-block">{{ $errors->first('titulo', ':message') }}</span>
                  </div>
                </div>
				<?php $opcoess = ['' => 'Selecione', 'motorista' => 'Motorista','ajudante' => 'Ajudante']; ?>
				
				<?php $opcoes = ['pendente' => 'Pendente','concluida' => 'Concluída']; ?>
                <div class="form-group  {{ $errors->has('status') ? 'has-error' : '' }}">
                 {!! Form::label('status','Nome', array('class' => 'col-sm-2 control-label')) !!}
                  <div class="col-sm-10">
                     {!! Form::select('status', $opcoes, null, ['class' => 'form-control input-sm']) !!}
                     <span class="help-block">{{ $errors->first('status', ':message') }}</span>
                  </div>
                </div>

                @if(!isset($solicitacao))
                	<input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                @endif

                <div class="form-group">
                    <label class="col-sm-2 control-label" for="radios">Status da OS</label>
					<div class="col-sm-10">
	                    <label class="radio-inline" for="radios-0">
	                      <input type="radio" name="prioridade" id="radios-0" value="baixa" @if(!isset($solicitacao) || (isset($solicitacao) && strcmp($solicitacao->prioridade ,'baixa') == 0) || empty($solicitacao->prioridade))checked="checked"@endif>
	                      Baixa
	                    </label> 
	                    <label class="radio-inline" for="radios-1">
	                      <input type="radio" name="prioridade" id="radios-1" value="media" @if( isset($solicitacao) && strcmp($solicitacao->prioridade ,'media') == 0 ) checked="checked"@endif >
	                      Média
	                    </label> 
	                    <label class="radio-inline" for="radios-1">
	                      <input type="radio" name="prioridade" id="radios-1" value="alta" @if( isset($solicitacao) && strcmp($solicitacao->prioridade ,'alta') == 0) checked="checked"@endif >
	                      Alta
	                    </label>
					</div>
					<textarea name="descricao" id="descriao" cols="30" rows="10" class="textarea"
					style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">
					@if(isset($solicitacao))
					{{ $solicitacao->descricao}}
					@endif
					</textarea>         
                </div>


	            
				
				


	          </div><!-- /.box-body -->
	          <div class="box-footer">
	          	<a href="{{ URL::to('/admin/users') }}" class="btn btn-sm btn-danger close_popup">
					<span class="glyphicon glyphicon-ban-circle"></span> 
					Cancelar
				</a>
				<button type="reset" class="btn btn-sm btn-default">
					<span class="glyphicon glyphicon-remove-circle"></span> 
					Limpar
				</button>

	            <button type="submit" class="btn btn-success pull-right">
					<span class="glyphicon glyphicon-ok-circle"></span> 
					    @if	(isset($user))
					        Alterar
					    @else
					        Salvar
					    @endif
				</button>
	          </div><!-- /.box-footer -->
	        </form>
	      </div><!-- /.box -->

@stop
