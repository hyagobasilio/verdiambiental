@extends('adminlte')
@section('content-header')
<h1>
    Solicitação
    <small>visualização</small>
</h1>

<!-- You can dynamically generate breadcrumbs here -->
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Solicitação</a></li>
    <li class="active">Visualização</li>
</ol>
@endsection
{{-- Content --}}
@section('content')
    <div class="row">
        <div class="col-md-12">
            
            <div class="pull-right">
                <a href="{{{ URL::to('admin/solicitacao/create') }}}"
                   class="btn btn-sm  btn-success iframe"><span
                            class="glyphicon glyphicon-plus-sign"></span> Nova Solicitação</a>
            </div>
        </div>
    </div>
<br>
<div class="row">
    <div class="col-md-6">
        
        <div class="box box-primary">
            <div class="box-header ui-sortable-handle" style="cursor: move;">
                <i class="ion ion-clipboard"></i>

                <h3 class="box-title">Solicitações para o sistema</h3>

                <div class="box-tools pull-right">
                    <?php echo $solicitacoes->appends(Request::except('page'))->render(); ?>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
           
                <table class="table">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Titulo</th>
                            <th>Responsável</th>
                            <th>Prioridade</th>
                            <th>Status</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php 
                        function nomeLabel($prioridade) {
                            if (strcmp($prioridade, 'baixa') == 0 )
                            {
                                return 'label-success';
                            }
                            if (strcmp($prioridade, 'media') == 0)
                            {
                               return 'label-warning';
                            }
                            if (strcmp($prioridade, 'alta') == 0)
                            {
                               return 'label-danger';
                            }
                            return '';
                        }



                     ?>
                    
                        @foreach($solicitacoes as $solicitacao)
                        
                        <tr>
                            <td>{{ $solicitacao->id }}</td>
                            <td>{{ $solicitacao->titulo }}</td>
                            <td>
                            @if(!is_null($solicitacao->user_id)) 
                            {{ $solicitacao->name}} 
                            @endif
                            </td>
                            <td>
                                <small class="label {{ nomeLabel($solicitacao->prioridade) }}">    
                                {{ $solicitacao->prioridade }}
                                </small>
                            </td>
                            <td>
                                @if($solicitacao->status == 'pendente')
                                <small class="label label-warning">Pendente</small>
                                @elseif($solicitacao->status == 'concluida')
                                <small class="label label-success">Concluída</small>
                                @endif
                            </td>
                            <td>
                                <a href="{{{ URL::to('admin/solicitacao/' . $solicitacao->id . '/edit' ) }}}" class="iframe">
                                <i class="fa fa-edit"></i>
                                </a>
                            
                                <a href="{{{ URL::to('admin/solicitacao/' . $solicitacao->id . '/delete' ) }}}" class="iframe">
                                    <i class="fa fa-trash-o"></i>
                                </a>

                                <a href="{{{ URL::to('admin/solicitacao/'.$solicitacao->id ) }}}" class="iframe">
                                    <span class="glyphicon glyphicon-eye-open"></span>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix no-border">
                <a href="{{{ URL::to('admin/solicitacao/create') }}}"
                    class="btn btn-sm  btn-success pull-right iframe"><span
                            class="fa fa-plus"></span> Nova Solicitação</a>
               <!--  <button type="button" class="btn btn-default pull-right"><i class="fa fa-plus"></i> Add item</button> -->
            </div>
          </div>
    </div>
</div>
@endsection

{{-- Scripts --}}
@section('scripts')

<script>
    
    $(document).ready(function(){


        $(".iframe").colorbox({
            iframe: true,
            width: "80%",
            height: "80%"
        });
    });
</script>
   
@endsection
