{{-- @extends('admin.layouts.modal2') --}}
@section('styles')
<style>
	table tbody  tr td:first-child {
		font-weight: bold;
		width: 15%;
	}
</style>
@endsection
@section('content')


		<div class="box box-success">
	        <div class="box-header with-border">
	        	<img width="55" height="50" src="{{ asset('/assets/images/verdi-ambiental.png') }}" alt="Verdi Ambiental">
	          <h3 class="box-title">Solicitação</h3>
	          <div class="pull-right" style="text-align:right">
	          	{{ date('d/m/Y') }} <br> {{ date('h:i:s') }}
	          </div>

	        </div><!-- /.box-header -->

          	<div class="box-body">

			<table class="table table-bordered table-condensed">
				<tbody>
					<tr>
						<td>Titulo</td>
						<td>{{ $solicitacao->titulo }}</td>
					</tr>
					<tr>
						<td>Responsável</td>
						<td>
						@if(!is_null($solicitacao->user_id)) 
                            {{ $solicitacao->name}} 
                        @endif
                        </td>
					</tr>
					<tr>
						<td>Prioridade</td>
						<td>{{ $solicitacao->prioridade }}</td>
					</tr>
					<tr>
						<td>Mensagem</td>
						<td>{!! $solicitacao->descricao !!}</td>
					</tr>
				</tbody>
			</table>
	        <hr>
	        <div id="comentarios">
				
	        </div>
           	<form action="{{ url('/admin/solicitacao/'.$solicitacao->id.'/comentar')}}" method="post">
           		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
            	<input class="form-control input-sm" name="descricao" type="text" placeholder="Escreva um comentário">
            	<button type="submit" style="display:none" ></button>
           	</form>

          	</div><!-- /.box-body -->
          	<div class="box-footer">
          		

	            
          	</div><!-- /.box-footer -->
	        
	      </div><!-- /.box -->

@stop
@section('scripts')
<script>

	function getAvatar(filename)
	{
		avatarDefault = '{{ asset("/images/user.png") }}';
		if(filename == ''){
			return avatarDefault;
		}
		avatarUsuario = "{{asset('uploads/users/fotos/thumbs/{filename}')}}";
		avatarUsuario = avatarUsuario.replace('{filename}', filename);
		return avatarUsuario;

	}

	function makeComentario(dados){
		idusuariologado = {{Auth::user()->id}};
		post = '<div class="post">'
	             + '<div class="user-block">'
	                + '<img class="img-circle img-bordered-sm" src="{avatar}" alt="user image">'
	                    +'<span class="username">'
	                      +'<a href="#">{autor}</a>';
	                      if (dados.user_id == idusuariologado){
	                      	link = '{{url("admin/solicitacao/{idComentario}/remover-comentario")}}';
	                      	link = link.replace('{idComentario}', dados.id );

		                    post = post + '<a href="'+link+'" class="pull-right btn-box-tool"><i class="fa fa-trash-o"></i></a>';           	
	                      }
	                    post = post + '</span>'
	                +'<span class="description">{dataPublicacao}</span>'
	            +'</div>'
				+'<p>{descricao}</p>'
			+'</div>';
		post = post.replace('{autor}', dados.name);
		post = post.replace('{dataPublicacao}', dados.created_at);
		post = post.replace('{descricao}', dados.descricao);
		post = post.replace('{avatar}', getAvatar(dados.filename));
		return post;
	}
	function getComentarios() {
		$.get("{{ url('admin/solicitacao/'.$solicitacao->id.'/comentarios') }}", 
		function (data) {
            $('#comentarios').html('');
            $.each(data, function(i, comentario) {
                $('#comentarios').append(makeComentario(comentario));
            });
        });
	}
	$(function(){
		getComentarios();
	})
</script>
@endsection
