@extends('admin.layouts.modal2') @section('content')
{!! Form::model($solicitacao, array('url' => URL::to('admin/solicitacao') . '/' . $solicitacao->id, 'method' => 'delete', 'class' => 'bf', 'files'=> true)) !!}


	<input type="hidden" name="_token" value="{{{ csrf_token() }}}" /> <input
		type="hidden" name="id" value="{{ $solicitacao->id }}" />
	<div class="form-group">
		<div class="controls">
			<p>Deseja realmente exluir ?</p>
			<element class="btn btn-warning btn-sm close_popup">
			<span class="glyphicon glyphicon-ban-circle"></span>Cancelar</element>
			<button type="submit" class="btn btn-sm btn-danger">
				<span class="glyphicon glyphicon-trash"></span> Excluir
			</button>
		</div>
	</div>
</form>
@stop
