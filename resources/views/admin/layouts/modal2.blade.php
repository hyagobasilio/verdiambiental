<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>@section('title') Administration @show</title>
    @section('meta_keywords')
        <meta name="keywords" content="Hyago, Henrique, Basílio"/>
    @show @section('meta_author')
        <meta name="author" content="Hyago Henrique Basílio"/>
    @show @section('meta_description')
        <meta name="Hyago Henrique Basílio"
              content="Sistema de criação de orçamentos"/>
    @show
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="{{ asset("/bower_components/admin-lte/bootstrap/css/bootstrap.min.css") }}" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="{{ asset("/bower_components/admin-lte/dist/css/AdminLTE.min.css")}}" rel="stylesheet" type="text/css" />
    <!-- Editor -->
    <link href="{{ asset("/bower_components/admin-lte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css")}}" rel="stylesheet" type="text/css" />

    @yield('styles')
</head>
<!-- Container -->
<div class="container">
    <!-- <div class="page-header">
        &nbsp;
        <div class="pull-right">
            <button class="btn btn-primary btn-xs close_popup">
                <span class="glyphicon glyphicon-backward"></span> Voltar
            </button>
        </div>
    </div> -->
    <!-- Content -->
    @yield('content')
            <!-- ./ content -->
<!-- jQuery 2.1.4 -->
<script src="{{ asset ("/bower_components/admin-lte/plugins/jQuery/jQuery-2.1.4.min.js") }}"></script>
<!-- Editor -->
<script src="{{ asset ("/bower_components/admin-lte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js") }}"></script>
<script src="{{ asset('js/bootstrap-typeahead.js') }}"></script>
 <!-- InputMask -->
<script src="{{ asset('plugins/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ asset('plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script src="{{ asset('plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
<script src="{{ asset('plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<!-- MaskMoney -->
<script src="{{ asset('js/jquery.maskMoney.min.js') }}"></script>
<script src="{{ asset('js/verdi.js') }}"></script>
    <script type="text/javascript">
        $(function () {
            $('#cliente').typeahead({
         source: function(q, process) {
            objects = [];
            map = {};
            // requisição ajax
            

            return $.get("{{ url('admin/cliente/dados') }}", 
                { 
                    query: q 
                }, function (data) {
                    objects = [];
                    $.each(data, function(i, object) {
                        var chave = object.nome + " " + object.nome_fantasia;
                        map[chave] = object;
                        objects.push(chave);
                    });
                    return process(objects);
                });
            // fim ajax
            
           
            process(objects);
        },
        updater: function(item) {
            
            $("#cliente_id").val(map[item].id);
            $("#cidade_id").val(map[item].cidade_id);
            $("#cidade").val(map[item].cidade + ' - ' + map[item].uf);
            $("#bairro").val(map[item].bairro);
            $("#cep").val(map[item].cep);
            $("#endereco").val(map[item].endereco);
            return item;
        }
    });


    $('#cidade').typeahead({
         source: function(q, process) {
            objects = [];
            map = {};
            // requisição ajax
            

            return $.get("{{ url('admin/cidades') }}", 
                { 
                    query: q 
                }, function (data) {
                    objects = [];
                    $.each(data, function(i, object) {
                        var chave = object.nome + " - " + object.uf;
                        map[chave] = object;
                        objects.push(chave);
                    });
                    return process(objects);
                });
            // fim ajax
            
           
            process(objects);
        },
        updater: function(item) {
            
            $("#cidade_id").val(map[item].id);
            return item;
        }
    });
    
           $(".textarea").wysihtml5();
            //$('textarea').summernote({height: 250});
            $('form').submit(function (event) {
                $('.has-error').removeClass('has-error');
                $('.help-block').remove();
                event.preventDefault();
                var form = $(this);

                if (form.attr('id') == '' || form.attr('id') != 'fupload') {
                    $.ajax({
                        type: form.attr('method'),
                        url: form.attr('action'),
                        data: form.serialize()
                    }).success(function () {
                        setTimeout(function () {
                            parent.$.colorbox.close();
                        }, 10);
                    }).fail(function (jqXHR, textStatus, errorThrown) {
                        // Optionally alert the user of an error here...
                        console.log(jqXHR.responseText);
                        var textResponse = jqXHR.responseText;
                        var alertText = "Erro de validação:\n\n";
                        var jsonResponse = jQuery.parseJSON(textResponse);
                        
                        $.each(jsonResponse.error, function (n, elem) {
                            
                            $("input[name='"+elem.campo+"']").parent().addClass('has-error');
                            $("input[name='"+elem.campo+"']").parent().append('<span class="help-block">'+elem.mensagem+'</span>');
                        });
                        //alert(alertText);
                    });
                }
                else {
                    var formData = new FormData(this);

                    $.ajax({
                        type: form.attr('method'),
                        url: form.attr('action'),
                        data: formData,
                        mimeType: "multipart/form-data",
                        contentType: false,
                        cache: false,
                        processData: false
                    }).success(function (data) {
                        
                        setTimeout(function () {
                            parent.$.colorbox.close();
                        }, 10);

                    }).fail(function (jqXHR, textStatus, errorThrown) {
                        // Optionally alert the user of an error here...
                        var textResponse = jqXHR.responseText;
                        var alertText = "Erro de validação :\n\n";
                        var jsonResponse = jQuery.parseJSON(textResponse);
                            
                        $.each(jsonResponse.error, function (n, elem) {
                            
                            $("input[name='"+elem.campo+"']").parent().addClass('has-error');
                            $("input[name='"+elem.campo+"']").parent().append('<span class="help-block">'+elem.mensagem+'</span>')
                            //alertText = alertText + elem + "\n";
                        });

                        //alert(alertText);
                    });
                }
                ;
            });

            $('.close_popup').click(function () {
                parent.$.colorbox.close();
            });

            $('#cidade').typeahead({
                 source: function(q, process) {
                    objects = [];
                    map = {};
                    // requisição ajax
                    

                    return $.get("{{ url('admin/cidades') }}", 
                        { 
                            query: q 
                        }, function (data) {
                            objects = [];
                            $.each(data, function(i, object) {
                                var chave = object.nome + " - " + object.uf;
                                map[chave] = object;
                                objects.push(chave);
                            });
                            return process(objects);
                        });
                    // fim ajax
                    
                   
                    process(objects);
                },
                updater: function(item) {
                    
                    $("#cidade_id").val(map[item].id);
                    return item;
                }
            });

            
            $(".valor").maskMoney({
                /*symbol:'R$ ', 
                showSymbol:true, */
                thousands:'.', 
                decimal:',', 
                symbolStay: true
            });

        });
    </script>
    @yield('scripts')
</div>
</body>
</html>