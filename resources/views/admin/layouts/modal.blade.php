<!DOCTYPE html>

<html lang="en">

<head id="Starter-Site">

<meta charset="UTF-8">

<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">


<title>Ordem de Serviço</title>

<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
<!-- CSS -->
<link href="{{{ asset('assets/admin/css/bootstrap.min.css') }}}"
	rel="stylesheet" type="text/css">
<link
	href="{{{ asset('assets/admin/css/jquery-ui-1.10.3.custom.css') }}}"
	rel="stylesheet" type="text/css">
<link href="{{{ asset('assets/admin/css/colorbox.css') }}}"
	rel="stylesheet) }}}" type="text/css">
<link href="{{{ asset('assets/admin/css/jquery.multiselect.css') }}}"
	rel="stylesheet" type="text/css">
<link href="{{{ asset('assets/admin/css/style_modal.min.css') }}}"
	rel="stylesheet" type="text/css">
<link href="{{{ asset('assets/admin/css/select2.css') }}}"
	rel="stylesheet" type="text/css">
<link href="{{ asset('assets/admin/css/summernote.css')}}"
	rel="stylesheet" type="text/css">
<link href="{{ asset('assets/admin/css/summernote-bs3.css')}}"
	rel="stylesheet" type="text/css">
<link
	href="{{asset('assets/admin/font-awesome-4.2.0/css/font-awesome.min.css')}}"
	rel="stylesheet" type="text/css">
<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
<!-- start: Favicon and Touch Icons -->
<link rel="shortcut icon"
	href="{{{ asset('assets/admin/ico/favicon.ico') }}}">


<!-- end: Favicon and Touch Icons -->
<style>
	
	* {
		font-family: Arial;
	}

table.table {border-collapse: collapse;}

table.table tr td {
	border:1px solid #ddd;

}
.pull-right {
	display: block;
	float: right;
}
td {
	padding: 4px;
}
.page-header {
	border-bottom: 1px solid #ddd;
	padding: 10px;
	margin-bottom: 20px;
}
</style>
</head>
<body>
	<!-- Container -->
	<div class="container" >
		<div class="page-header" style="margin-top: 5px;">
				
				<div class="pull-right">
					<p>{{ date('d/m/Y') }}</p>
				</div>
		</div>
		<!-- Content -->
		@yield('content')
		<!-- ./ content -->
	</div>
	<!-- ./ container -->
	<!-- start: JavaScript-->
	<!--[if !IE]>-->
	<script src="{{{ asset('assets/admin/js/jquery-2.1.1.min.js') }}}"></script>
	<!--<![endif]-->
	<!--[if IE]>
		<script src="{{{ asset('assets/admin/js/jquery-1.11.1.min.js') }}}"></script>
		<![endif]-->
	<script src="{{{ asset('assets/admin/js/bootstrap.min.js') }}}"></script>
	<!-- page scripts -->
	<script src="{{{ asset('assets/admin/js/jquery-ui.1.11.2.min.js') }}}"></script>
	<script src="{{{ asset('js/plugins/jquery-printme.js') }}}"></script>
	<script>
	$(function(){
		$('.btn-print-now').click(function(){
			console.log('vai imprimir');
			$('.container').printMe({
				'path' : "{{{ asset('assets/admin/css/bootstrap.min.css')}}}",
				'title': 'Ordem de Serviço'
			});
		});
	});
	</script>
	
	@yield('scripts')
</body>
</html>