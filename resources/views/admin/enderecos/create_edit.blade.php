{{-- @extends('admin.layouts.modal2') --}}
@section('content-header')
<h1>
    Endereço
    <small>{{ isset($endereco) ? "Editar" : "Cadastro" }}</small>
</h1>

<!-- You can dynamically generate breadcrumbs here -->
<ol class="breadcrumb">
    <li><a href="{{ url('admin/ordemservico') }}"><i class="fa fa-dashboard"></i> Endereço</a></li>
    <li class="active">{{ isset($endereco) ? "Editar" : "Cadastro" }}</li>
</ol>
@endsection
@section('content')


		<div class="box box-success">
	        <div class="box-header with-border">
	          <h3 class="box-title">Cadastro de Rota</h3>
	        </div><!-- /.box-header -->
	        <!-- form start -->
	        @if (isset($endereco))
			{!! Form::model($endereco, array('url' => url('admin/endereco') . '/' . $endereco->id, 'method' => 'put', 'class' => 'bf', 'files'=> true)) !!}
			@else
            <form action="{{{ url('admin/endereco') }}}" method="post" class="bf">
			@endif
            <input type="hidden" name="contrato_id" value="{{ Input::get('idContrato')}}" />
            <input type="hidden" name="cliente_id" value="{{ Input::get('idCliente')}}" />
			<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
			
	   

       <div class="box-body"><!-- início dos campos -->
		
		<div class="row">
			<div class="col-md-12">
                <h3>Endereço</h3>
            </div>
            <div class="col-md-4">
                <div class="form-group  {{ $errors->has('cidade') ? 'has-error' : '' }}">
                    {!! Form::label('cidade', 'Cidade', array('class' => 'control-label')) !!}
                   
                    {!! Form::text('cidade', null, array('id'=>'cidade','autocomplete' => 'off','class' => 'form-control')) !!}
                    <input type="hidden" id="cidade_id" name="cidade_id" value="@if(isset($cliente)){{$cliente->cidade_id}}@endif">
                    <span class="help-block">{{ $errors->first('cidade', ':message') }}</span>
                
                </div>
            </div>
    
            <div class="col-md-6">
                <div class="form-group  {{ $errors->has('endereco') ? 'has-error' : '' }}">
                    {!! Form::label('endereco', 'Endereço', array('class' => 'control-label')) !!}
                    {!! Form::text('endereco', null, array('class' => 'form-control')) !!}
                    <span class="help-block">{{ $errors->first('endereco', ':message') }}</span>
              
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group  {{ $errors->has('numero') ? 'has-error' : '' }}">
                    {!! Form::label('numero', 'Número', array('class' => 'control-label')) !!}
                    {!! Form::text('numero', null, array('class' => 'form-control')) !!}
                    <span class="help-block">{{ $errors->first('numero', ':message') }}</span>
              
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <div class="form-group  {{ $errors->has('cep') ? 'has-error' : '' }}">
                    {!! Form::label('cep','CEP', array('class' => 'control-label')) !!}
                    {!! Form::text('cep', null, array('class' => 'form-control input-sm')) !!}
                    <span class="help-block">{{ $errors->first('cep', ':message') }}</span>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group  {{ $errors->has('bairro') ? 'has-error' : '' }}">
                    {!! Form::label('bairro', 'Bairro', array('class' => 'control-label')) !!}
                    {!! Form::text('bairro', null, array('class' => 'form-control')) !!}
                    <span class="help-block">{{ $errors->first('bairro', ':message') }}</span>
                </div>
            </div>
            
            <div class="col-md-6">
                <div class="form-group   {{ $errors->has('complemento') ? 'has-error' : '' }}">
                    {!! Form::label('complemento','Complemento', array('class' => 'control-label')) !!}
                    {!! Form::text('complemento', null, array('class' => 'form-control')) !!}
                    <span class="help-block">{{ $errors->first('complemento', ':message') }}</span>
                </div>
            </div>
			
		</div>

		<!-- fim dos campos -->
      </div><!-- /.box-body -->
      <div class="box-footer">
      	
		

        <button type="submit" class="btn btn-success btn-sm pull-right">
			<span class="glyphicon glyphicon-ok-circle"></span> 
			    @if(isset($endereco))
			        Editar
			    @else
			        Salvar
			    @endif
		</button>
      </div><!-- /.box-footer -->
    </form>
	</div><!-- /.box -->

@stop

@section('scripts')
<script>

	$(function(){
       

	});
</script>
@endsection
