{{-- @extends('adminlte') --}}
@section('content-header')
<h1>
    Usuário
    <small>{{ isset($user) ? "Editar" : "Cadastro" }}</small>
</h1>

<!-- You can dynamically generate breadcrumbs here -->
<ol class="breadcrumb">
    <li><a href="{{ URL::to('admin/users') }}"><i class="fa fa-dashboard"></i> Usuário</a></li>
    <li class="active">{{ isset($user) ? "Editar" : "Cadastro" }}</li>
</ol>
@endsection
@section('content')


		<div class="box box-info">
	        <div class="box-header with-border">
	          <h3 class="box-title">Cadastro de Usuário</h3>
	        </div><!-- /.box-header -->
	        <!-- form start -->
	        <!-- <form class="form-horizontal" method="post" enctype="multipart/form-data"
	        	action="@if (isset($user)){{ URL::to('admin/user/' . $user->id . '/edit') }}@endif"
	        	autocomplete="off"> -->
	       @if (isset($user))
		{!! Form::model($user, array('url' => URL::to('admin/user/' . $user->id . '/edit'), 'method' => 'post', 'class' => 'form-horizontal bf', 'files'=> true)) !!}
		@else
		{!! Form::open(array('url' => URL::to('admin/user'), 'method' => 'post', 'class' => 'form-horizontal bf', 'files'=> true)) !!}
		@endif

			
	          <div class="box-body">	
	          <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />

	       			<input type="hidden" name="tipo_usuario" valu="1">

				
				  <div class="form-group {{{ $errors->has('image') ? 'error' : '' }}}">
					  <label for="name" class="col-sm-2 control-label">Foto</label>
					  <div class="col-sm-10">
						  {{{ $errors->first('image', "<label class=\"control-label\" for=\"inputError\"\> :message </label>") }}}
						  <input name="image"
								 type="file" class="uploader" id="image" value="Upload" />
					  </div>
				  </div>

	            <div class="form-group  {{ $errors->has('name') ? 'has-error' : '' }}">
                 {!! Form::label('name','Nome', array('class' => 'col-sm-2 control-label')) !!}
                  <div class="col-sm-10">
                    {!! Form::text('name', null, array('class' => 'form-control')) !!}
                     <span class="help-block">{{ $errors->first('name', ':message') }}</span>
                  </div>
                </div>


	            <div class="form-group  {{ $errors->has('username') ? 'has-error' : '' }}">
                 {!! Form::label('username','Nickname', array('class' => 'col-sm-2 control-label')) !!}
                  <div class="col-sm-10">
                    {!! Form::text('username', null, array('class' => 'form-control')) !!}
                     <span class="help-block">{{ $errors->first('username', ':message') }}</span>
                  </div>
                </div>

                <div class="form-group  {{ $errors->has('email') ? 'has-error' : '' }}">
                 {!! Form::label('email','Email', array('class' => 'col-sm-2 control-label')) !!}
                  <div class="col-sm-10">
                    {!! Form::text('email', null, array('class' => 'form-control')) !!}
                     <span class="help-block">{{ $errors->first('email', ':message') }}</span>
                  </div>
                </div>

                 <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
	              <label for="password" class="col-sm-2 control-label">Senha</label>
	              <div class="col-sm-10">
	                <input type="password" name="password" class="form-control" id="password" placeholder="Senha">
	                {!! $errors->first('password', '<label class="control-label"
                                                            for="password">:message</label>') !!}
	              </div>
	            </div>
	            <div class="form-group {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
	              <label for="password_confirmation" class="col-sm-2 control-label">Confimar Senha</label>
	              <div class="col-sm-10">
	                <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" placeholder="Senha" value="">
	                {!! $errors->first('password_confirmation', '<label class="control-label"
                                                            for="password_confirmation">:message</label>') !!}
	              </div>
	            </div>
				
				

				{{--Cadastra usuário já ativo no sistema--}}
				<input type="hidden" name="confirmed" value="1">

	          </div><!-- /.box-body -->
	          <div class="box-footer">
	          	<a href="{{ URL::to('/admin/users') }}" class="btn btn-sm btn-danger close_popup">
					<span class="glyphicon glyphicon-ban-circle"></span> 
					Cancelar
				</a>
				<button type="reset" class="btn btn-sm btn-default">
					<span class="glyphicon glyphicon-remove-circle"></span> 
					Limpar
				</button>

	            <button type="submit" class="btn btn-success pull-right">
					<span class="glyphicon glyphicon-ok-circle"></span> 
					    @if	(isset($user))
					        Alterar
					    @else
					        Salvar
					    @endif
				</button>
	          </div><!-- /.box-footer -->
	        </form>
	      </div><!-- /.box -->

@stop
