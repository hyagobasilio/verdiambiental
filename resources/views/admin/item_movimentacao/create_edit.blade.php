{{-- @extends('admin.layouts.modal2') --}}

@section('content')


		<div class="box box-success">
	        <div class="box-header with-border">
	          <h3 class="box-title">Cadastro de item</h3>
	        </div><!-- /.box-header -->
	        <!-- form start -->
	    
	      	@if (isset($item ))
			{!! Form::model($item , array('url' => url('admin/itemmovimentacao/' . $item ->id . '/edit'), 'method' => 'put', 'class' => 'form-horizontal bf', 'files'=> true)) !!}
			@else
			{!! Form::open(array('url' => url('admin/itemmovimentacao'), 'method' => 'post', 'class' => 'form-horizontal bf', 'files'=> true)) !!}
			@endif
			

			
	          <div class="box-body">


              	<div class="col-md-9">
                	<div class="form-group  {{ $errors->has('titulo') ? 'has-error' : '' }}">
	                 	{!! Form::label('titulo','Titulo', array('class' => ' control-label')) !!}
	                    {!! Form::text('titulo', null, array('class' => 'form-control input-sm')) !!}
	                    <span class="help-block">{{ $errors->first('titulo', ':message') }}</span>
                  	</div>
                </div>

                <!-- Categoria -->
                <div class="col-md-3">
                    <div class="form-group {{ $errors->has('categoria_id') ? 'has-error' : '' }}">
                        {!! Form::label('categoria_id','Categoria', array('class' => 'control-label')) !!}
                        {!! Form::select('categoria_id', $categorias, null, ['class' => 'form-control input-sm']) !!}
                        <span class="help-block">{{ $errors->first('categoria_id', ':message') }}</span>
                    </div>
                </div>

	          	</div><!-- /.box-body -->
	          	<div class="box-footer">
	          		<a href="{{ url('/admin/itemmovimentacao') }}" class="btn btn-sm btn-danger close_popup">
						<span class="glyphicon glyphicon-ban-circle"></span> 
					Cancelar
					</a>
					<button type="reset" class="btn btn-sm btn-default">
						<span class="glyphicon glyphicon-remove-circle"></span> 
						Limpar
					</button>

		            <button type="submit" class="btn btn-success pull-right">
						<span class="glyphicon glyphicon-ok-circle"></span> 
						    @if	(isset($item))
						        Alterar
						    @else
						        Salvar
						    @endif
					</button>
	          </div><!-- /.box-footer -->
	        </form>
	      </div><!-- /.box -->

@stop
