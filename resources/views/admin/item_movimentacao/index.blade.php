@extends('adminlte')
@section('content-header')
<h1>
    Item movimentação
    <small>visualização</small>
</h1>

<!-- You can dynamically generate breadcrumbs here -->
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Item movimentação</a></li>
    <li class="active">Visualização</li>
</ol>
@endsection
{{-- Content --}}
@section('content')
    
<br>
<div class="row">
    <div class="col-md-12">
        
        <div class="box box-primary">
            <div class="box-header ui-sortable-handle" style="cursor: move;">
                <!-- <i class="ion ion-clipboard"></i> -->

                <h3 class="box-title">Item movimentação</h3>
                <div class="pull-right">
                    <a href="{{{ url('admin/itemmovimentacao/create') }}}"
                        class="btn btn-sm  btn-success iframe"><span
                            class="glyphicon glyphicon-plus-sign"></span> Novo</a>
                </div>

                

                
            </div>
            <!-- /.box-header -->
            <div class="box-body">

                <table id="table">
                    <thead>
                        <tr>
                            <th>Cód.</th>
                            <th>Titulo</th>
                            <th>Categoria</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix no-border">
               
                
            </div>
          </div>
    </div>
</div>
@endsection


