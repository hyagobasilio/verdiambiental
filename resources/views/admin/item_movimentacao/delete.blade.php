@extends('admin.layouts.modal2') @section('content')
{!! Form::model($item, array('url' => url('admin/itemmovimentacao') . '/' . $item->id, 'method' => 'delete', 'class' => 'bf', 'files'=> true)) !!}


	<input type="hidden" name="_token" value="{{{ csrf_token() }}}" /> <input
		type="hidden" name="id" value="{{ $item->id }}" />
	<div class="form-group">
		<div class="controls">
			<p>Deseja realmente exluir esta Categoria?</p>
			<element class="btn btn-warning btn-sm close_popup">
			<span class="glyphicon glyphicon-ban-circle"></span>Cancelar</element>
			<button type="submit" class="btn btn-sm btn-danger">
				<span class="glyphicon glyphicon-trash"></span> Excluir
			</button>
		</div>
	</div>
</form>
@stop
