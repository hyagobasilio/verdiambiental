{{-- @extends('adminlte') --}}

@section('head')
<style>
    .lista-rotas {
        border: 1px solid #eee;
        padding: 0;
    }
    .lista-rotas > li {
        padding: 3px 10px;
        list-style: none;
    }
</style>
@endsection
@section('content-header')
<h1>
   .
</h1>

<!-- You can dynamically generate breadcrumbs here -->
<ol class="breadcrumb">
    <li><a href="{{ URL::to('admin/ocorrencia') }}"><i class="fa fa-dashboard"></i> Ordem de serviço</a></li>
    <li class="active">{{ isset($ocorrencia) ? "Editar" : "Cadastro" }}</li>
</ol>

@endsection
@section('content')



<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Cadastro de Ordem de Serviço</h3>
    </div><!-- /.box-header -->
    <!-- form start -->
    @if (isset($ocorrencia))
	{!! Form::model($ocorrencia, array('url' => URL::to('admin/ocorrencia/' . $ocorrencia->id . '/edit'), 'method' => 'put', 'class' => 'bf', 'files'=> true)) !!}
	@else
	{!! Form::open(array('url' => URL::to('admin/ocorrencia'), 'method' => 'post', 'class' => ' bf', 'files'=> true)) !!}
	@endif

		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
        <input type="hidden" name="itens_rota" id="itens_rota" value="{{ isset($ocorrencia) ? $ocorrencia->rotas : ''}}{{ old('itens_rota') }}">
        <div class="box-body">
				       

            <div class="row">

                <div class="col-md-2">
                    <?php $opcoes = ['entulho' => 'Entulho','lixo' => 'Lixo']; ?>
                    <div class="form-group {{ $errors->has('servico') ? 'has-error' : '' }}">
                        {!! Form::label('servico','Serviço', array('class' => 'control-label')) !!}
                        {!! Form::select('servico', $opcoes, null, ['class' => 'form-control input-sm']) !!}
                        <span class="help-block">{{ $errors->first('servico', ':message') }}</span>          
                    </div>
                </div>

                <!-- Entulho -->
                <div class="entulho">
                    
                    <!-- Tipo -->
                    <div class="col-md-2">
                        <?php $opcoes = ['' => 'Selecione', 'remocao' => 'Remoção', 'retirada' => 'Retirada', 'entrega' => 'Entrega']; ?>
                        <div class="form-group {{ $errors->has('tipo') ? 'has-error' : '' }}">
                            {!! Form::label('tipo','Tipo', array('class' => 'control-label')) !!}
                            {!! Form::select('tipo', $opcoes, null, ['class' => 'form-control input-sm']) !!}
                            <span class="help-block">{{ $errors->first('tipo', ':message') }}</span>          
                        </div>
                    </div>
                          
                    <!-- Número container -->
                    <div class="col-sm-2">
                        <?php $numero_container = isset($ocorrencia->container->numero) ? $ocorrencia->container->numero : null ; ?>
                        <div class="form-group {{ $errors->has('container') ? 'has-error' : '' }}">
                        {!! Form::label('container','Nº do Container', array('class' => 'control-label')) !!}
                            {!! Form::text('container', $numero_container, array('class' => 'form-control input-sm', 'autocomplete' => 'off')) !!}
                            <input type="hidden" name="container_id" id="container_id" value="@if(isset($ocorrencia->container_id)){{$ocorrencia->container_id}}@endif">
                            <span class="help-block">{{ $errors->first('container', ':message') }}</span>
                        </div>
                    </div>

                    <!-- Prazo -->
                    <div class="col-sm-2">
                        <div class="form-group {{ $errors->has('prazo') ? 'has-error' : '' }}">
                            {!! Form::label('prazo','Prazo dias', array('class' => 'control-label')) !!}
                            {!! Form::number('prazo', null, array('class' => 'form-control input-sm')) !!}
                            <span class="help-block">{{ $errors->first('prazo', ':message') }}</span>
                        </div>
                    </div>
                </div>

                
            </div>
            <div class="row">

                <!-- cliente -->
                <div class="col-sm-3">
                    <div class=" form-group {{ $errors->has('cliente') ? 'has-error' : '' }}">
                        {!! Form::label('cliente','Cliente', array('class' => 'control-label')) !!}
                        <span class="input-group">
                            {!! Form::text('cliente-os', null, array('class' => 'form-control input-sm', 'autocomplete' => 'off', 'id' => 'cliente-os')) !!}
                            <input type="hidden" name="cliente_id" id="cliente_id" value="@if(isset($ocorrencia)) {{ $ocorrencia->cliente_id }} @endif">
                            <span class="input-group-btn input-group-sm">  
                                <a href="{{url('/admin/cliente/create')}}" class=" input-sm iframe btn btn-success btn-flat">
                                <i class="glyphicon glyphicon-plus-sign"></i></a>
                            </span>
                            
                            <span class="help-block">{{ $errors->first('cliente', ':message') }}</span>
                        </span>
                    </div>
                </div>
                <!-- Motorista -->
                <div class="col-sm-3">
                    <div class="form-group {{ $errors->has('id_motorista') ? 'has-error' : '' }}">
                        {!! Form::label('id_motorista','Motorista', array('class' => 'control-label')) !!}
                        {!! Form::select('id_motorista', $motoristas, null, ['class' => 'form-control input-sm']) !!}
                        <span class="help-block">{{ $errors->first('id_motorista', ':message') }}</span>
                    </div>
                </div>

                <!-- Data Abertura -->
                <div class="col-sm-2">
                    <div class="form-group {{ $errors->has('data') ? 'has-error' : '' }}">
                        {!! Form::label('data','Data Abertura', array('class' => 'control-label')) !!}
                        {!! Form::text('data', null, array('class' => 'form-control input-sm')) !!}
                        <span class="help-block">{{ $errors->first('data', ':message') }}</span>
                    </div>
                </div>

                <!-- Valor Estimado -->
                <div class="col-sm-2">
                    <div class="form-group {{ $errors->has('valor_estimado') ? 'has-error' : '' }}">
                        {!! Form::label('valor_estimado','Valor Estimado', array('class' => 'control-label')) !!}
                        {!! Form::text('valor_estimado', null, array('class' => 'valor form-control input-sm')) !!}
                        <span class="help-block">{{ $errors->first('valor_estimado', ':message') }}</span>
                    </div>
                </div>
        		<!-- Valor final -->
                @if(isset($ocorrencia))
                    <div class="col-sm-2">
                        <div class="form-group  {{ $errors->has('valor_final') ? 'has-error' : '' }}">
                            {!! Form::label('valor_final','Valor Final', array('class' => 'control-label')) !!}
                            {!! Form::text('valor_final', null, array('class' => 'valor form-control input-sm')) !!}
                            <span class="help-block">{{ $errors->first('valor_final', ':message') }}</span>
                        </div>
                    </div>
                @endif
            </div>
            <div class="row">
                <div class="col-sm-2">
                    <div class="form-group {{ $errors->has('cidade') ? 'has-error' : '' }}">
                        {!! Form::label('cidade','Cidade', array('class' => 'control-label')) !!}
                        {!! Form::text('cidade', null, array('class' => 'form-control input-sm', 'autocomplete' => 'off')) !!}
                        <input type="hidden" name="cidade_id" id="cidade_id" value="@if(isset($ocorrencia)) {{ $ocorrencia->cidade_id }} @endif">
                        <span class="help-block">{{ $errors->first('cidade', ':message') }}</span>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group  {{ $errors->has('endereco') ? 'has-error' : '' }}">
                        {!! Form::label('endereco','Endereço', array('class' => 'control-label')) !!}
                        {!! Form::text('endereco', null, array('class' => 'form-control input-sm')) !!}
                        <span class="help-block">{{ $errors->first('endereco', ':message') }}</span>
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="form-group  {{ $errors->has('bairro') ? 'has-error' : '' }}">
                        {!! Form::label('bairro','Bairro', array('class' => 'control-label')) !!}
                        {!! Form::text('bairro', null, array('class' => 'form-control input-sm')) !!}
                        <span class="help-block">{{ $errors->first('bairro', ':message') }}</span>
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="form-group  {{ $errors->has('cep') ? 'has-error' : '' }}">
                        {!! Form::label('cep','CEP', array('class' => 'control-label')) !!}
                        {!! Form::text('cep', null, array('class' => 'form-control input-sm')) !!}
                        <span class="help-block">{{ $errors->first('cep', ':message') }}</span>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group  {{ $errors->has('observacao') ? 'has-error' : '' }}">
                        {!! Form::label('observacao','Observação', array('class' => 'control-label')) !!}
                        {!! Form::text('observacao', null, array('class' => 'form-control input-sm')) !!}
                        <span class="help-block">{{ $errors->first('observacao', ':message') }}</span>
                    </div>
                </div>

                <!-- Multiple Radios (inline) -->
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="" for="radios">Status da OS</label>
                        <br>              
                        <label class="radio-inline" for="radios-0">
                          <input type="radio" name="status" id="radios-0" value="0" @if(!isset($ocorrencia) || (isset($ocorrencia) && $ocorrencia->status == 0))checked="checked"@endif>
                          Aberto
                        </label> 
                        <label class="radio-inline" for="radios-1">
                          <input type="radio" name="status" id="radios-1" value="1" @if( isset($ocorrencia) && $ocorrencia->status == 1) checked="checked"@endif >
                          Concluída
                        </label>
                            
                      
                    </div>
                </div>
                </div>
                <div class="row">
                <!-- Numero Cupom -->
                <div class="col-md-2">
                    <div class="form-group  {{ $errors->has('numero_cupom') ? 'has-error' : '' }}">
                        {!! Form::label('numero_cupom', 'Número cupom', array('class' => 'control-label')) !!}
                        {!! Form::number('numero_cupom', null, array('class' => ' form-control')) !!}
                        <span class="help-block">{{ $errors->first('numero_cupom', ':message') }}</span>
                    </div>
                </div>

                <!-- Manifesto -->
                <div class="col-md-2">
                    <div class="form-group  {{ $errors->has('manifesto') ? 'has-error' : '' }}">
                        {!! Form::label('manifesto', 'Manifesto', array('class' => 'control-label')) !!}
                        {!! Form::text('manifesto', null, array('class' => 'form-control')) !!}
                        <span class="help-block">{{ $errors->first('manifesto', ':message') }}</span>
                    </div>
                </div>
                <!-- Número Ticket -->
                <div class="col-md-3">
                    <div class="form-group  {{ $errors->has('numero_ticket') ? 'has-error' : '' }}">
                        {!! Form::label('numero_ticket', 'Número ticket', array('class' => 'control-label')) !!}
                        {!! Form::text('numero_ticket', null, array('class' => 'form-control')) !!}
                        <span class="help-block">{{ $errors->first('numero_ticket', ':message') }}</span>
                    </div>
                </div>

                <!-- Peso -->
                <div class="col-md-3">
                    <div class="form-group  {{ $errors->has('peso') ? 'has-error' : '' }}">
                        {!! Form::label('peso', 'Peso em kg', array('class' => 'control-label')) !!}
                        {!! Form::text('peso', null, array('class' => 'valor form-control')) !!}
                        <span class="help-block">{{ $errors->first('peso', ':message') }}</span>
                    </div>
                </div>

                <!-- Lixo -->
                <div class="lixo col-sm-12">
                    
                    <!-- Rotas -->
                    <div class="col-sm-6">
                        <div class="form-group @if(!isset($ocorrencia)) has-warning @endif">
                            <?php 
                            /*
                            * Apenas permite salvar Rotas se a OS já estiver salva
                            */
                            ?>
                            @if(!isset($ocorrencia))
                            <label>Para Inserir Rotas é necessario salvar esta OS primeiro!</label>
                            @else
                            <div class="input-group">
                                <a href="{{ url('admin/itemrota/'.$ocorrencia->id.'/create'.'?servico='.$ocorrencia->servico.'&tipo='.$ocorrencia->tipos)   }}" class="btn btn-success btn-sm iframe">Adicionar Rota</a>
                            </div>
                            @endif
                            
                        </div>
                    </div>

                    <div class="col-md-12">


                    <!-- .Box -->  
                    <div class="box box-success">
                        <div class="box-header">
                          <h3 class="box-title">Rotas</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body no-padding">
                            <table id="tabela-rotas" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Data</th>
                                        <th>Tipo</th>
                                        <th>Estado</th>
                                        <th>Nº do cupom</th>
                                        <th>Manifesto</th>
                                        <th>Descarte</th>
                                        <th>Peso</th>
                                        <th>Preço</th>
                                        <th>Status</th>
                                        <th>Ações</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                      </div>
                        
                    </div>


                    <!-- Rotas
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="alunos_falta">Dias Rotas</label>
                            <div class="input-group">
                                <input type="text" id="data-rota" class="form-control input-sm data">
                                <span class="input-group-btn">  
                                    <a id="adicionar-rota" onclick="javascript:adicionarDataRota()" class=" input-sm btn btn-success btn-flat">
                                    <i class="glyphicon glyphicon-plus-sign"></i></a>
                                
                                    <a id="remover-rotas" onclick="javascript:removerItensRota()" class=" input-sm btn btn-danger btn-flat">
                                    <i class="glyphicon glyphicon-remove"></i></a>
                                </span>
                            </div>
                            <select size="3" id="lista-rotas" multiple="" class="height form-control">
                            </select>
                        </div>
                    </div>
                </div> -->
                <!-- ./lixo -->
            </div>
			<!-- fim dos campos -->
        </div><!-- /.box-body -->
        <div class="box-footer">
			

            <button type="submit" class="btn btn-success btn-sm pull-right">
				<span class="glyphicon glyphicon-ok-circle"></span> 
				    @if	(isset($ocorrencia))
				        Editar
				    @else
				        Salvar
				    @endif
			</button>
        </div><!-- /.box-footer -->
    </form>
</div><!-- /.box -->

@section('scripts')
<script>
function habilitaResiduoOrLixo(){
    $('.lixo').show();
    $('.entulho').show();
    if($('#servico').val() == 'entulho') {
        $('.lixo').hide();
    }else {
        $('.entulho').hide();
    }
}

function dataToView(data) {
    d = data.split('-');
    return d[0]+'/'+d[1]+'/'+d[2];

}
function montaItensRota() {
    rotas = $('#itens_rota').val();
    if (rotas != '' ) {
        dados = JSON.parse(rotas);
        $.each(dados, function(i, iten) {

            $('<option>', {
                value: iten,
                html: dataToView(iten)
            }).appendTo('#lista-rotas');

        });
    }
}


function removerItensRota()
{
    var selecionados = $("#lista-rotas option:selected");
    for(i = 0; i < selecionados.length; i++) {
        selecionados[i].remove();
    }
}
function adicionarDataRota() {
    el = $('#data-rota')
    data = el.val();
    el.val('');
    dataFormatoBanco = dataToBd(data);
    if(data != undefined && data != '') {

       
        if(!contains(dataFormatoBanco, "#lista-rotas option")) {
            
            $('<option>', {
                value: dataFormatoBanco,
                html: data
            }).appendTo('#lista-rotas');
        }
        
    }
}
function contains(valor, selector) {
    lista = $(selector);
    for(i = 0; i < lista.length; i++) {
        if(lista[i].value == valor) {
            return true;
        }
    }
    return false;
}
function dataToBd(data) {
    //console.log(data);
    d = data.split('/');
    return d[2]+'-'+d[1]+'-'+d[0];
}
$(function() {


    $('#cliente-os').typeahead({
         source: function(q, process) {
            objects = [];
            map = {};
            // requisição ajax
            

            return $.get("{{ URL::to('admin/cliente/dados/os') }}", 
                { 
                    query: q 
                }, function (data) {
                    objects = [];
                    $.each(data, function(i, object) {
                        var chave = object.nome + " " + object.nome_fantasia;
                        map[chave] = object;
                        objects.push(chave);
                    });
                    return process(objects);
                });
            // fim ajax
            
           
            process(objects);
        },
        updater: function(item) {
            
            $("#cliente_id").val(map[item].id);
            $("#cidade_id").val(map[item].cidade_id);
            $("#cidade").val(map[item].cidade + ' - ' + map[item].uf);
            $("#bairro").val(map[item].bairro);
            $("#cep").val(map[item].cep);
            $("#endereco").val(map[item].endereco);
            return item;
        }
    });

    $(".iframe").colorbox({
        iframe: true,
        width: "80%",
        height: "80%"
    });
    
    //habilitaResiduoOrLixo();
    //montaItensRota();

    /*$('#servico').on('change', function(){
        habilitaResiduoOrLixo();
    })*/
    //submete o formulario
    /*$('form').submit(function(){
        //pega os itens
        lista = $('#lista-rotas option');
        dados =[];
        for(i = 0; i < lista.length; i++) {
            dados.push(lista[i].value);
        }
        $('#itens_rota').val(JSON.stringify(dados))
       // return false;
    })*/
    var x =$('input#prazo');
    if (x.val() == "") { x.val(7)}
    
    // adicionar Data Rota
    /*$("#adicionar-rota").on('click',function(){
        adicionarDataRota();*/
            @if(isset($ocorrencia))
        var oTable;
        oTable = $('#tabela-rotas').DataTable({
            "sDom": "<'row'<'col-md-6'l><'col-md-6'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
            "sPaginationType": "full_numbers",
            "pageLength": 5,
            "oLanguage": {
                "sProcessing": "Processando",
                "sLengthMenu": "",
                "sZeroRecords": "Nenhum resultado",
                "sInfo": "",
                "sEmptyTable": "Tabela vazia",
                "sInfoEmpty": "Vazio",
                "sInfoFiltered": "Filtro",
                "sInfoPostFix": "",
                "sSearch": "Pesquisar:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Início",
                    "sPrevious": "Anterior",
                    "sNext": "Próximo",
                    "sLast": "Último"
                }
            },
            "processing": true,
            "serverSide": true,
            "ajax": "<?php echo url("admin/itemrota/data/$ocorrencia->id"); ?>",
            "fnDrawCallback": function (oSettings) {
                $('.paginate_button').addClass('btn-sm');
            
                $(".iframe").colorbox({
                    iframe: true,
                    width: "80%",
                    height: "80%",
                    onClosed: function () {
                        oTable.ajax.reload();
                    }
                });
                
            }
        });
    @endif



});
</script>
@endsection


@stop
