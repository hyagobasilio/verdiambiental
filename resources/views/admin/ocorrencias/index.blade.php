@extends('adminlte')
@section('content-header')


<!-- You can dynamically generate breadcrumbs here -->
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Ordem de Serviço</a></li>
    <li class="active">Visualização</li>
</ol>
@endsection
{{-- Content --}}
@section('content')
    <div class="page-header">
        <h3>
            Ordem de Serviço
            <div class="pull-right">
                <div class="pull-right">
                    <a href="{!! URL::to('admin/ocorrencia/create') !!}"
                       class="btn btn-sm  btn-success"><span
                                class="glyphicon glyphicon-plus-sign"></span>
                    Novo</a>
                </div>
            </div>
        </h3>
    </div>

    <table id="table" class="table table-striped table-hover">
        <thead>
            <tr>
                <th>Nº</th>
                <th>Serviço</th>
                <th>Tipo</th>
                <th>Data</th>
                <th>Cliente</th>
                <th>Valor. Estimado</th>
                <th>Valor Final</th>
                <th>Nº do Container</th>
                <th>Status</th>
                <th>Ações</th>
            </tr>
        </thead>
        <tbody></tbody>
    </table>
@endsection

{{-- Scripts --}}
@section('scripts')

<script>
    
    $(document).ready(function(){
    /*    var oTable;
        oTable = $('#table').DataTable({
            "sDom": "<'row'<'col-md-6'l><'col-md-6'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
            "sPaginationType": "full_numbers",
            columns : [
                {data: 'id', name: 'ocorrencias.id'},
                {data: 'servico', name: 'ocorrencias.servico'},
                {data: 'tipo', name: 'ocorrencias.tipo'},
                {data: 'data', name: 'ocorrencias.data'},
                {data: 'cliente', name: 'ocorrencias.cliente'},
                {data: 'valor_estimado', name: 'ocorrencias.valor_estimado'},
                {data: 'valor_estimado', name: 'ocorrencias.valor_estimado'},
                {data: 'valor_final', name: 'ocorrencias.valor_final'},
                {data: 'valor_final', name: 'ocorrencias.valor_final'},
                {data: 'numero_container', name: 'ocorrencias.numero_container'},
                {data: 'numero_container', name: 'ocorrencias.numero_container'},
                {data: 'cidade', name: 'ocorrencias.cidade'}
            ],
            "oLanguage": {
                "sProcessing": "Processando",
                "sLengthMenu": "",
                "sZeroRecords": "Nenhum resultado",
                "sInfo": "{{ trans('table.show') }}",
                "sEmptyTable": "Tabela vazia",
                "sInfoEmpty": "Vazio",
                "sInfoFiltered": "Filtro",
                "sInfoPostFix": "",
                "sSearch": "Pesquisar:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Inicio",
                    "sPrevious": "Anterior",
                    "sNext": "Próximo",
                    "sLast": "Último"
                }
            },
            "processing": true,
            "serverSide": true,
            "ajax": "{!! url('admin/ocorrencia/data') !!}",
            "fnDrawCallback": function (oSettings) {
            
                

                $(".iframe").colorbox({
                    iframe: true,
                    width: "80%",
                    height: "80%",
                    onClosed: function () {
                        oTable.ajax.reload();
                    }
                });
                
            }
        });*/

        /*fim datatable*/
        $('#cidade').typeahead({
                 source: function(q, process) {
                    objects = [];
                    map = {};
                    // requisição ajax
                    

                    return $.get("{{ URL::to('admin/cidadeestado') }}", 
                        { 
                            query: q 
                        }, function (data) {
                            objects = [];
                            $.each(data, function(i, object) {
                                var chave = object.nome + " - " + object.uf;
                                map[chave] = object;
                                objects.push(chave);
                            });
                            return process(objects);
                        });
                    // fim ajax
                    
                   
                    process(objects);
                },
                updater: function(item) {
                    
                    $("#cidade_id").val(map[item].id);
                    return item;
                }
            });
    });
</script>
@parent

@endsection
