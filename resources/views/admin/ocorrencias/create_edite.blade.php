{{-- @extends('adminlte') --}}
@section('content-header')
<h1>
   .
</h1>

<!-- You can dynamically generate breadcrumbs here -->
<ol class="breadcrumb">
    <li><a href="{{ URL::to('admin/materias') }}"><i class="fa fa-dashboard"></i> Ordem de serviço</a></li>
    <li class="active">{{ isset($ocorrencia) ? "Editar" : "Cadastro" }}</li>
</ol>
@endsection
@section('content')



<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Cadastro de Ordem de Serviço</h3>
    </div><!-- /.box-header -->
    <!-- form start -->
    @if (isset($ocorrencia))
  {!! Form::model($ocorrencia, array('url' => URL::to('admin/ocorrencia/' . $ocorrencia->id . '/edit'), 'method' => 'put', 'class' => 'bf', 'files'=> true)) !!}
  @else
  {!! Form::open(array('url' => URL::to('admin/ocorrencia'), 'method' => 'post', 'class' => ' bf', 'files'=> true)) !!}
  @endif

    <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
        <div class="box-body">
               

            <div class="row">

                <div class="col-md-2">
                    <?php $opcoes = ['remocao' => 'Remoção', 'retirada' => 'Retirada', 'entrega' => 'Entrega']; ?>
                    <div class="form-group {{ $errors->has('tipo') ? 'has-error' : '' }}">
                        {!! Form::label('tipo','Tipo', array('class' => 'control-label')) !!}
                        {!! Form::select('tipo', $opcoes, null, ['class' => 'form-control']) !!}
                        <span class="help-block">{{ $errors->first('tipo', ':message') }}</span>          
                    </div>
                </div>
                      
                <div class="col-sm-5">
                    <div class=" form-group {{ $errors->has('cliente') ? 'has-error' : '' }}">
                        {!! Form::label('cliente','Cliente', array('class' => 'control-label')) !!}
                        <span class="input-group">
                            {!! Form::text('cliente', null, array('class' => 'form-control', 'autocomplete' => 'off')) !!}
                            <input type="hidden" name="cliente_id" id="cliente_id" value="@if(isset($ocorrencia)) {{ $ocorrencia->cliente_id }} @endif">
                            <span class="input-group-btn">  
                                <a href="{{url('/admin/cliente/create')}}" class="iframe btn btn-success btn-flat">+</a>
                            </span>
                            
                            <span class="help-block">{{ $errors->first('cliente', ':message') }}</span>
                        </span>
                    </div>
                </div>

                <div class="col-sm-5">
                    <div class="form-group {{ $errors->has('id_motorista') ? 'has-error' : '' }}">
                        {!! Form::label('id_motorista','Motorista', array('class' => 'control-label')) !!}
                        {!! Form::select('id_motorista', $motoristas, null, ['class' => 'form-control']) !!}
                        <span class="help-block">{{ $errors->first('id_motorista', ':message') }}</span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-2">
                    <div class="form-group {{ $errors->has('numero_container') ? 'has-error' : '' }}">
                    {!! Form::label('numero_container','Nº do Container', array('class' => 'control-label')) !!}
                        {!! Form::number('numero_container', null, array('class' => 'form-control')) !!}
                        <span class="help-block">{{ $errors->first('numero_container', ':message') }}</span>
                    </div>
                </div>

                <div class="col-sm-2">
                    <div class="form-group {{ $errors->has('data') ? 'has-error' : '' }}">
                        {!! Form::label('data','Data', array('class' => 'control-label')) !!}
                        {!! Form::text('data', null, array('class' => 'form-control')) !!}
                        <span class="help-block">{{ $errors->first('data', ':message') }}</span>
                    </div>
                </div>

                <div class="col-sm-2">
                    <div class="form-group {{ $errors->has('prazo') ? 'has-error' : '' }}">
                        {!! Form::label('prazo','Prazo dias', array('class' => 'control-label')) !!}
                        {!! Form::number('prazo', null, array('class' => 'form-control')) !!}
                        <span class="help-block">{{ $errors->first('prazo', ':message') }}</span>
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="form-group {{ $errors->has('valor_estimado') ? 'has-error' : '' }}">
                        {!! Form::label('valor_estimado','Valor Estimado', array('class' => 'control-label')) !!}
                        {!! Form::text('valor_estimado', null, array('class' => 'form-control')) !!}
                        <span class="help-block">{{ $errors->first('valor_estimado', ':message') }}</span>
                    </div>
                </div>
                
                @if(isset($ocorrencia))
                    <div class="col-sm-3">
                        <div class="form-group  {{ $errors->has('valor_final') ? 'has-error' : '' }}">
                            {!! Form::label('valor_final','Valor Final', array('class' => 'control-label')) !!}
                            {!! Form::text('valor_final', null, array('class' => 'form-control')) !!}
                            <span class="help-block">{{ $errors->first('valor_final', ':message') }}</span>
                        </div>
                    </div>
                @endif
            </div>
            <div class="row">
                <div class="col-sm-2">
                    <div class="form-group {{ $errors->has('cidade') ? 'has-error' : '' }}">
                        {!! Form::label('cidade','Cidade', array('class' => 'control-label')) !!}
                        {!! Form::text('cidade', null, array('class' => 'form-control', 'autocomplete' => 'off')) !!}
                        <input type="hidden" name="cidade_id" id="cidade_id" value="@if(isset($ocorrencia)) {{ $ocorrencia->cidade_id }} @endif">
                        <span class="help-block">{{ $errors->first('cidade', ':message') }}</span>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group  {{ $errors->has('endereco') ? 'has-error' : '' }}">
                        {!! Form::label('endereco','Endereço', array('class' => 'control-label')) !!}
                        {!! Form::text('endereco', null, array('class' => 'form-control')) !!}
                        <span class="help-block">{{ $errors->first('endereco', ':message') }}</span>
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="form-group  {{ $errors->has('bairro') ? 'has-error' : '' }}">
                        {!! Form::label('bairro','Bairro', array('class' => 'control-label')) !!}
                        {!! Form::text('bairro', null, array('class' => 'form-control')) !!}
                        <span class="help-block">{{ $errors->first('bairro', ':message') }}</span>
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="form-group  {{ $errors->has('cep') ? 'has-error' : '' }}">
                        {!! Form::label('cep','CEP', array('class' => 'control-label')) !!}
                        {!! Form::text('cep', null, array('class' => 'form-control')) !!}
                        <span class="help-block">{{ $errors->first('cep', ':message') }}</span>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group  {{ $errors->has('observacao') ? 'has-error' : '' }}">
                        {!! Form::label('observacao','Observação', array('class' => 'control-label')) !!}
                        {!! Form::text('observacao', null, array('class' => 'form-control')) !!}
                        <span class="help-block">{{ $errors->first('observacao', ':message') }}</span>
                    </div>
                </div>
            </div>
      <!-- fim dos campos -->
        </div><!-- /.box-body -->
        <div class="box-footer">
      <button type="reset" class="btn btn-sm btn-default">
        <span class="glyphicon glyphicon-remove-circle"></span> 
        Limpar
      </button>

            <button type="submit" class="btn btn-success pull-right">
        <span class="glyphicon glyphicon-ok-circle"></span> 
            @if (isset($ocorrencia))
                Editar
            @else
                Criar
            @endif
      </button>
        </div><!-- /.box-footer -->
    </form>
</div><!-- /.box -->

@section('scripts')
<script>
    $(function(){
        var x =$('input#prazo');
        if (x.val() == "") { x.val(7)}

        $(".iframe").colorbox({
            iframe: true,
            width: "80%",
            height: "80%"
        });
    });
</script>
@endsection

@stop
