@extends('adminlte')
@section('content-header')


<!-- You can dynamically generate breadcrumbs here -->

        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Ordem de Serviço</a></li>
            <li class="active">Visualização</li>
        </ol>
        <br>

@endsection
{{-- Content --}}
@section('content')


        <div class="box box-success">
            <div class="box-header with-border">

              <h3 class="box-title">Filtro de pesquisa</h3>
              <div class="pull-right">
                <div class="pull-right">
                    <a href="{!! URL::to('admin/ocorrencia/create') !!}"
                       class="btn btn-sm  btn-success"><span
                                class="glyphicon glyphicon-plus-sign"></span>
                    Novo</a>
                </div>
            </div>
            </div><!-- /.box-header -->
            <!-- form start -->
            {!! Form::open( array('url' => URL::to('admin/ocorrencia/filtro'), 'method' => 'get', 'class' => 'bf')) !!}
              <div class="box-body">

                <!-- início dos campos -->
        
            <div class="row">
                <!-- Cliente -->
                <div class="col-sm-2">
                    <div class="form-group {{ $errors->has('cliente_search') ? 'has-error' : '' }}">
                        {!! Form::label('cliente_search_search','Cliente', array('class' => 'control-label')) !!}
                        {!! Form::text('cliente_search', Input::get('cliente_search'), array('class' => 'form-control input-sm')) !!}
                        <span class="help-block">{{ $errors->first('cliente_search', ':message') }}</span>
                    </div>
                </div>

                <!-- Tipo -->
                <div class="col-md-2">
                    <?php $opcoes = ['' => 'Selecione', 'remocao' => 'Remoção', 'retirada' => 'Retirada', 'entrega' => 'Entrega']; ?>
                    <div class="form-group {{ $errors->has('tipo') ? 'has-error' : '' }}">
                        {!! Form::label('tipo','Tipo', array('class' => 'control-label')) !!}
                        {!! Form::select('tipo', $opcoes,  Input::get('tipo'), ['class' => 'form-control input-sm']) !!}
                        <span class="help-block">{{ $errors->first('tipo', ':message') }}</span>          
                    </div>
                </div>
                <!-- Inicio -->
                <div class="col-sm-2">
                    <div class="form-group {{ $errors->has('inicio') ? 'has-error' : '' }}">
                        {!! Form::label('inicio','Data Inicio', array('class' => 'control-label')) !!}
                        {!! Form::text('inicio', Input::get('inicio'), array('class' => 'form-control input-sm data')) !!}
                        <span class="help-block">{{ $errors->first('inicio', ':message') }}</span>
                    </div>
                </div>
                <!-- Fim -->
                <div class="col-sm-2">
                    <div class="form-group {{ $errors->has('fim') ? 'has-error' : '' }}">
                        {!! Form::label('fim','Data Fim', array('class' => 'control-label')) !!}
                        {!! Form::text('fim', Input::get('fim'), array('class' => 'form-control input-sm data')) !!}
                        <span class="help-block">{{ $errors->first('fim', ':message') }}</span>
                    </div>
                </div>

                <!-- Status -->
                <div class="col-md-2">
                    <?php $opcoes = ['' => 'Selecione', '1' => 'Concluído', '0' => 'Aberto']; ?>
                    <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                        {!! Form::label('status','Status', array('class' => 'control-label')) !!}
                        {!! Form::select('status', $opcoes,  Input::get('status'), ['class' => 'form-control input-sm']) !!}
                        <span class="help-block">{{ $errors->first('status', ':message') }}</span>          
                    </div>
                </div>
            </div>
            <div class="row"><div class="col-lg-3">
                
                    <button type="submit" class="btn btn-success btn-sm">
                        <span class="glyphicon glyphicon-search"></span> 
                            Pesquisar
                    </button>
            </div>
            </div>

            
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Nº</th>
                        <th>Serviço</th>
                        <th>Tipo</th>
                        <th>Data</th>
                        <th>Cliente</th>
                        <th>Valor. Estimado</th>
                        <th>Valor Final</th>    
                        <th>Nº do Container</th>
                        <th>Status</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($oss as $os )
                    <tr>
                        <td>{{ $os->id }}</td>
                        <td>{{ $os->servico }}</td>
                        <td>{{ $os->tipo }}</td>
                        <td>{{ $os->data }}</td>
                        <td>{{ $os->nome . $os->nome_fantasia }}</td>
                        <td>{{ $os->valor_estimado }}</td>
                        <td>{{ $os->valor_final }}</td>
                        <td>{{ $os->numero_container }}</td>
                        <td>@if($os->status == 1) <span class="label label-success">Concluído</span> @else <span class="label label-warning">Aberto</span>@endif</td>
                        <td><a href="{{{ URL::to('admin/ocorrencia/' . $os->id . '/edit' ) }}}" class="btn btn-success btn-xs " ><span class="glyphicon glyphicon-pencil"></span></a>
                            <a href="{{{ URL::to('admin/ocorrencia/' . $os->id . '/delete' ) }}}" class="btn btn-xs btn-danger iframe"><span class="glyphicon glyphicon-trash"></span></a>
                            <a target="_blank" href="{{{ URL::to('admin/ocorrencia/' . $os->id . '/pdf' ) }}}" class="btn btn-xs btn-success "><span class="fa fa-file-pdf-o"></span></a>
                            </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        

        <!-- fim dos campos -->
      </div><!-- /.box-body -->
      <div class="box-footer">
        
        

            
      </div><!-- /.box-footer -->
    </form>
    </div><!-- /.box -->



<!-- asdfhs -->


    

    
    <?php echo $oss->appends(Request::except('page'))->render(); ?>
@endsection

{{-- Scripts --}}
@section('scripts')

<script>
    
    $(document).ready(function(){


        $(".iframe").colorbox({
            iframe: true,
            width: "80%",
            height: "80%"
        });
    });
</script>
@parent

@endsection
