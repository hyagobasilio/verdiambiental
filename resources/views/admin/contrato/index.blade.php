@extends('adminlte')
@section('content-header')
<h1>
    Contrato
    <small>visualização</small>
</h1>

<!-- You can dynamically generate breadcrumbs here -->
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Contrato</a></li>
    <li class="active">Visualização</li>
</ol>
@endsection
{{-- Content --}}
@section('content')
    
<br>
<div class="row">
    <div class="col-md-12">
        
        <div class="box box-primary">
            <div class="box-header ui-sortable-handle" style="cursor: move;">
                <!-- <i class="ion ion-clipboard"></i> -->

                <h3 class="box-title">Contratos</h3>
                <div class="pull-right">
                    <a href="{{{ URL::to('admin/contrato/create') }}}"
                        class="btn btn-sm  btn-success"><span
                            class="glyphicon glyphicon-plus-sign"></span> Novo Contrato</a>
                </div>

                

                
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                {!! Form::open( array('url' => URL::to('admin/contrato'), 'method' => 'get', 'class' => 'bf')) !!}
                    <div class="row">
                        <!-- Cliente -->
                       <div class="col-sm-3">
                           <div class="form-group {{ $errors->has('cliente_search') ? 'has-error' : '' }}">
                               {!! Form::label('cliente_search','Cliente', array('class' => 'control-label')) !!}
                               {!! Form::text('cliente_search', Input::get('cliente_search'), array('class' => 'form-control input-sm')) !!}
                               <span class="help-block">{{ $errors->first('cliente_search', ':message') }}</span>
                           </div>
                       </div>
                       
                       <!-- Descrição -->
                       <div class="col-sm-3">
                           <div class="form-group {{ $errors->has('descricao') ? 'has-error' : '' }}">
                               {!! Form::label('descricao','Descrição', array('class' => 'control-label')) !!}
                               {!! Form::text('descricao', Input::get('descricao'), array('class' => 'form-control input-sm')) !!}
                               <span class="help-block">{{ $errors->first('descricao', ':message') }}</span>
                           </div>
                       </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3">
                
                            <button type="submit" class="btn btn-success btn-sm">
                                <span class="glyphicon glyphicon-search"></span> 
                                Pesquisar
                            </button>
                        </div>
                    </div>


                </form>

                <table class="table">
                    <thead>
                        <tr>
                            <th>Cód.</th>
                            <th>Cliente</th>
                            <th>Descrição</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($contratos as $contrato)
                        <tr>
                            <td>{{ $contrato->id }}</td>
                            <td>{{ $contrato->nome . $contrato->nome_fantasia }}</td>
                            <td>{{ $contrato->descricao }}</td>
                            <td>
                                <a href="{{{ URL::to('admin/contrato/' . $contrato->id ) }}}" class="iframe btn btn-success btn-xs">
                                    <i class="fa fa-eye"></i>
                                    Visualizar
                                </a>
                                <a href="{{{ URL::to('admin/contrato/' . $contrato->id . '/edit' ) }}}" class="btn btn-success btn-xs">
                                    <i class="fa fa-edit"></i>
                                    Editar
                                </a>
                            
                            <a href="{{{ URL::to('admin/contrato/' . $contrato->id . '/delete' ) }}}" class="iframe btn btn-danger btn-xs">
                                <i class="fa fa-trash-o"></i>
                                Remover
                            </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix no-border">
                <div class="box-tools pull-right">
                    <?php echo $contratos->appends(Request::except('page'))->render(); ?>
                </div>
                
            </div>
          </div>
    </div>
</div>
@endsection

{{-- Scripts --}}
@section('scripts')

<script>
    
    $(document).ready(function(){


        $(".iframe").colorbox({
            iframe: true,
            width: "80%",
            height: "80%"
        });
    });
</script>
   
@endsection
