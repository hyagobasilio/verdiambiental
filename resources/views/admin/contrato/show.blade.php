<?php 
	use App\Contrato;
 ?>
{{-- @extends('admin.layouts.modal2') --}}
@section('styles')
<style>
	table tbody  tr td:first-child {
		font-weight: bold;
		width: 15%;
	}
</style>
@endsection
@section('content')


		<div class="box box-success">
	        <div class="box-header with-border">
	        	<img width="55" height="50" src="{{ asset('/assets/images/verdi-ambiental.png') }}" alt="Verdi Ambiental">
	          <h3 class="box-title">Contrato</h3>
	          <div class="pull-right" style="text-align:right">
	          	{{ date('d/m/Y') }} <br> {{ date('h:i:s') }}
	          </div>

	        </div><!-- /.box-header -->

          	<div class="box-body">

			<table class="table table-bordered table-condensed">
				<tbody>
					<tr>
						<td>Cliente</td>
						<td>{{ $contrato->cliente->nome . $contrato->cliente->nome_fantasia}}</td>
					</tr>
					<tr>
						<td>Nº do Contrato</td>
						<td>{{ $contrato->numero_contrato }}</td>
					</tr>
					<tr>
						<td>Responsável</td>
						<td>{{ $contrato->responsavel }}</td>
					</tr>
					<tr>
						<td>CPF Responsável</td>
						<td>{{ $contrato->responsavel_cpf }}</td>
					</tr>
					<tr>
						<td>Data Início</td>
						<td>{{ $contrato->data }}</td>
					</tr>
					<tr>
						<td>Descrição</td>
						<td>{{ $contrato->descricao }}</td>
					</tr>
					<tr>
						<td>Material</td>
						<td>{{ $contrato->material }}</td>
					</tr>
					<tr>
						<td>Quantidade</td>
						<td>{{ $contrato->quantidade }}</td>
					</tr>
					<tr>
						<td>Concessão recipiente</td>
						<td>{{ $contrato->concessao_recipiente }}</td>
					</tr>
					<tr>
						<td>Tipo contrato</td>
						<td>{{ $contrato->tipo_contrato }}</td>
					</tr>
					<tr>
						<td>Valor</td>
						<td>{{ $contrato->valor }}</td>
					</tr>
					<tr>
						<td>Valor Entulho Sujo</td>
						<td>{{ $contrato->valor_sujo }}</td>
					</tr>
					<tr>
						<td>Material</td>
						<td>{{ $contrato->material }}</td>
					</tr>
					<tr>
						<td>Quantidade remoção semanal</td>
						<td>{{ $contrato->qtd_remocoes_sema }}</td>
					</tr>
					<tr>
						<td>Dias da semana</td>
						<td>{{ implode(', ', Contrato::getDias($contrato)) }}</td>
					</tr>
					<tr>
						<td>Prazo Vigência</td>
						<td>{{ $contrato->prazo_vigencia }} Meses</td>
					</tr>
					<tr>
						<td>Tipo Resíduo</td>
						<td>Classe IIA</td>
					</tr>
					<tr>
						<td>Endereços</td>
						<td>
						@foreach($contrato->enderecos as $endereco)
						{{ $endereco->endereco . ' '. $endereco->numero. ', bairro: ' . $endereco->bairro }},  {{ $endereco->cidade->nome . ' - ' . $endereco->cidade->uf }} @if(!empty($endereco->cep)), <b>CEP:</b> {{ $endereco->cep }} @endif<br>
						@endforeach
						</td>
					</tr>
					
				</tbody>
			</table>
	            


          	</div><!-- /.box-body -->
          	<div class="box-footer">
          		

	            <a href="{{ url('admin/contrato/'.$contrato->id.'/pdf')}}" class="btn btn-sm btn-success pull-right">
					<span class="fa fa-file-pdf-o"></span> 
					   Download PDF
				</a>
          	</div><!-- /.box-footer -->
	        
	      </div><!-- /.box -->

@stop
