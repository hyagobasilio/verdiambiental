{{-- @extends('adminlte') --}}

@section('content')


		<div class="box box-success">
	        <div class="box-header with-border">
	          <h3 class="box-title">Cadastro de Contrato</h3>
	        </div><!-- /.box-header -->
	        <!-- form start -->
	    
	      	@if (isset($contrato))
			{!! Form::model($contrato, array('url' => URL::to('admin/contrato/' . $contrato->id . '/edit'), 'method' => 'put', 'class' => 'form-horizontal bf', 'files'=> true)) !!}
			@else
			{!! Form::open(array('url' => URL::to('admin/contrato'), 'method' => 'post', 'class' => 'form-horizontal bf', 'files'=> true)) !!}
			@endif
			

			
	          <div class="box-body">

                <div class="form-group  {{ $errors->has('cliente') ? 'has-error' : '' }}">
                    {!! Form::label('cliente','Cliente', array('class' => 'col-sm-2 control-label')) !!}
                    <div class="col-sm-10">
                    <?php $cliente = isset($contrato->cliente) ? $contrato->cliente->nome . $contrato->cliente->nome_fantasia : null; ?>
                        {!! Form::text('cliente', $cliente, array('class' => 'form-control input-sm', 'autocomplete' => 'off')) !!}
                        <input type="hidden" name="cliente_id" id="cliente_id" value="@if(isset($contrato)) {{ $contrato->cliente_id }} @endif">
                    </div>
                </div>

                <div class="form-group  {{ $errors->has('numero_contrato') ? 'has-error' : '' }}">
                 {!! Form::label('numero_contrato','Nº contrato', array('class' => 'col-sm-2 control-label')) !!}
                  <div class="col-sm-10">
                    {!! Form::text('numero_contrato', null, array('class' => 'form-control input-sm')) !!}
                     <span class="help-block">{{ $errors->first('numero_contrato', ':message') }}</span>
                  </div>
                </div>
                <div class="form-group  {{ $errors->has('responsavel_cpf') ? 'has-error' : '' }}">
                 {!! Form::label('responsavel_cpf', 'CPF Responsável', array('class' => 'col-sm-2 control-label')) !!}
                  <div class="col-sm-10">
                    {!! Form::text('responsavel_cpf', null, array('class' => ' cpf form-control input-sm')) !!}
                     <span class="help-block">{{ $errors->first('responsavel_cpf', ':message') }}</span>
                  </div>
                </div>
                <div class="form-group  {{ $errors->has('responsavel') ? 'has-error' : '' }}">
                 {!! Form::label('responsavel','Responsável', array('class' => 'col-sm-2 control-label')) !!}
                  <div class="col-sm-10">
                    {!! Form::text('responsavel', null, array('class' => 'form-control input-sm')) !!}
                     <span class="help-block">{{ $errors->first('responsavel', ':message') }}</span>
                  </div>
                </div>

	            <div class="form-group  {{ $errors->has('data') ? 'has-error' : '' }}">
                 {!! Form::label('data','Data Início', array('class' => 'col-sm-2 control-label')) !!}
                  <div class="col-sm-10">
                    {!! Form::text('data', null, array('class' => 'form-control input-sm data')) !!}
                     <span class="help-block">{{ $errors->first('data', ':message') }}</span>
                  </div>
                </div>

                <div class="form-group  {{ $errors->has('descricao') ? 'has-error' : '' }}">
                 {!! Form::label('descricao','Descrição', array('class' => 'col-sm-2 control-label')) !!}
                  <div class="col-sm-10">
                    {!! Form::text('descricao', null, array('class' => 'form-control input-sm')) !!}
                     <span class="help-block">{{ $errors->first('descricao', ':message') }}</span>
                  </div>
                </div>

				<!-- Material -->
                <?php $opcoes = ['entulho' => 'Entulho','lixo' => 'Lixo']; ?>
                <div class="form-group {{ $errors->has('material') ? 'has-error' : '' }}">
                    {!! Form::label('material','Material', array('class' => 'control-label col-sm-2')) !!}
                    <div class="col-sm-10">
                    	{!! Form::select('material', $opcoes, null, ['class' => 'form-control input-sm']) !!}
                    	<span class="help-block">{{ $errors->first('material', ':message') }}</span>          
                    </div>
                </div>
                
                <div class="form-group  {{ $errors->has('valor_sujo') ? 'has-error' : '' }}">
                    {!! Form::label('valor_sujo','Valor Entulho Sujo', array('class' => 'col-sm-2 control-label')) !!}
                    <div class="col-sm-10">
                        {!! Form::text('valor_sujo', null, array('class' => 'form-control input-sm valor')) !!}
                        <span class="help-block">{{ $errors->first('valor_sujo', ':message') }}</span>
                    </div>
                </div>
                
                <div class="form-group  {{ $errors->has('valor') ? 'has-error' : '' }}">
                 	{!! Form::label('valor','Valor', array('class' => 'col-sm-2 control-label')) !!}
                  	<div class="col-sm-10">
                    	{!! Form::text('valor', null, array('class' => 'form-control input-sm valor')) !!}
                     	<span class="help-block">{{ $errors->first('valor', ':message') }}</span>
                  	</div>
                </div>
                
                <!-- Quantidade -->
                <?php $opcoes = ['200 L' => '200 L', 
                '300 L' => '300 L', 
                '500 L' => '500 L', 
                '1.6 M3'=> '1.6 M3', 
                '5 M3'  => '5 M3', 
                '7 M3'  => '7 M3']; ?>
                <div class="form-group {{ $errors->has('quantidade') ? 'has-error' : '' }}">
                    {!! Form::label('quantidade','Quantidade', array('class' => 'control-label col-sm-2')) !!}
                    <div class="col-sm-10">
                        {!! Form::select('quantidade', $opcoes, null, ['class' => 'form-control input-sm']) !!}
                        <span class="help-block">{{ $errors->first('quantidade', ':message') }}</span>          
                    </div>
                </div>

                <!-- Concessão Recipiente -->
                <?php $opcoes = ['NÃO' => 'NÃO', 
                'BOMBONA' => 'BOMBONA', 
                'CAIXA 1.6'=> 'CAIXA 1.6', 
                'CAIXA 5'   => 'CAIXA 5', 
                'CAIXA 7'   => 'CAIXA 7'
                ]; ?>
                <div class="form-group {{ $errors->has('concessao_recipiente') ? 'has-error' : '' }}">
                    {!! Form::label('concessao_recipiente','Concessão Recipiente', array('class' => 'control-label col-sm-2')) !!}
                    <div class="col-sm-10">
                        {!! Form::select('concessao_recipiente', $opcoes, null, ['class' => 'form-control input-sm']) !!}
                        <span class="help-block">{{ $errors->first('concessao_recipiente', ':message') }}</span>          
                    </div>
                </div>

                <!-- Tipo Contrato -->
                <?php $opcoes = ['mensal' => 'Mensal', 'remocao' => 'Remoção']; ?>
                <div class="form-group {{ $errors->has('tipo_contrato') ? 'has-error' : '' }}">
                    {!! Form::label('tipo_contrato','Tipo Contrato', array('class' => 'control-label col-sm-2')) !!}
                    <div class="col-sm-10">
                        {!! Form::select('tipo_contrato', $opcoes, null, ['class' => 'form-control input-sm']) !!}
                        <span class="help-block">{{ $errors->first('tipo_contrato', ':message') }}</span>          
                    </div>
                </div>


                <div id="dias-qtd">
                    
                    
    				
    				<!-- Qtd. Remoção Semanal -->
                    <?php $opcoes = [0,1,2,3,4,5,6]; ?>
                    <div class="form-group {{ $errors->has('qtd_remocoes_sema') ? 'has-error' : '' }}">
                        {!! Form::label('qtd_remocoes_sema','Qtd. Remoção Semanal', array('class' => 'control-label col-sm-2')) !!}
                        <div class="col-sm-10">
                        	{!! Form::select('qtd_remocoes_sema', $opcoes, null, ['class' => 'form-control input-sm']) !!}
                        	<span class="help-block">{{ $errors->first('qtd_remocoes_sema', ':message') }}</span>          
                        </div>
                    </div>

                    <div class="form-group">
                    	{!! Form::label('qtd_remocoes_sema','Dias', array('class' => 'control-label col-sm-2')) !!}
                    	<div class="col-sm-10">
                    		<label class="checkbox-inline" for="segunda">
                    			{!! Form::checkbox('segunda', '1' ) !!}
    					    	Segunda
    					    </label>  
                    		<label class="checkbox-inline" for="terca">
                    			{!! Form::checkbox('terca', '1') !!}
    					    	Terça
    					    </label>  
                    		<label class="checkbox-inline" for="quarta">
                    			{!! Form::checkbox('quarta', '1') !!}
    					    	Quarta
    					    </label>  
                    		<label class="checkbox-inline" for="quinta">
    					    	{!! Form::checkbox('quinta', '1') !!}
    					    	Quinta
    					    </label>  
                    		<label class="checkbox-inline" for="sexta">
    					    	{!! Form::checkbox('sexta', '1') !!}
    					    	Sexta
    					    </label>  
                    		<label class="checkbox-inline" for="sabado">
    					    	{!! Form::checkbox('sabado', '1') !!}
    					    	Sábado
    					    </label>  
                        </div>
                    </div>
                </div> 

                <!-- Prazo Vigência -->
                <?php $opcoes = [6 => '6 Meses', 12 => '12 Meses']; ?>
                <div class="form-group {{ $errors->has('prazo_vigencia') ? 'has-error' : '' }}">
                    {!! Form::label('prazo_vigencia','Prazo Vigência', array('class' => 'control-label col-sm-2')) !!}
                    <div class="col-sm-10">
                    	{!! Form::select('prazo_vigencia', $opcoes, null, ['class' => 'form-control input-sm']) !!}
                    	<span class="help-block">{{ $errors->first('prazo_vigencia', ':message') }}</span>          
                    </div>
                </div>
				
				<!-- Tipo Resíduo -->
                <?php $opcoes = ['Classe IIA']; ?>
                <div class="form-group {{ $errors->has('tipo_residuo') ? 'has-error' : '' }}">
                    {!! Form::label('tipo_residuo','Tipo Resíduo', array('class' => 'control-label col-sm-2')) !!}
                    <div class="col-sm-10">
                    	{!! Form::select('tipo_residuo', $opcoes, null, ['class' => 'form-control input-sm']) !!}
                    	<span class="help-block">{{ $errors->first('tipo_residuo', ':message') }}</span>          
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <h3>Endereços</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        @if(isset($contrato))
                        <a href="{{ url('admin/endereco/create?idContrato='.$contrato->id.'&idCliente='.$contrato->cliente_id) }}" class="btn btn-success iframe">Adicionar endereço</a>
                        @else
                        <label class="alert alert-info">Para adicionar endereços é necessário salvar o contrato primeiro</label>
                        @endif
                    </div>
                </div>

                <table id="tabela" class="table">
                    <thead>
                        <tr>
                            <th>Cidade / UF</th>
                            <th>Endereço</th>
                            <th>Nº</th>
                            <th>Bairro</th>
                            <th>CEP</th>
                            <th>Complemento</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>


	          	</div><!-- /.box-body -->
	          	<div class="box-footer">
	          		<a href="{{ URL::to('/admin/users') }}" class="btn btn-sm btn-danger close_popup">
						<span class="glyphicon glyphicon-ban-circle"></span> 
					Cancelar
					</a>
					<button type="reset" class="btn btn-sm btn-default">
						<span class="glyphicon glyphicon-remove-circle"></span> 
						Limpar
					</button>

		            <button type="submit" class="btn btn-success pull-right">
						<span class="glyphicon glyphicon-ok-circle"></span> 
                            @if (isset($contrato))
                                Alterar
                            @else
                                Salvar
                            @endif
                    </button>
              </div><!-- /.box-footer -->
            </form>
          </div><!-- /.box -->

@stop

@section('scripts')
<script>
var criada = false;
var oTable;
    function escondeDias() {
        valor = $( "#material option:selected" ).val();      
            
            if (valor == 'entulho') {
                $('#dias-qtd').addClass('hide');
                $('input[type="checkbox"]').attr('checked', false);
                $('select#qtd_remocoes_sema option:first').attr('selected', true);   
            }else {
                $('#dias-qtd').removeClass('hide');
            }
    }
    
    function montaTabelaEnderecos(id)
    {
        url = "{{ url('admin/endereco/cliente/{idCliente}') }}".replace('{idCliente}', id);
        console.log(url);
        if(!criada){

        oTable = $('#tabela').DataTable({
            "sDom": "<'row'<'col-md-6'l><'col-md-6'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
            "sPaginationType": "full_numbers",
            "pageLength": 5,
            "oLanguage": {
                "sProcessing": "Processando",
                "sLengthMenu": "",
                "sZeroRecords": "Nenhum resultado",
                "sInfo": "",
                "sEmptyTable": "Tabela vazia",
                "sInfoEmpty": "Vazio",
                "sInfoFiltered": "Filtro",
                "sInfoPostFix": "",
                "sSearch": "Pesquisar:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Início",
                    "sPrevious": "Anterior",
                    "sNext": "Próximo",
                    "sLast": "Último"
                }
            },
            "processing": true,
            "serverSide": true,
            "ajax": url,
            "fnDrawCallback": function (oSettings) {
                $('.paginate_button').addClass('btn-sm');
            
                $(".iframe").colorbox({
                    iframe: true,
                    width: "80%",
                    height: "80%",
                    onClosed: function () {
                        oTable.ajax.reload();
                    }
                });
                
            }
        });
        }
        else {
            oTable.ajax.url = url;
            oTable.ajax.reload()
        }
        criada = true;
    }
    $(function(){  

        escondeDias();

        $( "#material" ).on('change', function(){
            escondeDias();
        });
        @if(isset($contrato))
            montaTabelaEnderecos({{ $contrato->cliente_id }});
        @endif

        $( "#cliente" ).on('change', function(){
           montaTabelaEnderecos($("#cliente_id").val());
        });


    });
</script>
@endsection