{{-- @extends('admin.layouts.modal2') --}}
@section('content-header')
<h1>
    Clientes
    <small>{{ isset($user) ? "Editar" : "Cadastro" }}</small>
</h1>

<!-- You can dynamically generate breadcrumbs here -->
<ol class="breadcrumb">
    <li><a href="{{ URL::to('admin/materias') }}"><i class="fa fa-dashboard"></i> Clientes</a></li>
    <li class="active">{{ isset($cliente) ? "Editar" : "Cadastro" }}</li>
</ol>
@endsection
@section('content')


		<div class="box box-info">
	        <div class="box-header with-border">
	          <h3 class="box-title">Cadastro de Clientes</h3>
	        </div><!-- /.box-header -->
	        <!-- form start -->
	        @if (isset($cliente))
			{!! Form::model($cliente, array('url' => URL::to('admin/cliente') . '/' . $cliente->id, 'method' => 'put', 'class' => 'bf', 'files'=> true)) !!}
			@else
			{!! Form::open(array('url' => URL::to('admin/cliente'), 'method' => 'post', 'class' => 'bf', 'files'=> true)) !!}
			@endif
			<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
	          <div class="box-body">

				<!-- início dos campos -->
		
		<div class="row">
			<div class="col-md-2">
				<div class="form-group has-success">
				  <label class="control-label" for="tipo-pessoa">Tipo Pessoa</label>
				  <div class="controls">
				    <select id="tipo-pessoa" name="tipo-pessoa" class="form-control">
						<option value="juridica" {{ (isset($cliente) && empty($cliente->nome) ) ? 'selected': '' }}>Jurídica</option>
				    	<option value="fisica" {{ (isset($cliente) && empty($cliente->cnpj) ) ? 'selected': '' }}>Física</option>
				    </select>
				  </div>
				</div>
			</div>

				       
			<div class="pessoa-fisica hide">
				
		    	<div class="col-md-5">
		    		<div class="form-group   {{ $errors->has('nome') ? 'has-error' : '' }}">
			            {!! Form::label('nome','Nome', array('class' => 'control-label')) !!}
		                {!! Form::text('nome', null, array('class' => 'form-control')) !!}
		                <span class="help-block">{{ $errors->first('nome', ':message') }}</span>
			        </div>
		    	</div>
			
		        <div class="col-md-5">
		        	<div class="form-group {{ $errors->has('cpf') ? 'has-error' : '' }}">
		        	    {!! Form::label('cpf', 'CPF', array('class' => 'control-label')) !!}
	        	        {!! Form::text('cpf', null, array('class' => 'cpf form-control')) !!}
	        	        <span class="help-block">{{ $errors->first('cpf', ':message') }}</span>
	        	        
		        	</div>
		        </div>
			</div>

			<div class="pessoa-juridica">
				<div class="col-md-3">
		    		<div class="form-group   {{ $errors->has('cnpj') ? 'has-error' : '' }}">
			            {!! Form::label('cnpj','CNPJ', array('class' => 'control-label')) !!}
		                {!! Form::text('cnpj', null, array('class' => 'form-control')) !!}
		                <span class="help-block">{{ $errors->first('cnpj', ':message') }}</span>
			        </div>
		    	</div>
				<div class="col-md-3">
		    		<div class="form-group   {{ $errors->has('razao_social') ? 'has-error' : '' }}">
			            {!! Form::label('razao_social','Razão Social', array('class' => 'control-label')) !!}			            
		                {!! Form::text('razao_social', null, array('class' => 'form-control')) !!}
		                <span class="help-block">{{ $errors->first('razao_social', ':message') }}</span>
			        </div>
		    	</div>

		    	<div class="col-md-4">
		    		<div class="form-group   {{ $errors->has('nome_fantasia') ? 'has-error' : '' }}">
			            {!! Form::label('nome_fantasia','Nome Fantasia', array('class' => 'control-label')) !!}
		                {!! Form::text('nome_fantasia', null, array('class' => 'form-control')) !!}
		                <span class="help-block">{{ $errors->first('nome_fantasia', ':message') }}</span>
			        </div>
		    	</div>
			</div>

		</div>
		<div class="row">
		
		<!-- Tipo Serviço -->
		<div class="col-md-3">
			<div class="form-group {{ $errors->has('tipo_plano') ? 'has-error' : '' }} ">
			  <label class="control-label" for="tipo_plano">Tipo Plano</label>
			  <div class="controls">
			    <select id="tipo_plano" name="tipo_plano" class="form-control">
			    	<option value="">Selecione</option>
			    	<option value="simples" {{ (isset($cliente) && ($cliente->tipo_plano == 'basico') ) ? 'selected': '' }}>Básico</option>
			    	<option value="simples" {{ (isset($cliente) && ($cliente->tipo_plano == 'simples') ) ? 'selected': '' }}>Simples</option>
			    	<option value="especial" {{ (isset($cliente) && ($cliente->tipo_plano == 'especial') ) ? 'selected': '' }}>Especial</option>
			    </select>
			  </div>
			</div>
		</div>
		
		<!-- Telefone Fixo -->
		<div class="col-md-3">
	        <div class="form-group  {{ $errors->has('telefone1') ? 'has-error' : '' }}">
	            {!! Form::label('telefone1', 'Tel. Fixo', array('class' => 'control-label')) !!}
	            {!! Form::text('telefone1', null, array('class' => 'telefone form-control')) !!}
                <span class="help-block">{{ $errors->first('telefone1', ':message') }}</span>
	        </div>
		</div>

		<!-- Telefone Cel -->
        <div class="col-md-3">
        	<div class="form-group  {{ $errors->has('telefone2') ? 'has-error' : '' }}">
        	    {!! Form::label('telefone2', 'Celular', array('class' => 'control-label')) !!}
    	        {!! Form::text('telefone2', null, array('class' => 'telefone form-control')) !!}
    	        <span class="help-block">{{ $errors->first('telefone2', ':message') }}</span>
        	</div>
        </div>
		<!-- Email -->
        <div class="col-md-3">
        	<div class="form-group  {{ $errors->has('email') ? 'has-error' : '' }}">
        	    {!! Form::label('email', 'Email', array('class' => 'control-label')) !!}
    	        {!! Form::text('email', null, array('class' => 'form-control')) !!}
    	        <span class="help-block">{{ $errors->first('email', ':message') }}</span>
        	 
        	</div>
        </div>
        	<div class="col-md-12">
	    		<div class="form-group   {{ $errors->has('observacao') ? 'has-error' : '' }}">
		            {!! Form::label('observacao','Observação', array('class' => 'control-label')) !!}
	                {!! Form::text('observacao', null, array('class' => 'form-control')) !!}
	                <span class="help-block">{{ $errors->first('observacao', ':message') }}</span>
		        </div>
	    	</div>
		</div>

			<div class="row">
				<div class="col-md-12">
	        		<h3>Endereço</h3>
	        	</div>
		        <div class="col-md-4">
		        	<div class="form-group  {{ $errors->has('cidade') ? 'has-error' : '' }}">
		        	    {!! Form::label('cidade', 'Cidade', array('class' => 'control-label')) !!}
		        	   
	        	        {!! Form::text('cidade', null, array('id'=>'cidade','autocomplete' => 'off','class' => 'form-control')) !!}
	        	        <input type="hidden" id="cidade_id" name="cidade_id" value="@if(isset($cliente)){{$cliente->cidade_id}}@endif">
	        	        <span class="help-block">{{ $errors->first('cidade', ':message') }}</span>
	        	    
		        	</div>
		        </div>
	
		        <div class="col-md-6">
			        <div class="form-group  {{ $errors->has('endereco') ? 'has-error' : '' }}">
			            {!! Form::label('endereco', 'Endereço', array('class' => 'control-label')) !!}
		                {!! Form::text('endereco', null, array('class' => 'form-control')) !!}
		                <span class="help-block">{{ $errors->first('endereco', ':message') }}</span>
		          
			        </div>
		        </div>
		        <div class="col-md-2">
			        <div class="form-group  {{ $errors->has('numero') ? 'has-error' : '' }}">
			            {!! Form::label('numero', 'Número', array('class' => 'control-label')) !!}
		                {!! Form::text('numero', null, array('class' => 'form-control')) !!}
		                <span class="help-block">{{ $errors->first('numero', ':message') }}</span>
		          
			        </div>
		        </div>
            </div>
			<div class="row">
				<div class="col-sm-3">
                    <div class="form-group  {{ $errors->has('cep') ? 'has-error' : '' }}">
                        {!! Form::label('cep','CEP', array('class' => 'control-label')) !!}
                        {!! Form::text('cep', null, array('class' => 'form-control input-sm')) !!}
                        <span class="help-block">{{ $errors->first('cep', ':message') }}</span>
                    </div>
                </div>
		        <div class="col-md-3">
			        <div class="form-group  {{ $errors->has('bairro') ? 'has-error' : '' }}">
			            {!! Form::label('bairro', 'Bairro', array('class' => 'control-label')) !!}
		                {!! Form::text('bairro', null, array('class' => 'form-control')) !!}
		                <span class="help-block">{{ $errors->first('bairro', ':message') }}</span>
			        </div>
		        </div>
		        
		        <div class="col-md-6">
		    		<div class="form-group   {{ $errors->has('complemento') ? 'has-error' : '' }}">
			            {!! Form::label('complemento','Complemento', array('class' => 'control-label')) !!}
		                {!! Form::text('complemento', null, array('class' => 'form-control')) !!}
		                <span class="help-block">{{ $errors->first('complemento', ':message') }}</span>
			        </div>
		    	</div>

		        
			</div>

				<!-- fim dos campos -->
	          </div><!-- /.box-body -->
	          <div class="box-footer">
	          	
				<button type="reset" class="btn btn-sm btn-default">
					<span class="glyphicon glyphicon-remove-circle"></span> 
					Limpar
				</button>

	            <button type="submit" class="btn btn-success pull-right">
					<span class="glyphicon glyphicon-ok-circle"></span> 
					    @if	(isset($cliente))
					        Editar
					    @else
					        Criar
					    @endif
				</button>
	          </div><!-- /.box-footer -->
	        </form>
	      </div><!-- /.box -->

@stop
@section('scripts')

<script>
	$(document).ready(function(){

		$('#cliente_os').typeahead({
         source: function(q, process) {
            objects = [];
            map = {};
            // requisição ajax
            

            return $.get("{{ URL::to('admin/cliente/dados/os') }}", 
                { 
                    query: q 
                }, function (data) {
                    objects = [];
                    $.each(data, function(i, object) {
                        var chave = object.nome + " " + object.nome_fantasia;
                        map[chave] = object;
                        objects.push(chave);
                    });
                    return process(objects);
                });
            // fim ajax
            
           
            process(objects);
        },
        updater: function(item) {
            
            $("#cliente_id").val(map[item].id);
            $("#cidade_id").val(map[item].cidade_id);
            $("#cidade").val(map[item].cidade + ' - ' + map[item].uf);
            $("#bairro").val(map[item].bairro);
            $("#cep").val(map[item].cep);
            $("#endereco").val(map[item].endereco);
            return item;
        }
    });
		
		@if(isset($cliente))

		var isFisica = {{ !empty($cliente->cpf) ? 'true' : 'false'}}

		if (isFisica) {
	    	$('.pessoa-juridica').addClass('hide');
	    	$('.pessoa-fisica').removeClass('hide');
	    }else {
	    	$('.pessoa-fisica').addClass('hide');
	    	$('.pessoa-juridica').removeClass('hide');

	    }	

		@endif
		
		


		$( "#tipo-pessoa" ).on('change', function(){
			valor = $( "#tipo-pessoa option:selected" ).val();		
			
		    if (valor == 'fisica') {
		    	$('.pessoa-fisica').toggleClass('hide');
		    	$('.pessoa-juridica').toggleClass('hide');
		    }else {
		    	$('.pessoa-juridica').toggleClass('hide');
		    	$('.pessoa-fisica').toggleClass('hide');

		    }	
		});

		
	});
</script>

@endsection