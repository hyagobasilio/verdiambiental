@extends('adminlte')
@section('content-header')


<!-- You can dynamically generate breadcrumbs here -->
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Cliente</a></li>
    <li class="active">Visualização</li>
</ol>
@endsection
{{-- Content --}}
@section('content')
    <div class="page-header">
        <h3>
            Clientes
            <div class="pull-right">
                <div class="pull-right">
                    <a href="{!! URL::to('admin/cliente/create') !!}"
                       class="btn btn-sm  btn-success iframe"><span
                                class="glyphicon glyphicon-plus-sign"></span>
                    Novo</a>
                </div>
            </div>
        </h3>
    </div>

    <div class="row">
        <form action="">
            <div class="col-sm-2">
                <div class="form-group {{ $errors->has('nome') ? 'has-error' : '' }}">
                    {!! Form::label('nome_search','Cliente', array('class' => 'control-label')) !!}
                    {!! Form::text('nome', Input::get('nome'), array('class' => 'form-control input-sm')) !!}
                    
                    <span class="help-block">{{ $errors->first('nome', ':message') }}</span>
                    <button type="submit" class="btn btn-sm btn-success">Pesquisar</button>
                </div>
            </div>
        </form>
    </div>

    <table id="table" class="table table-striped table-hover">
        <thead>
            <tr>
                <th>Cod.</th>
                <th>Nome</th>
                <th>Plano</th>
                <th>CPF/CNPJ</th>
                <th>Email</th>
                <th>Telefone 1</th>
                <th>Telefone 2</th>                
                <th>#</th>
            </tr>
        </thead>
        <tbody>
        @foreach($clientes as $cliente)
            <tr>
                <td>{{ $cliente->id }}</td>
                <td>{{ $cliente->nome . $cliente->nome_fantasia }}</td>
                <td>{{ $cliente->plano }}</td>
                <td>{{ $cliente->cpf . $cliente->cnpj }}</td>
                <td>{{ $cliente->email }}</td>
                <td>{{ $cliente->telefone1 }}</td>
                <td>{{ $cliente->telefone2 }}</td>
                <td>
                    <a href="{{{ URL::to('admin/cliente/' . $cliente->id . '/edit' ) }}}" class="btn btn-success btn-xs iframe" ><span class="glyphicon glyphicon-pencil"></span></a>
                    <a href="{{{ URL::to('admin/cliente/'.$cliente->id ) }}}" class="btn btn-xs btn-success iframe"><span class="glyphicon glyphicon-eye-open"></span></a>
                    <a href="{{{ URL::to('admin/cliente/' . $cliente->id . '/delete' ) }}}" class="btn btn-xs btn-success iframe"><span class="glyphicon glyphicon-trash"></span></a>
                    <a target="_blank" href="{{{ URL::to('admin/cliente/' . $cliente->id . '/pdf' ) }}}" class="btn btn-xs btn-success "><span class="fa fa-file-pdf-o"></span></a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <div class="box-tools pull-right">
        <?php echo $clientes->appends(Request::except('page'))->render(); ?>
    </div>
@endsection


{{-- Scripts --}}
@section('scripts')

<script>
    
    $(document).ready(function(){

        $(".iframe").colorbox({
            iframe: true,
            width: "80%",
            height: "80%"
        });

        $('#cidade').typeahead({
                 source: function(q, process) {
                    objects = [];
                    map = {};
                    // requisição ajax
                    

                    return $.get("{{ URL::to('admin/cidadeestado') }}", 
                        { 
                            query: q 
                        }, function (data) {
                            objects = [];
                            $.each(data, function(i, object) {
                                var chave = object.nome + " - " + object.uf;
                                map[chave] = object;
                                objects.push(chave);
                            });
                            return process(objects);
                        });
                    // fim ajax
                    
                   
                    process(objects);
                },
                updater: function(item) {
                    
                    $("#cidade_id").val(map[item].id);
                    return item;
                }
            });
    });
</script>
@parent

@endsection
