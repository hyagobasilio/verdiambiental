@extends('admin.layouts.modal2') @section('content')
{!! Form::model($cliente, array('url' => URL::to('admin/cliente') . '/' . $cliente->id, 'method' => 'delete', 'class' => 'bf', 'files'=> true)) !!}

	<input type="hidden" name="_token" value="{{{ csrf_token() }}}" /> <input
		type="hidden" name="id" value="{{ $cliente->id }}" />
	<div class="form-group">
		<div class="controls">
			<p>Tem certeza que deseja excluir o cliente, {{ $cliente->nome }}?</p>
			<element class="btn btn-warning btn-sm close_popup">
			<span class="glyphicon glyphicon-ban-circle"></span> Cancelar</element>
			<button type="submit" class="btn btn-sm btn-danger">
				<span class="glyphicon glyphicon-trash"></span> Deletar
			</button>
		</div>
	</div>
</form>
@stop
