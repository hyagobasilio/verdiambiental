{{-- @extends('admin.layouts.modal2') --}}
@section('styles')
<style>
	table tbody  tr td:first-child {
		font-weight: bold;
		width: 15%;
	}
</style>
@endsection
@section('content')


		<div class="box box-success">
	        <div class="box-header with-border">
	        	<img width="55" height="50" src="{{ asset('/assets/images/verdi-ambiental.png') }}" alt="Verdi Ambiental">
	          <h3 class="box-title">Cliente</h3>
	          <div class="pull-right" style="text-align:right">
	          	{{ date('d/m/Y') }} <br> {{ date('h:i:s') }}
	          </div>

	        </div><!-- /.box-header -->

          	<div class="box-body">

			<table class="table table-bordered table-condensed">
				<tbody>
					<tr>
						<td>Nome</td>
						<td>{{ $cliente->nome . $cliente->nome_fantasia}}</td>
					</tr>
					<tr>
						<td>Razao Social</td>
						<td>{{ $cliente->razao_social}}</td>
					</tr>
					<tr>
						<td>CPF/CNPJ</td>
						<td>{{ $cliente->cpf . $cliente->cnpj}}</td>
					</tr>
					<tr>
						<td>Tipo Plano</td>
						<td>{{ $cliente->tipo_plano }}</td>
					</tr>
					<tr>
						<td>Telefones</td>
						<td>{{ $cliente->telefone1 }} @if(!empty($cliente->telefone2)) {{ $cliente->telefone2}} @endif</td>
					</tr>
					<tr>
						<td>Email</td>
						<td>{{ $cliente->email }}</td>
					</tr>
					<tr>
						<td>Endereços</td>
						<td>
						@foreach($enderecos as $endereco)
						{{ $endereco->endereco . ' '. $endereco->numero. ', bairro: ' . $endereco->bairro }},  {{ $endereco->cidade->nome . ' - ' . $endereco->cidade->uf }} @if(!empty($endereco->cep)), <b>CEP:</b> {{ $endereco->cep }} @endif<br>
						@endforeach
						</td>
					</tr>
					<tr>
						<td>Observação</td>
						<td>{{ $cliente->observacao }}</td>
					</tr>
					<tr>
						<td>Contrato</td>
						<td>
						@if(count($contratos) == 0) Cliente sem contrato! @endif
						@foreach($contratos as $contrato)
							<a target="_blank" href="{{ url('admin/contrato/'.$contrato->id)}}">{{ $contrato->descricao }}</a> <br>
						@endforeach
						</td>
					</tr>
				</tbody>
			</table>
	            


          	</div><!-- /.box-body -->
          	<div class="box-footer">
          		

	            
          	</div><!-- /.box-footer -->
	        
	      </div><!-- /.box -->

@stop
{{-- Scripts --}}
@section('scripts')

<script>
    
    $(document).ready(function(){
        var oTable;
        oTable = $('#table').DataTable({
            "sDom": "<'row'<'col-md-6'l><'col-md-6'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
            "sPaginationType": "full_numbers",
            columns : [
                {data: 'id', name: 'ocorrencias.id'},
                {data: 'servico', name: 'ocorrencias.servico'},
                {data: 'tipo', name: 'ocorrencias.tipo'},
                {data: 'data', name: 'ocorrencias.data'},
                {data: 'cliente', name: 'ocorrencias.cliente'},
                {data: 'valor_estimado', name: 'ocorrencias.valor_estimado'},
                {data: 'valor_estimado', name: 'ocorrencias.valor_estimado'},
                {data: 'valor_final', name: 'ocorrencias.valor_final'},
                {data: 'valor_final', name: 'ocorrencias.valor_final'},
                {data: 'numero_container', name: 'ocorrencias.numero_container'},
                {data: 'numero_container', name: 'ocorrencias.numero_container'},
                {data: 'cidade', name: 'ocorrencias.cidade'}
            ],
            "oLanguage": {
                "sProcessing": "Processando",
                "sLengthMenu": "",
                "sZeroRecords": "Nenhum resultado",
                "sInfo": "{{ trans('table.show') }}",
                "sEmptyTable": "Tabela vazia",
                "sInfoEmpty": "Vazio",
                "sInfoFiltered": "Filtro",
                "sInfoPostFix": "",
                "sSearch": "Pesquisar:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Inicio",
                    "sPrevious": "Anterior",
                    "sNext": "Próximo",
                    "sLast": "Último"
                }
            },
            "processing": true,
            "serverSide": true,
            "ajax": "{!! url('admin/ocorrencia/data') !!}",
            "fnDrawCallback": function (oSettings) {
            
                

                $(".iframe").colorbox({
                    iframe: true,
                    width: "80%",
                    height: "80%",
                    onClosed: function () {
                        oTable.ajax.reload();
                    }
                });
                
            }
        });
    });
</script>
@parent

@endsection
