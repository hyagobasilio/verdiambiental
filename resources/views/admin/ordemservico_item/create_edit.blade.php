{{-- @extends('admin.layouts.modal2') --}}
@section('content-header')
<h1>
    Itemrota
    <small>{{ isset($item) ? "Editar" : "Cadastro" }}</small>
</h1>

<!-- You can dynamically generate breadcrumbs here -->
<ol class="breadcrumb">
    <li><a href="{{ url('admin/ordemservico') }}"><i class="fa fa-dashboard"></i> Itemrota</a></li>
    <li class="active">{{ isset($item) ? "Editar" : "Cadastro" }}</li>
</ol>
@endsection
@section('content')


		<div class="box box-success">
	        <div class="box-header with-border">
	          <h3 class="box-title">Cadastro de Rota</h3>
	        </div><!-- /.box-header -->
	        <!-- form start -->
	        @if (isset($item))
			{!! Form::model($item, array('url' => url('admin/ordemservicoitem') . '/' . $item->id, 'method' => 'put', 'class' => 'bf', 'files'=> true)) !!}
			@else
			{!! Form::open(array('url' => url('admin/ordemservicoitem'), 'method' => 'post', 'class' => 'bf', 'files'=> true)) !!}
			@endif
			<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
			@if (!isset($item))
			<input type="hidden" name="os_id" value="{{$idOS}}">
			@endif
	   

       <div class="box-body"><!-- início dos campos -->
		
		<div class="row">
			
			<!-- cliente -->
            <div class="col-sm-3">
                <div class=" form-group {{ $errors->has('cliente') ? 'has-error' : '' }}">
                <?php $clienteNome = isset($item) ? $item->cliente->nome . $item->cliente->nome_fantasia : null; ?>
                    {!! Form::label('cliente_id','Cliente', array('class' => 'control-label')) !!}

                    <span class="input-group">
                        {!! Form::text('cliente-os', $clienteNome, array('class' => 'form-control input-sm', 'autocomplete' => 'off', 'id' => 'cliente-os')) !!}
                        {!! Form::hidden('cliente_id') !!}
                        <span class="input-group-btn input-group-sm">  
                            <a href="{{url('/admin/cliente/create')}}" class=" input-sm iframe btn btn-success btn-flat">
                            <i class="glyphicon glyphicon-plus-sign"></i></a>
                        </span>
                        
                        <span class="help-block">{{ $errors->first('cliente', ':message') }}</span>
                    </span>
                </div>
            </div>
            
        @if(!isset($item))
            @if(Input::get('servico') == 'entulho' && Input::get('tipo') != 'entrega' || 
            Input::get('servico') == 'lixo' && Input::get('tipo') != 'remocao')
            <!-- Caixa Remoção -->
            <div class="col-sm-3">
                <div class="form-group  {{ $errors->has('remove_cx_id') ? 'has-error' : '' }}">
                    {!! Form::label('remove_cx_id', 'Caixa Remoção', array('class' => 'control-label')) !!}
                    
                    {!! Form::select('remove_cx_id', [], null, array('class' => 'form-control input-sm', 'id' => 'remove_cx_id')) !!}
                    
                    <span class="help-block">{{ $errors->first('remove_cx_id', ':message') }}</span>
                </div>
            </div>
            <!-- Tipo Entulho -->
            <div class="col-sm-3">
                <div class="form-group  {{ $errors->has('tipo_entulho') ? 'has-error' : '' }}">
                    {!! Form::label('tipo_entulho', 'Tipo Entulho', array('class' => 'control-label')) !!}
                    <?php $tipo_entulho = ['limpo' => 'Limpo', 'sujo' => 'Sujo']; ?>
                    {!! Form::select('tipo_entulho', $tipo_entulho, null, array('class' => 'form-control input-sm', 'id' => 'tipo_entulho')) !!}
                    
                    <span class="help-block">{{ $errors->first('tipo_entulho', ':message') }}</span>
                </div>
            </div>
            @endif
            
            @if(Input::get('tipo') == 'entrega' || 
            Input::get('servico') == 'entulho' && Input::get('tipo') == 'remocao')
			<!-- Numero Entrega -->
	        <div class="col-sm-3">
	        	<div class="form-group  {{ $errors->has('caixa_id') ? 'has-error' : '' }}">
	        	    {!! Form::label('caixa_id', 'Caixa Entrega', array('class' => 'control-label')) !!}
	    	        
                    {!! Form::select('caixa_id', [], null, array('class' => 'form-control input-sm', 'id' => 'caixa_id')) !!}
					<!-- {!! Form::hidden('caixa_id') !!} -->
	    	        <span class="help-block">{{ $errors->first('caixa_id', ':message') }}</span>
	        	</div>
	        </div>
            @endif
        @endif
            
        </div>
        <div class="row">
			<!-- Numero Cupom -->
	        <div class="col-sm-3">
	        	<div class="form-group  {{ $errors->has('numero_cupom') ? 'has-error' : '' }}">
	        	    {!! Form::label('numero_cupom', 'Número cupom', array('class' => 'control-label')) !!}
	    	        {!! Form::text('numero_cupom', null, array('class' => ' form-control')) !!}
	    	        <span class="help-block">{{ $errors->first('numero_cupom', ':message') }}</span>
	        	</div>
	        </div>

			<!-- Manifesto -->
	        <div class="col-sm-3">
	        	<div class="form-group  {{ $errors->has('manifesto') ? 'has-error' : '' }}">
	        	    {!! Form::label('manifesto', 'Manifesto', array('class' => 'control-label')) !!}
	    	        {!! Form::text('manifesto', null, array('class' => 'form-control')) !!}
	    	        <span class="help-block">{{ $errors->first('manifesto', ':message') }}</span>
	        	</div>
	        </div>

            <input type="hidden" value="{{ Input::get('tipo') }}" name="tipo">
			

	        
		</div>
        <div class="row">
            <!-- Endereços -->
            <div class="col-sm-12">
                <div class="form-group  {{ $errors->has('endereco_id') ? 'has-error' : '' }}">
                    {!! Form::label('endereco_id', 'Endereços', array('class' => 'control-label')) !!}
                    <select name="endereco_id" id="enderecos" class="form-control input-sm">
                        
                    </select>
                    <span class="help-block">{{ $errors->first('endereco_id', ':message') }}</span>
                </div>
            </div>
        </div>

		<!-- fim dos campos -->
      </div><!-- /.box-body -->
      <div class="box-footer">
      	
		

        <button type="submit" class="btn btn-success btn-sm pull-right">
			<span class="glyphicon glyphicon-ok-circle"></span> 
			    @if(isset($item))
			        Editar
			    @else
			        Salvar
			    @endif
		</button>
      </div><!-- /.box-footer -->
    </form>
	</div><!-- /.box -->

@stop

@section('scripts')
<script>

function atualizaCaixaEntrega()
{
    url = "{{ url('admin/container/disponiveis/')}}";
    $.ajax({
    dataType: "json",
    url: url,
    success: function(dados) {
        
            $.each( dados, function(index, value) {
                options  = {
                    value: value.id,
                    html: value.numero + ' - ' + value.tamanho
                };
                if(itemId == value.id) {
                    options.selected = 'selected'
                }
                $('<option>', options).appendTo('#caixa_id');
            });
        }
    });

}
function atualizaCaixaRemocao(idCliente)
{
    url = "{{ url('admin/container/locado/?')}}".replace('?', idCliente);
    $.ajax({
    dataType: "json",
    url: url,
    success: function(dados) {
        
            $.each( dados, function(index, value) {
                options  = {
                    value: value.id,
                    html: value.numero + ' - ' + value.tamanho
                };
                if(itemId == value.id) {
                    options.selected = 'selected'
                }
                $('<option>', options).appendTo('#remove_cx_id');
            });
        }
    });

}
var itemId = 0;
function populaItens(tipo, itemId)
  {
    $('#caixa_id').empty();
    var link = "{{ url('admin/container/?')}}".replace("?", itemId);
    
    $.ajax({
    dataType: "json",
    url: link,
    success: function(dados) {
        
            $.each( dados, function(index, value) {
                options  = {
                    value: value.id,
                    html: value.numero + ' - ' + value.tamanho
                };
                if(itemId == value.id) {
                    options.selected = 'selected'
                }
                $('<option>', options).appendTo('#caixa_id');
            });
        }
    });
  }
  function popularEnderecos()
  {
    clienteID = $('#cliente_id').val();
    enderecoID = <?php echo  isset($item) && !empty($item->endereco_id) ? $item->endereco_id : 'null'  ?>;

    $('#enderecos').empty();
    var link = "{{ url('admin/ordemservicoitem/endereco/cliente/?')}}".replace("?", clienteID);
    
    if(clienteID != '') {
        $.ajax({
            dataType: "json",
            url: link,
            success: function(dados) {
            
                $.each( dados, function(index, value) {
                    options  = {
                        value: index,
                        html: value
                    };
                    if(enderecoID != null && enderecoID == index) {
                        options.selected = 'selected'
                    }
                    $('<option>', options).appendTo('#enderecos');
                });
            }
        });
    }
  }

	$(function(){
        <?php 
            $tipo = $_GET['tipo'] == 'retirada' ? 'locado' : 'disponiveis';
         ?>
        var tipo = '{{ $tipo }}';
       
        

        popularEnderecos();
        


		$('#cliente-os').typeahead({
         source: function(q, process) {
            objects = [];
            map = {};
            // requisição ajax
            

            return $.get("{{ URL::to('admin/cliente/dados/os') }}", 
                { 
                    query: q 
                }, function (data) {
                    objects = [];
                    $.each(data, function(i, object) {
                        var chave = object.nome + " " + object.nome_fantasia;
                        map[chave] = object;
                        objects.push(chave);
                    });
                    return process(objects);
                });
            // fim ajax
            
           
            process(objects);
        },
        updater: function(item) {
            
            $("#cliente_id").val(map[item].id);
            atualizaCaixaRemocao(map[item].id);
            atualizaCaixaEntrega();
            popularEnderecos();
            return item;
        }
    });

		
	});
</script>
@endsection
