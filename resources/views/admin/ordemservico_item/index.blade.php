@extends('adminlte')
@section('content-header')


<!-- You can dynamically generate breadcrumbs here -->
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Cliente</a></li>
    <li class="active">Visualização</li>
</ol>
@endsection
{{-- Content --}}
@section('content')
    <div class="page-header">
        <h3>
            Clientes
            <div class="pull-right">
                <div class="pull-right">
                    <a href="{!! URL::to('admin/cliente/create') !!}"
                       class="btn btn-sm  btn-success iframe"><span
                                class="glyphicon glyphicon-plus-sign"></span>
                    Novo</a>
                </div>
            </div>
        </h3>
    </div>

    <table id="table" class="table table-striped table-hover">
        <thead>
            <tr>
                <!-- <th>Cod.</th> -->
                <th>Nome</th>
                <th>CPF</th>
                <th>CNPJ</th>
                <th>Fantasia</th>
                <th>Email</th>
                <th>Telefone 1</th>
                <th>Telefone 2</th>                
                <th>#</th>
            </tr>
        </thead>
        <tbody></tbody>
    </table>
@endsection

{{-- Scripts --}}
@section('scripts')

<script>
    
    $(document).ready(function(){
        $('#cidade').typeahead({
                 source: function(q, process) {
                    objects = [];
                    map = {};
                    // requisição ajax
                    

                    return $.get("{{ URL::to('admin/cidadeestado') }}", 
                        { 
                            query: q 
                        }, function (data) {
                            objects = [];
                            $.each(data, function(i, object) {
                                var chave = object.nome + " - " + object.uf;
                                map[chave] = object;
                                objects.push(chave);
                            });
                            return process(objects);
                        });
                    // fim ajax
                    
                   
                    process(objects);
                },
                updater: function(item) {
                    
                    $("#cidade_id").val(map[item].id);
                    return item;
                }
            });
    });
</script>
@parent

@endsection
