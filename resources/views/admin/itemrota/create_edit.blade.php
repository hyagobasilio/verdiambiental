{{-- @extends('admin.layouts.modal2') --}}
@section('content-header')
<h1>
    Itemrota
    <small>{{ isset($itemrota) ? "Editar" : "Cadastro" }}</small>
</h1>

<!-- You can dynamically generate breadcrumbs here -->
<ol class="breadcrumb">
    <li><a href="{{ URL::to('admin/materias') }}"><i class="fa fa-dashboard"></i> Itemrota</a></li>
    <li class="active">{{ isset($itemrota) ? "Editar" : "Cadastro" }}</li>
</ol>
@endsection
@section('content')


		<div class="box box-success">
	        <div class="box-header with-border">
	          <h3 class="box-title">Cadastro de Rota</h3>
	        </div><!-- /.box-header -->
	        <!-- form start -->
	        @if (isset($itemrota))
			{!! Form::model($itemrota, array('url' => URL::to('admin/itemrota') . '/' . $itemrota->id, 'method' => 'put', 'class' => 'bf', 'files'=> true)) !!}
			@else
			{!! Form::open(array('url' => URL::to('admin/itemrota'), 'method' => 'post', 'class' => 'bf', 'files'=> true)) !!}
			@endif
			<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
			@if (!isset($itemrota))
			<input type="hidden" name="ocorrencia_id" value="{{$idOS}}">
			@endif
	          <div class="box-body">

				<!-- início dos campos -->
		
		<div class="row">

			@if( strcasecmp(Input::get('servico'), 'entulho') == 0)
			<!-- Estado -->
            <div class="col-md-2">
                <?php $opcoes = ['' => 'Selecione', 'limpo' => 'Limpo', 'sujo' => 'Sujo']; ?>
                <div class="form-group {{ $errors->has('estado') ? 'has-error' : '' }}">
                    {!! Form::label('estado','Estado', array('class' => 'control-label')) !!}
                    {!! Form::select('estado', $opcoes, null, ['class' => 'form-control input-sm']) !!}
                    <span class="help-block">{{ $errors->first('estado', ':message') }}</span>          
                </div>
            </div>
			
			<!-- Tipo -->
            <div class="col-md-2">
                <?php $opcoes = ['' => 'Selecione', 'remocao' => 'Remoção', 'retirada' => 'Retirada', 'entrega' => 'Entrega']; ?>
                <div class="form-group {{ $errors->has('tipo') ? 'has-error' : '' }}">
                    {!! Form::label('tipo','Tipo', array('class' => 'control-label')) !!}
                    {!! Form::select('tipo', $opcoes, null, ['class' => 'form-control input-sm']) !!}
                    <span class="help-block">{{ $errors->first('tipo', ':message') }}</span>          
                </div>
            </div>
            @endif

			<!-- Data Rota -->
			<div class="col-md-2">
		        <div class="form-group  {{ $errors->has('data_rota') ? 'has-error' : '' }}">
		            {!! Form::label('data_rota', 'Data da rota', array('class' => 'control-label')) !!}
		            {!! Form::text('data_rota', null, array('class' => 'data form-control')) !!}
	                <span class="help-block">{{ $errors->first('data_rota', ':message') }}</span>
		        </div>
			</div>

			<!-- Numero Cupom -->
	        <div class="col-md-2">
	        	<div class="form-group  {{ $errors->has('numero_cupom') ? 'has-error' : '' }}">
	        	    {!! Form::label('numero_cupom', 'Número cupom', array('class' => 'control-label')) !!}
	    	        {!! Form::number('numero_cupom', null, array('class' => ' form-control')) !!}
	    	        <span class="help-block">{{ $errors->first('numero_cupom', ':message') }}</span>
	        	</div>
	        </div>

			<!-- Manifesto -->
	        <div class="col-md-2">
	        	<div class="form-group  {{ $errors->has('manifesto') ? 'has-error' : '' }}">
	        	    {!! Form::label('manifesto', 'Manifesto', array('class' => 'control-label')) !!}
	    	        {!! Form::text('manifesto', null, array('class' => 'form-control')) !!}
	    	        <span class="help-block">{{ $errors->first('manifesto', ':message') }}</span>
	        	</div>
	        </div>
			<!-- Número Ticket -->
	        <div class="col-md-3">
	        	<div class="form-group  {{ $errors->has('numero_ticket') ? 'has-error' : '' }}">
	        	    {!! Form::label('numero_ticket', 'Descarte', array('class' => 'control-label')) !!}
	    	        {!! Form::text('numero_ticket', null, array('class' => 'form-control')) !!}
	    	        <span class="help-block">{{ $errors->first('numero_ticket', ':message') }}</span>
	        	</div>
	        </div>

			<!-- Peso -->
	        <div class="col-md-3">
	        	<div class="form-group  {{ $errors->has('peso') ? 'has-error' : '' }}">
	        	    {!! Form::label('peso', 'Peso em kg', array('class' => 'control-label')) !!}
	    	        {!! Form::text('peso', null, array('class' => 'valor form-control')) !!}
	    	        <span class="help-block">{{ $errors->first('peso', ':message') }}</span>
	        	</div>
	        </div>

	        <!-- Valor Estimado -->
            <div class="col-sm-2">
                <div class="form-group {{ $errors->has('preco') ? 'has-error' : '' }}">
                    {!! Form::label('preco','Preço', array('class' => 'control-label')) !!}
                    {!! Form::text('preco', null, array('class' => 'valor form-control input-sm')) !!}
                    <span class="help-block">{{ $errors->first('preco', ':message') }}</span>
                </div>
            </div>

	        <!-- Multiple Radios (inline) -->
            <div class="form-group">
                <label class="col-md-2 control-label" for="radios">Status da Rota</label>
                <div class="col-md-4"> 
                    <label class="radio-inline" for="radios-0">
                      <input type="radio" name="status" id="radios-0" value="pendente" @if(!isset($itemrota) || (isset($itemrota) && $itemrota->status == 0))checked="checked"@endif>
                      Aberto
                    </label> 
                    <label class="radio-inline" for="radios-1">
                      <input type="radio" name="status" id="radios-1" value="concluida" @if( isset($itemrota) && $itemrota->status == 1) checked="checked"@endif >
                      Concluída
                    </label>
              </div>
            </div>

			<!-- Status -->
		</div>

		<!-- fim dos campos -->
      </div><!-- /.box-body -->
      <div class="box-footer">
      	
		

        <button type="submit" class="btn btn-success btn-sm pull-right">
			<span class="glyphicon glyphicon-ok-circle"></span> 
			    @if(isset($itemrota))
			        Editar
			    @else
			        Salvar
			    @endif
		</button>
      </div><!-- /.box-footer -->
    </form>
	</div><!-- /.box -->

@stop
