@extends('adminlte')
@section('content-header')
<h1>
    Container
    <small>visualização</small>
</h1>

<!-- You can dynamically generate breadcrumbs here -->
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Container</a></li>
    <li class="active">Visualização</li>
</ol>
@endsection
{{-- Content --}}
@section('content')
    
<br>
<div class="row">
    <div class="col-md-12">
        
        <div class="box box-primary">
            <div class="box-header ui-sortable-handle" style="cursor: move;">
                <!-- <i class="ion ion-clipboard"></i> -->

                <h3 class="box-title">Container</h3>
                <div class="pull-right">
                    <a href="{{{ URL::to('admin/container/create') }}}"
                        class="btn btn-sm  btn-success"><span
                            class="glyphicon glyphicon-plus-sign"></span> Novo Container</a>
                </div>

                

                
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                {!! Form::open( array('url' => url('admin/container'), 'method' => 'get', 'class' => 'bf')) !!}
                    <div class="row">
                        <!-- Cliente -->
                       <div class="col-sm-3">
                           <div class="form-group {{ $errors->has('numero') ? 'has-error' : '' }}">
                               {!! Form::label('numero','Nº Container', array('class' => 'control-label')) !!}
                               {!! Form::text('numero', Input::get('numero'), array('class' => 'form-control input-sm')) !!}
                               <span class="help-block">{{ $errors->first('numero', ':message') }}</span>
                           </div>
                       </div>
                      
                    </div>
                    <div class="row">
                        <div class="col-lg-3">
                
                            <button type="submit" class="btn btn-success btn-sm">
                                <span class="glyphicon glyphicon-search"></span> 
                                Pesquisar
                            </button>
                        </div>
                    </div>


                </form>

                <table class="table">
                    <thead>
                        <tr>
                            <th>Número do container</th>
                            <th>Tamanho</th>
                            <th>Status</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($containers as $container)
                        <tr>
                            <td>{{ $container->numero }}</td>
                            <td>{{ $container->tamanho }}</td>
                            <td>
                            @if(strcmp($container->status, 'disponivel') == 0)
                                <label class="label label-success">Disponível</label>
                            @elseif(strcmp($container->status, 'manutencao') == 0)
                                <label class="label label-danger">Manutenção</label>
                            @elseif(strcmp($container->status, 'locado') == 0)
                                <label class="label label-warning">Locado</label>
                            @endif
                            </td>
                            <td>
                                <a href="{{{ url('admin/container/' . $container->id . '/edit' ) }}}" class="btn btn-success btn-xs">
                                    <i class="fa fa-edit"></i>
                                    Editar
                                </a>
                            
                            <a href="{{{ url('admin/container/' . $container->id . '/delete' ) }}}" class="iframe btn btn-danger btn-xs">
                                <i class="fa fa-trash-o"></i>
                                Remover
                            </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix no-border">
                <div class="box-tools pull-right">
                    <?php echo $containers->appends(Request::except('page'))->render(); ?>
                </div>
                
            </div>
          </div>
    </div>
</div>
@endsection

{{-- Scripts --}}
@section('scripts')

<script>
    
    $(document).ready(function(){


        $(".iframe").colorbox({
            iframe: true,
            width: "80%",
            height: "80%"
        });
    });
</script>
   
@endsection
