{{-- @extends('adminlte') --}}

@section('content')


		<div class="box box-success">
	        <div class="box-header with-border">
	          <h3 class="box-title">Cadastro de Container</h3>
	        </div><!-- /.box-header -->
	        <!-- form start -->
	    
	      	@if (isset($container))
			{!! Form::model($container, array('url' => URL::to('admin/container/' . $container->id . '/edit'), 'method' => 'put', 'class' => 'form-horizontal bf', 'files'=> true)) !!}
			@else
			{!! Form::open(array('url' => URL::to('admin/container'), 'method' => 'post', 'class' => 'form-horizontal bf', 'files'=> true)) !!}
			@endif
			

			
	          <div class="box-body">


                <div class="form-group  {{ $errors->has('numero') ? 'has-error' : '' }}">
                 {!! Form::label('numero','Nº container', array('class' => 'col-sm-2 control-label')) !!}
                  <div class="col-sm-10">
                    {!! Form::text('numero', null, array('class' => 'form-control input-sm')) !!}
                     <span class="help-block">{{ $errors->first('numero', ':message') }}</span>
                  </div>
                </div>


				<!-- Status -->
                <?php $opcoes = ['disponivel' => 'Disponível','manutencao' => 'Manutenção', 'locado' => 'Locado']; ?>
                <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                    {!! Form::label('status','Status', array('class' => 'control-label col-sm-2')) !!}
                    <div class="col-sm-10">
                    	{!! Form::select('status', $opcoes, null, ['class' => 'form-control input-sm']) !!}
                    	<span class="help-block">{{ $errors->first('status', ':message') }}</span>          
                    </div>
                </div>
				<?php $opcoes = ['200 L' => '200 L', 
                '300 L' => '300 L', 
                '500 L' => '500 L', 
                '1.6 M3'=> '1.6 M3', 
                '5 M3'  => '5 M3', 
                '7 M3'  => '7 M3']; ?>
                <div class="form-group  {{ $errors->has('tamanho') ? 'has-error' : '' }}">
                 {!! Form::label('tamanho','Tamanho', array('class' => 'col-sm-2 control-label')) !!}
                  <div class="col-sm-10">
                  	{!! Form::select('tamanho', $opcoes, null, ['class' => 'form-control input-sm']) !!}
                    <span class="help-block">{{ $errors->first('tamanho', ':message') }}</span>
                  </div>
                </div>
                

	          	</div><!-- /.box-body -->
	          	<div class="box-footer">
	          		<a href="{{ url('/admin/container') }}" class="btn btn-sm btn-danger close_popup">
						<span class="glyphicon glyphicon-ban-circle"></span> 
					Cancelar
					</a>
					<button type="reset" class="btn btn-sm btn-default">
						<span class="glyphicon glyphicon-remove-circle"></span> 
						Limpar
					</button>

		            <button type="submit" class="btn btn-success pull-right">
						<span class="glyphicon glyphicon-ok-circle"></span> 
						    @if	(isset($container))
						        Alterar
						    @else
						        Salvar
						    @endif
					</button>
	          </div><!-- /.box-footer -->
	        </form>
	      </div><!-- /.box -->

@stop
