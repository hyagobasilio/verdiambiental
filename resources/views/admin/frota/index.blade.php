@extends('adminlte')
@section('content-header')
<h1>
    Frota
    <small>visualização</small>
</h1>

<!-- You can dynamically generate breadcrumbs here -->
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Frota</a></li>
    <li class="active">Visualização</li>
</ol>
@endsection
{{-- Content --}}
@section('content')
    
<br>
<div class="row">
    <div class="col-md-12">
        
        <div class="box box-primary">
            <div class="box-header ui-sortable-handle" style="cursor: move;">
                <!-- <i class="ion ion-clipboard"></i> -->

                <h3 class="box-title">Frota</h3>
                <div class="pull-right">
                    <a href="{{{ url('admin/frota/create') }}}"
                        class="btn btn-sm  btn-success"><span
                            class="glyphicon glyphicon-plus-sign"></span> Novo</a>
                </div>

                

                
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                {!! Form::open( array('url' => url('admin/frota'), 'method' => 'get', 'class' => 'bf')) !!}
                    <div class="row">
                        <!-- Caminhão -->
                       <div class="col-sm-3">
                           <div class="form-group {{ $errors->has('caminhao') ? 'has-error' : '' }}">
                               {!! Form::label('caminhao','Caminhão', array('class' => 'control-label')) !!}
                               {!! Form::text('caminhao', Input::get('caminhao'), array('class' => 'form-control input-sm')) !!}
                               <span class="help-block">{{ $errors->first('caminhao', ':message') }}</span>
                           </div>
                       </div> 
                       <!-- Placa -->
                       <div class="col-sm-3">
                           <div class="form-group {{ $errors->has('placa') ? 'has-error' : '' }}">
                               {!! Form::label('placa','Placa', array('class' => 'control-label')) !!}
                               {!! Form::text('placa', Input::get('placa'), array('class' => 'form-control input-sm')) !!}
                               <span class="help-block">{{ $errors->first('placa', ':message') }}</span>
                           </div>
                       </div>
                       <!-- Tipo -->
                        <?php $opcoes = ['compactador' => 'Compactador','poli' => 'Poli']; ?>
                       <div class="col-sm-3">
                           <div class="form-group {{ $errors->has('tipo') ? 'has-error' : '' }}">
                               {!! Form::label('tipo','Tipo', array('class' => 'control-label')) !!}
                              {!! Form::select('status', $opcoes, null, ['class' => 'form-control input-sm']) !!}
                               <span class="help-block">{{ $errors->first('tipo', ':message') }}</span>
                           </div>
                       </div>
                      
                    </div>
                    <div class="row">
                        <div class="col-lg-3">
                
                            <button type="submit" class="btn btn-success btn-sm">
                                <span class="glyphicon glyphicon-search"></span> 
                                Pesquisar
                            </button>
                        </div>
                    </div>


                </form>

                <table class="table">
                    <thead>
                        <tr>
                            <th>Caminhão</th>
                            <th>Placa</th>
                            <th>Tipo</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($frota as $caminhao)
                        <tr>
                            <td>{{ $caminhao->caminhao }}</td>
                            <td>{{ $caminhao->placa }}</td>
                            <td>
                            @if(strcmp($caminhao->tipo, 'compactador') == 0)
                                <label class="label label-success">Compactador</label>
                            @elseif(strcmp($caminhao->tipo, 'poli') == 0)
                                <label class="label label-success">Poli</label>
                            @endif
                            </td>
                            <td>
                                <a href="{{{ url('admin/frota/' . $caminhao->id . '/edit' ) }}}" class="btn btn-success btn-xs">
                                    <i class="fa fa-edit"></i>
                                    Editar
                                </a>
                            
                            <a href="{{{ url('admin/frota/' . $caminhao->id . '/delete' ) }}}" class="iframe btn btn-danger btn-xs">
                                <i class="fa fa-trash-o"></i>
                                Remover
                            </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix no-border">
                <div class="box-tools pull-right">
                    <?php echo $frota->appends(Request::except('page'))->render(); ?>
                </div>
                
            </div>
          </div>
    </div>
</div>
@endsection

{{-- Scripts --}}
@section('scripts')

<script>
    
    $(document).ready(function(){


        $(".iframe").colorbox({
            iframe: true,
            width: "80%",
            height: "80%"
        });
    });
</script>
   
@endsection
