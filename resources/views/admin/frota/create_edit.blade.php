{{-- @extends('adminlte') --}}

@section('content')


		<div class="box box-success">
	        <div class="box-header with-border">
	          <h3 class="box-title">Cadastro de Frota</h3>
	        </div><!-- /.box-header -->
	        <!-- form start -->
	    
	      	@if (isset($frota))
			{!! Form::model($frota, array('url' => URL::to('admin/frota/' . $frota->id . '/edit'), 'method' => 'put', 'class' => 'form-horizontal bf', 'files'=> true)) !!}
			@else
			{!! Form::open(array('url' => URL::to('admin/frota'), 'method' => 'post', 'class' => 'form-horizontal bf', 'files'=> true)) !!}
			@endif
			

			
	          <div class="box-body">


                <div class="form-group  {{ $errors->has('caminhao') ? 'has-error' : '' }}">
                 {!! Form::label('caminhao','Caminhão', array('class' => 'col-sm-2 control-label')) !!}
                  <div class="col-sm-10">
                    {!! Form::text('caminhao', null, array('class' => 'form-control input-sm')) !!}
                     <span class="help-block">{{ $errors->first('caminhao', ':message') }}</span>
                  </div>
                </div>

                <div class="form-group  {{ $errors->has('placa') ? 'has-error' : '' }}">
                 {!! Form::label('placa','Placa', array('class' => 'col-sm-2 control-label')) !!}
                  <div class="col-sm-10">
                    {!! Form::text('placa', null, array('class' => 'form-control input-sm', 'placeholder' => 'XXX-0000')) !!}
                     <span class="help-block">{{ $errors->first('placa', ':message') }}</span>
                  </div>
                </div>


				<!-- Material -->
                <?php $opcoes = ['compactador' => 'Compactador','poli' => 'Poli']; ?>
                <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                    {!! Form::label('tipo','Tipo', array('class' => 'control-label col-sm-2')) !!}
                    <div class="col-sm-10">
                    	{!! Form::select('tipo', $opcoes, null, ['class' => 'form-control input-sm']) !!}
                    	<span class="help-block">{{ $errors->first('tipo', ':message') }}</span>          
                    </div>
                </div>

                

	          	</div><!-- /.box-body -->
	          	<div class="box-footer">
	          		<a href="{{ url('/admin/frota') }}" class="btn btn-sm btn-danger close_popup">
						<span class="glyphicon glyphicon-ban-circle"></span> 
					Cancelar
					</a>
					<button type="reset" class="btn btn-sm btn-default">
						<span class="glyphicon glyphicon-remove-circle"></span> 
						Limpar
					</button>

		            <button type="submit" class="btn btn-success pull-right">
						<span class="glyphicon glyphicon-ok-circle"></span> 
						    @if	(isset($frota))
						        Alterar
						    @else
						        Salvar
						    @endif
					</button>
	          </div><!-- /.box-footer -->
	        </form>
	      </div><!-- /.box -->

@stop
