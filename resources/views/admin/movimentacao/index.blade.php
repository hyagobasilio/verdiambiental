@extends('adminlte')
@section('content-header')
<h1>
    Movimentação
    <small>visualização</small>
</h1>

<!-- You can dynamically generate breadcrumbs here -->
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Movimentação</a></li>
    <li class="active">Visualização</li>
</ol>
@endsection
{{-- Content --}}
@section('content')
    
<br>
<div class="row">
    <div class="col-md-12">
        
        <div class="box box-primary">
            <div class="box-header ui-sortable-handle" style="cursor: move;">
                <!-- <i class="ion ion-clipboard"></i> -->

                <h3 class="box-title">Movimentação</h3>
                <div class="pull-right">
                    <a href="{{{ url('admin/movimentacao/create') }}}"
                        class="btn btn-sm  btn-success iframe"><span
                            class="glyphicon glyphicon-plus-sign iframe"></span> Novo</a>
                </div>
                <form action="">

                <div class="row">
                <!-- Cliente -->
                <div class="col-sm-2">
                    <div class="form-group {{ $errors->has('categoria') ? 'has-error' : '' }}">
                        {!! Form::label('categoria','Categoria', array('class' => 'control-label')) !!}
                        {!! Form::select('categoria', $categorias, Input::get('categoria'), array('class' => 'form-control input-sm')) !!}
                        <span class="help-block">{{ $errors->first('categoria', ':message') }}</span>
                    </div>
                </div>
                <!-- Item -->
                <div class="col-sm-2">
                    <div class="form-group {{ $errors->has('item') ? 'has-error' : '' }}">
                        {!! Form::label('item','Item', array('class' => 'control-label')) !!}
                        {!! Form::select('item', $itens, Input::get('item'), array('class' => 'form-control input-sm')) !!}
                        <span class="help-block">{{ $errors->first('item', ':message') }}</span>
                    </div>
                </div>

                <!-- Tipo -->
                <div class="col-md-2">
                    <?php $opcoes = ['' => 'Selecione', 'E' => 'Entrada', 'S' => 'Saída']; ?>
                    <div class="form-group {{ $errors->has('tipo') ? 'has-error' : '' }}">
                        {!! Form::label('tipo','Tipo', array('class' => 'control-label')) !!}
                        {!! Form::select('tipo', $opcoes,  Input::get('tipo'), ['class' => 'form-control input-sm']) !!}
                        <span class="help-block">{{ $errors->first('tipo', ':message') }}</span>          
                    </div>
                </div>
                <!-- Inicio -->
                <div class="col-sm-2">
                    <div class="form-group {{ $errors->has('inicio') ? 'has-error' : '' }}">
                        {!! Form::label('inicio','Data Inicio', array('class' => 'control-label')) !!}
                        {!! Form::text('inicio', Input::get('inicio'), array('class' => 'form-control input-sm data')) !!}
                        <span class="help-block">{{ $errors->first('inicio', ':message') }}</span>
                    </div>
                </div>
                <!-- Fim -->
                <div class="col-sm-2">
                    <div class="form-group {{ $errors->has('fim') ? 'has-error' : '' }}">
                        {!! Form::label('fim','Data Fim', array('class' => 'control-label')) !!}
                        {!! Form::text('fim', Input::get('fim'), array('class' => 'form-control input-sm data')) !!}
                        <span class="help-block">{{ $errors->first('fim', ':message') }}</span>
                    </div>
                </div>

                
            </div>

            <div class="row"><div class="col-lg-3">
                
                    <button type="submit" class="btn btn-success btn-sm">
                        <span class="glyphicon glyphicon-search"></span> 
                            Pesquisar
                    </button>
            </div>

            </form> 

                

                
            </div>
            <!-- /.box-header -->
            <div class="box-body">

                <table class="table">
                    <thead>
                        <tr>
                            <th>Categoria</th>
                            <th>Item</th>
                            <th>Tipo Mov.</th>
                            <th>Valor</th>
                            <th>Data</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($movimentacoes as $movimentacao)
                        <tr>
                            <td>{{ $movimentacao->categoria }}</td>
                            <td>{{ $movimentacao->item }}</td>
                            <td>{{ $movimentacao->tipo_mov }}</td>
                            <td>{{ $movimentacao->valor }}</td>
                            <td>{{ $movimentacao->data_mov }}</td>
                            <td>
                            <a href="{{ url('admin/movimentacao/'.$movimentacao->id.'/edit') }}" class="btn btn-success btn-xs iframe cboxElement"><span class="glyphicon glyphicon-pencil"></span></a>
                            <a href="{{ url('admin/movimentacao/'.$movimentacao->id.'/delete') }}" class="btn btn-xs btn-success iframe cboxElement"><span class="glyphicon glyphicon-trash"></span></a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix no-border">
               
                
            </div>
          </div>
    </div>
</div>
@endsection


