{{-- @extends('admin.layouts.modal2') --}}

@section('content')


		<div class="box box-success">
	        <div class="box-header with-border">
	          <h3 class="box-title">Movimentação</h3>
	        </div><!-- /.box-header -->
	        <!-- form start -->
	    
	      	@if (isset($movimentacao))
			{!! Form::model($movimentacao, array('url' => url('admin/movimentacao/' . $movimentacao->id . '/edit'), 'method' => 'put', 'class' => 'form-horizontal bf', 'files'=> true)) !!}
			@else
			{!! Form::open(array('url' => url('admin/movimentacao'), 'method' => 'post', 'class' => 'form-horizontal bf', 'files'=> true)) !!}
			@endif
			

			
	          <div class="box-body">


                

                <div class="form-group  {{ $errors->has('categoria') ? 'has-error' : '' }}">
                 {!! Form::label('categoria','Categoria', array('class' => 'col-sm-2 control-label')) !!}
                  <div class="col-sm-10">
                    {!! Form::select('categoria', $categorias, null, array('class' => 'form-control input-sm')) !!}
                     <span class="help-block">{{ $errors->first('categoria', ':message') }}</span>
                  </div>
                </div>

                <div class="form-group  {{ $errors->has('item_id') ? 'has-error' : '' }}">
                 {!! Form::label('item_id','Item', array('class' => 'col-sm-2 control-label')) !!}
                  <div class="col-sm-10">
                    {!! Form::select('item_id', ['' => 'Selecione'], null, array('class' => 'form-control input-sm')) !!}
                     <span class="help-block">{{ $errors->first('item_id', ':message') }}</span>
                  </div>
                </div>

                <div class="form-group  {{ $errors->has('valor') ? 'has-error' : '' }}">
                 {!! Form::label('valor','Valor', array('class' => 'col-sm-2 control-label')) !!}
                  <div class="col-sm-10">
                    {!! Form::text('valor', null, array('class' => 'form-control input-sm valor')) !!}
                     <span class="help-block">{{ $errors->first('valor', ':message') }}</span>
                  </div>
                </div>
				<?php $tipomov = [''=> 'Selecione', 'E' => 'Entrada', 'S' => 'Saída']; ?>
                <div class="form-group  {{ $errors->has('tipo_mov') ? 'has-error' : '' }}">
                 {!! Form::label('tipo_mov','Tipo', array('class' => 'col-sm-2 control-label')) !!}
                  <div class="col-sm-10">
                    {!! Form::select('tipo_mov', $tipomov, null, array('class' => 'form-control input-sm tipo_mov')) !!}
                     <span class="help-block">{{ $errors->first('tipo_mov', ':message') }}</span>
                  </div>
                </div>

                <div class="form-group  {{ $errors->has('data_mov') ? 'has-error' : '' }}">
                 {!! Form::label('data_mov','Data Movimentação', array('class' => 'col-sm-2 control-label')) !!}
                  <div class="col-sm-10">
                    {!! Form::text('data_mov', null, array('class' => 'form-control input-sm data')) !!}
                     <span class="help-block">{{ $errors->first('data_mov', ':message') }}</span>
                  </div>
                </div>

	          	</div><!-- /.box-body -->
	          	<div class="box-footer">
	          		<a href="{{ url('/admin/cargo') }}" class="btn btn-sm btn-danger close_popup">
						<span class="glyphicon glyphicon-ban-circle"></span> 
					Cancelar
					</a>
					<button type="reset" class="btn btn-sm btn-default">
						<span class="glyphicon glyphicon-remove-circle"></span> 
						Limpar
					</button>

		            <button type="submit" class="btn btn-success pull-right">
						<span class="glyphicon glyphicon-ok-circle"></span> 
						    @if	(isset($cargo))
						        Alterar
						    @else
						        Salvar
						    @endif
					</button>
	          </div><!-- /.box-footer -->
	        </form>
	      </div><!-- /.box -->

@stop

@section('scripts')
<script>

  function populaItens(idCategoria, itemId)
  {
    $('#item_id').empty();
    var link = "{{ url('admin/itemmovimentacao/categoria/1')}}".replace("1", idCategoria);
                    
    $.ajax({
    dataType: "json",
    url: link,
    success: function(dados) {
      
        $('#item_id').attr('disabled', false)
            $.each( dados, function(index, value) {
                options  = {
                    value: value,
                    html: index
                };
                if(itemId == value) {
                    options.selected = 'selected'
                }

                $('<option>', options).appendTo('#item_id');
            });
        }
    });
  }

	$(function(){
    var itemId = '';
    var categoria = '';
    @if( isset($movimentacao->item->categoria_id) ) 
        categoria = {{ $movimentacao->item->categoria_id }} 
        itemId = {{ $movimentacao->item_id }}
    @endif

    if(categoria != '') {
        $('select#categoria  option[value="'+categoria+'"]').attr("selected", "selected")
        populaItens(categoria, itemId);
    }


		$("#categoria").on('change', function () {
                var idCategoria = this.value;
                

                if(idCategoria != "") {
                    populaItens(idCategoria);
                }

            });
	});
</script>
@endsection
