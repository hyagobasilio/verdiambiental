<?php 
	use App\Contrato;
 ?>
{{-- @extends('admin.layouts.modal2') --}}
@section('styles')
<style>
	table tbody  tr td:first-child {
		font-weight: bold;
		width: 15%;
	}
</style>
@endsection
@section('content')


		<div class="box box-success">
	        <div class="box-header with-border">
	        	<img width="55" height="50" src="{{ asset('/assets/images/verdi-ambiental.png') }}" alt="Verdi Ambiental">
	          <h3 class="box-title"></h3>
	          <div class="pull-right" style="text-align:right">
	          	{{ date('d/m/Y') }} <br> {{ date('h:i:s') }}
	          </div>

	        </div><!-- /.box-header -->

          	<div class="box-body">

			<table class="table table-bordered table-condensed">
				<tbody>
					<tr>
						<td>Nome</td>
						<td>{{ $funcionario->nome}} - {{ $funcionario->tipo }}</td>
					</tr>
					<tr>
						<td>Matricula</td>
						<td>{{ $funcionario->matricula }}</td>
					</tr>
					<tr>
						<td>Telefone</td>
						<td>{{ $funcionario->telefone }}</td>
					</tr>
					<tr>
						<td>CPF / RG</td>
						<td>{{ $funcionario->cpf }} @if(!empty($funcionario->rg)) / {{ $funcionario->rg }} @endif</td>
					</tr>
					<tr>
						<td>Email</td>
						<td>{{ $funcionario->email }}</td>
					</tr>
					<tr>
						<td>Data Admissão</td>
						<td>{{ $funcionario->data_admissao }}</td>
					</tr>
					<tr>
						<td>Nº CNH</td>
						<td>{{ $funcionario->numero_cnh }}</td>
					</tr>
					<tr>
						<td>Categoria CNH</td>
						<td>{{ $funcionario->categoria_cnh }}</td>
					</tr>
					<tr>
						<td>Validade</td>
						<td>{{ $funcionario->validade }}</td>
					</tr>
					
				</tbody>
			</table>
	            


          	</div><!-- /.box-body -->
          	<div class="box-footer">
          		

	          <!--   <a href="{{ url('admin/funcionario/'.$funcionario->id.'/pdf')}}" class="btn btn-sm btn-success pull-right">
	          					<span class="fa fa-file-pdf-o"></span> 
	          					   Download PDF
	          				</a> -->
          	</div><!-- /.box-footer -->
	        
	      </div><!-- /.box -->

@stop
