@extends('adminlte')
@section('content-header')
<h1>
    Funcionario
    <small>visualização</small>
</h1>

<!-- You can dynamically generate breadcrumbs here -->
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Funcionario</a></li>
    <li class="active">Visualização</li>
</ol>
@endsection
{{-- Content --}}
@section('content')
    
<br>
<div class="row">
    <div class="col-md-12">
        
        <div class="box box-primary">
            <div class="box-header ui-sortable-handle" style="cursor: move;">
                <!-- <i class="ion ion-clipboard"></i> -->

                <h3 class="box-title">Funcionarios</h3>
                <div class="pull-right">
                    <a href="{{{ url('admin/funcionario/create') }}}"
                        class="btn btn-sm  btn-success"><span
                            class="glyphicon glyphicon-plus-sign"></span> Novo Funcionario</a>
                </div>
                
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                {!! Form::open( array('url' => url('admin/funcionario'), 'method' => 'get', 'class' => 'bf')) !!}
                    <div class="row">
                    
                       <!-- Nome -->
                       <div class="col-sm-3">
                           <div class="form-group {{ $errors->has('nome') ? 'has-error' : '' }}">
                               {!! Form::label('nome','Nome', array('class' => 'control-label')) !!}
                               {!! Form::text('nome', Input::get('nome'), array('class' => 'form-control input-sm')) !!}
                               <span class="help-block">{{ $errors->first('nome', ':message') }}</span>
                           </div>
                       </div>
                       
                       <!-- CPF -->
                       <div class="col-sm-3">
                           <div class="form-group {{ $errors->has('cpf') ? 'has-error' : '' }}">
                               {!! Form::label('cpf','CPF', array('class' => 'control-label')) !!}
                               {!! Form::text('cpf', Input::get('cpf'), array('class' => 'form-control input-sm')) !!}
                               <span class="help-block">{{ $errors->first('cpf', ':message') }}</span>
                           </div>
                       </div>
                       <!-- Tipo -->
                       <?php $opcoes = ['' => 'Selecione', 'funcionario' => 'Motorista','ajudante' => 'Ajudante']; ?>
                       <div class="col-sm-3">
                           <div class="form-group {{ $errors->has('tipo') ? 'has-error' : '' }}">
                               {!! Form::label('tipo','Tipo', array('class' => 'control-label')) !!}
                               {!! Form::select('tipo', $cargos, null, ['class' => 'form-control input-sm']) !!}
                               <span class="help-block">{{ $errors->first('tipo', ':message') }}</span>
                           </div>
                       </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3">
                
                            <button type="submit" class="btn btn-success btn-sm">
                                <span class="glyphicon glyphicon-search"></span> 
                                Pesquisar
                            </button>
                        </div>
                    </div>


                </form>

                <table class="table">
                    <thead>
                        <tr>
                            <th>Cód.</th>
                            <th>matricula</th>
                            <th>Nome</th>
                            <th>Telefone</th>
                            <th>Cargo</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($funcionarios as $funcionario)
                        <tr>
                            <td>{{ $funcionario->id }}</td>
                            <td>{{ $funcionario->matricula }}</td>
                            <td>{{ $funcionario->nome }}</td>
                            <td>{{ $funcionario->telefone }}</td>
                            <td>@if(isset( $funcionario->cargo->descricao )){{ $funcionario->cargo->descricao }} @endif</td>
                            <td>
                                <a href="{{{ url('admin/funcionario/' . $funcionario->id ) }}}" class="iframe btn btn-success btn-xs">
                                    <i class="fa fa-eye"></i>
                                    Visualizar
                                </a>
                                <a href="{{{ url('admin/funcionario/' . $funcionario->id . '/edit' ) }}}" class="btn btn-success btn-xs">
                                    <i class="fa fa-edit"></i>
                                    Editar
                                </a>
                            
                            <a href="{{{ url('admin/funcionario/' . $funcionario->id . '/delete' ) }}}" class="iframe btn btn-danger btn-xs">
                                <i class="fa fa-trash-o"></i>
                                Remover
                            </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix no-border">
                <div class="box-tools pull-right">
                    <?php echo $funcionarios->appends(Request::except('page'))->render(); ?>
                </div>
                
            </div>
          </div>
    </div>
</div>
@endsection

{{-- Scripts --}}
@section('scripts')

<script>
    
    $(document).ready(function(){


        $(".iframe").colorbox({
            iframe: true,
            width: "80%",
            height: "80%"
        });
    });
</script>
   
@endsection
