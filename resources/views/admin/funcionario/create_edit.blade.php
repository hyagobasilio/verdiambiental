{{-- @extends('adminlte') --}}

@section('content')


		<div class="box box-success">
	        <div class="box-header with-border">
	          <h3 class="box-title">Cadastro de Funcionario</h3>
	        </div><!-- /.box-header -->
	        <!-- form start -->
	    
	      	@if (isset($funcionario))
			{!! Form::model($funcionario, array('url' => url('admin/funcionario/' . $funcionario->id . '/edit'), 'method' => 'put', 'class' => 'form-horizontal bf', 'files'=> true)) !!}
			@else
			{!! Form::open(array('url' => url('admin/funcionario'), 'method' => 'post', 'class' => 'form-horizontal bf', 'files'=> true)) !!}
			@endif
			

			
	          <div class="box-body">

                
                <div class="form-group  {{ $errors->has('nome') ? 'has-error' : '' }}">
                 {!! Form::label('nome','Nome', array('class' => 'col-sm-2 control-label')) !!}
                  <div class="col-sm-10">
                    {!! Form::text('nome', null, array('class' => 'form-control input-sm')) !!}
                     <span class="help-block">{{ $errors->first('nome', ':message') }}</span>
                  </div>
                </div>

                <!-- Tipo -->
                
                <div class="form-group {{ $errors->has('cargo_id') ? 'has-error' : '' }}">
                    {!! Form::label('cargo','Cargo', array('class' => 'control-label col-sm-2')) !!}
                    <div class="col-sm-10">
                        {!! Form::select('cargo_id', $cargos, null, ['class' => 'form-control input-sm']) !!}
                        <span class="help-block">{{ $errors->first('cargo_id', ':message') }}</span>          
                    </div>
                </div>

                <div class="form-group  {{ $errors->has('matricula') ? 'has-error' : '' }}">
                 {!! Form::label('matricula','Matricula', array('class' => 'col-sm-2 control-label')) !!}
                  <div class="col-sm-10">
                    {!! Form::text('matricula', null, array('class' => 'form-control input-sm')) !!}
                     <span class="help-block">{{ $errors->first('matricula', ':message') }}</span>
                  </div>
                </div>

                <div class="form-group  {{ $errors->has('telefone') ? 'has-error' : '' }}">
                 {!! Form::label('telefone','Telefone', array('class' => 'col-sm-2 control-label')) !!}
                  <div class="col-sm-10">
                    {!! Form::text('telefone', null, array('class' => 'telefone form-control input-sm')) !!}
                     <span class="help-block">{{ $errors->first('telefone', ':message') }}</span>
                  </div>
                </div>

                <div class="form-group  {{ $errors->has('email') ? 'has-error' : '' }}">
                 {!! Form::label('email','Email', array('class' => 'col-sm-2 control-label')) !!}
                  <div class="col-sm-10">
                    {!! Form::text('email', null, array('class' => 'form-control input-sm')) !!}
                     <span class="help-block">{{ $errors->first('email', ':message') }}</span>
                  </div>
                </div>

                <div class="form-group  {{ $errors->has('data_nascimento') ? 'has-error' : '' }}">
                 {!! Form::label('data_nascimento', 'Data nascimento', array('class' => 'col-sm-2 control-label')) !!}
                  <div class="col-sm-10">
                    {!! Form::text('data_nascimento', null, array('class' => 'data form-control input-sm')) !!}
                     <span class="help-block">{{ $errors->first('data_nascimento', ':message') }}</span>
                  </div>
                </div>

                <div class="form-group  {{ $errors->has('rg') ? 'has-error' : '' }}">
                 {!! Form::label('rg','RG', array('class' => 'col-sm-2 control-label')) !!}
                  <div class="col-sm-10">
                    {!! Form::text('rg', null, array('class' => 'form-control input-sm')) !!}
                     <span class="help-block">{{ $errors->first('rg', ':message') }}</span>
                  </div>
                </div>

                <div class="form-group  {{ $errors->has('cpf') ? 'has-error' : '' }}">
                 {!! Form::label('cpf','CPF', array('class' => 'col-sm-2 control-label')) !!}
                  <div class="col-sm-10">
                    {!! Form::text('cpf', null, array('class' => 'cpf form-control input-sm')) !!}
                     <span class="help-block">{{ $errors->first('cpf', ':message') }}</span>
                  </div>
                </div>

	            <div class="form-group  {{ $errors->has('data_admissao') ? 'has-error' : '' }}">
                 {!! Form::label('data_admissao','Data admissao', array('class' => 'col-sm-2 control-label')) !!}
                  <div class="col-sm-10">
                    {!! Form::text('data_admissao', null, array('class' => 'form-control input-sm data')) !!}
                     <span class="help-block">{{ $errors->first('data', ':message') }}</span>
                  </div>
                </div>

                <div class="form-group  {{ $errors->has('numero_cnh') ? 'has-error' : '' }}">
                 {!! Form::label('numero_cnh','Nº CNH', array('class' => 'col-sm-2 control-label')) !!}
                  <div class="col-sm-10">
                    {!! Form::text('numero_cnh', null, array('class' => 'form-control input-sm')) !!}
                     <span class="help-block">{{ $errors->first('numero_cnh', ':message') }}</span>
                  </div>
                </div>

                <div class="form-group  {{ $errors->has('validade') ? 'has-error' : '' }}">
                 {!! Form::label('validade','Validade', array('class' => 'col-sm-2 control-label')) !!}
                  <div class="col-sm-10">
                    {!! Form::text('validade', null, array('class' => 'form-control input-sm data')) !!}
                     <span class="help-block">{{ $errors->first('validade', ':message') }}</span>
                  </div>
                </div>

				


	          	</div><!-- /.box-body -->
	          	<div class="box-footer">
	          		<a href="{{ url('/admin/funcionario') }}" class="btn btn-sm btn-danger close_popup">
						<span class="glyphicon glyphicon-ban-circle"></span> 
					Cancelar
					</a>
					<button type="reset" class="btn btn-sm btn-default">
						<span class="glyphicon glyphicon-remove-circle"></span> 
						Limpar
					</button>

		            <button type="submit" class="btn btn-success pull-right">
						<span class="glyphicon glyphicon-ok-circle"></span> 
						    @if	(isset($funcionario))
						        Alterar
						    @else
						        Salvar
						    @endif
					</button>
	          </div><!-- /.box-footer -->
	        </form>
	      </div><!-- /.box -->

@stop

@section('scripts')
<script>
   
</script>
@endsection