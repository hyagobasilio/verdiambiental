<?php
$userName = "";
$avatarThumb = "";

if(Auth::check()){
    $avatarThumb = (isset(Auth::user()->filename) && !empty(Auth::user()->filename)) ? asset('uploads/users/fotos/thumbs/'.Auth::user()->filename ): asset("/images/user.png");
    $nome = explode(" ", Auth::user()->name);
    if (count($nome) > 1) {

        $userName = $nome[0]. " " . $nome[count($nome) -1];
    }else {
        $userName = $nome[0];
    }
}
?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
    <meta charset="UTF-8">
    <title>Verdi Ambiental</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="{{ asset("/bower_components/admin-lte/bootstrap/css/bootstrap.min.css") }}" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="{{ asset("/bower_components/admin-lte/dist/css/AdminLTE.min.css")}}" rel="stylesheet" type="text/css" />
<link href="{{ asset("assets/admin/css/colorbox.css")}}" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link href="{{ asset("/bower_components/admin-lte/dist/css/skins/skin-green.min.css")}}" rel="stylesheet" type="text/css" />

    <!-- Datatables -->
    <link rel="stylesheet" href="{{ asset('/plugins/datatables/dataTables.bootstrap.css') }}">
    
    <!-- DatePicker -->
    <link rel="stylesheet" href="{{ asset('/plugins/datepicker/datepicker3.css') }}">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <style>
       /*  .box.box-info {
           border-top-color: #55D83B;
       }
       
       .btn-info{
           background-color: #188038;
           border-color: #11C147;
       }
       .btn-info:hover {
           background-color: #11C147;
           border-color: #188038;
       } */
       .pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover {
    z-index: 2;
    color: #fff;
    cursor: default;
    background-color: #00A65A;
    border-color: #3c763d;
}
    </style>
    <script>
    var publicPath = "{{ url('/') }}";
    </script>
    @yield('head')
</head>
<body class="skin-green">
<div class="wrapper">

    <!-- Header -->
    @include('header')
    <!-- modal -->   
    <div class="modal">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span></button>
            <h4 class="modal-title">Ordens de Seviço que vencem hoje!</h4>
          </div>
          <div class="modal-body">
            <table class="table">
                <thead>
                    <tr>
                        <th>Tipo</th>
                        <th>Cliente</th>
                        <th>Data</th>
                        <th>Prazo</th>
                    </tr>
                </thead>
                <tbody id="body-tb-modal">
                    
                </tbody>
            </table>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-success ok-entendi">Ok entendi!</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div> <!-- /.modal -->

    <!-- Sidebar -->
    @include('sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            @yield('content-header')
            
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Your Page Content Here -->
            @yield('content')
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    <!-- Footer -->
 @include('footer')

</div><!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 2.1.4 -->
<script src="{{ asset ("/bower_components/admin-lte/plugins/jQuery/jQuery-2.1.4.min.js") }}"></script>
<!-- Bootstrap 3.3.2 JS -->
<script src="{{ asset ("/bower_components/admin-lte/bootstrap/js/bootstrap.min.js") }}" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="{{ asset ("/bower_components/admin-lte/dist/js/app.min.js") }}" type="text/javascript"></script>
<!-- Datatable -->
<!-- <script src="{{asset('assets/admin/js/bootstrap-dataTables-paging.js') }}"></script>
<script src="{{asset('assets/admin/js/datatables.fnReloadAjax.js')}}"></script>
<script src="{{asset('assets/admin/js/modal.js')}}"></script> -->

<!-- DataTables -->
<script src="{{ asset('/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/admin/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/admin/js/jquery.colorbox.js') }}"></script>

<script src="{{asset('assets/admin/js/datatables.fnReloadAjax.js')}}"></script>
<script src="{{asset('assets/admin/js/modal.js')}}"></script>
<script src="{{ asset('js/bootstrap-typeahead.js') }}"></script>

<!-- InputMask -->
<script src="{{ asset('plugins/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ asset('plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script src="{{ asset('plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>

<!-- DatePicker -->
<script src="{{ asset('plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('bower_components/admin-lte/plugins/datepicker/locales/bootstrap-datepicker.pt-BR.js') }}"></script>
<!-- MaskMoney -->
<script src="{{ asset('js/jquery.maskMoney.min.js') }}"></script>

<script src="{{ asset('js/verdi.js') }}"></script>
<script src="{{ asset('js/verdi-modal.js') }}"></script>

<script>
$(function() {
    $('#cliente').typeahead({
         source: function(q, process) {
            objects = [];
            map = {};
            // requisição ajax
            

            return $.get("{{ url('admin/cliente/dados') }}", 
                { 
                    query: q 
                }, function (data) {
                    objects = [];
                    $.each(data, function(i, object) {
                        var chave = object.nome + " " + object.nome_fantasia;
                        map[chave] = object;
                        objects.push(chave);
                    });
                    return process(objects);
                });
            // fim ajax
            
           
            process(objects);
        },
        updater: function(item) {
            
            $("#cliente_id").val(map[item].id);
            $("#cidade_id").val(map[item].cidade_id);
            $("#cidade").val(map[item].cidade + ' - ' + map[item].uf);
            $("#bairro").val(map[item].bairro);
            $("#cep").val(map[item].cep);
            $("#endereco").val(map[item].endereco);
            return item;
        }
    });


    $('#cidade').typeahead({
         source: function(q, process) {
            objects = [];
            map = {};
            // requisição ajax
            

            return $.get("{{ url('admin/cidades') }}", 
                { 
                    query: q 
                }, function (data) {
                    objects = [];
                    $.each(data, function(i, object) {
                        var chave = object.nome + " - " + object.uf;
                        map[chave] = object;
                        objects.push(chave);
                    });
                    return process(objects);
                });
            // fim ajax
            
           
            process(objects);
        },
        updater: function(item) {
            
            $("#cidade_id").val(map[item].id);
            return item;
        }
    });

    $('#container').typeahead({
         source: function(q, process) {
            objects = [];
            map = {};
            // requisição ajax
            

            return $.get("{{ url('admin/ocorrencia/container') }}", 
                { 
                    query: q 
                }, function (data) {
                    objects = [];
                    $.each(data, function(i, object) {
                        var chave = object.numero + " - " + object.tamanho;
                        map[chave] = object;
                        objects.push(chave);
                    });
                    return process(objects);
                });
            // fim ajax
            
           
            process(objects);
        },
        updater: function(item) {
            
            $("#container_id").val(map[item].id);
            return item;
        }
    });
    @if(Auth::check() && isset($type))
    $.fn.colorbox.resize({width:"960px", height:"675px"});
    var oTable;
        oTable = $('#table').DataTable({
            "sDom": "<'row'<'col-md-6'l><'col-md-6'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
            "sPaginationType": "full_numbers",
            "oLanguage": {
                "sProcessing": "Processando",
                "sLengthMenu": "",
                "sZeroRecords": "Nenhum resultado",
                "sInfo": "",
                "sEmptyTable": "Tabela vazia",
                "sInfoEmpty": "Vazio",
                "sInfoFiltered": "Filtro",
                "sInfoPostFix": "",
                "sSearch": "Pesquisar:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Inicio",
                    "sPrevious": "Anterior",
                    "sNext": "Próximo",
                    "sLast": "Último"
                }
            },
            "processing": true,
            "serverSide": true,
            "ajax": "{{ url('admin/'.$type.'/data') }}",
            "fnDrawCallback": function (oSettings) {
            
                

                $(".iframe").colorbox({
                    iframe: true,
                    width: "90%",
                    height: "90%",
                    onClosed: function () {
                        oTable.ajax.reload();
                    }
                });
                
            }
        });
    $("#table").css("width","100%")

        @endif

        $(".iframe").colorbox({
            iframe: true,
            width: "90%",
            height: "90%"
        });

    });
    </script>
    
<!-- Scripts -->
@yield('scripts')
<!-- Optionally, you can add Slimscroll and FastClick plugins.
      Both of these plugins are recommended to enhance the
      user experience -->
</body>
</html>