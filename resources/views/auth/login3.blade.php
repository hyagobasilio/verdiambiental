]@extends('adminlte')
{{-- Content --}}
@section('content')
    <div class="row">
        <div class="page-header">
            <h2></h2>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            {{--<div class="col-md-8 col-md-offset-2">--}}
                {{--<div class="panel panel-default">--}}
                    {{--<div class="panel-heading">Login</div>--}}
                    {{--<div class="panel-body">--}}
                        
                        <div class="center">
                            
                            <img style="margin:0 auto; margin-top:-50px; padding-bottom:20px; display:block" src="{{ asset('/assets/images/verdi-ambiental.png') }}" alt="Verdi Ambiental">
                        </div>

                        <form class="form-horizontal" role="form" method="POST" action="{{ URL::to('/auth/login') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <div class="form-group">
                                <label class="col-md-4 control-label">E-Mail</label>

                                <div class="col-md-4">
                                    <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Senha</label>

                                <div class="col-md-4">
                                    <input type="password" class="form-control" name="password">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="remember"> Lembrar
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-success" style="margin-right: 15px;">
                                        Entrar
                                    </button>

                                   <!--  <a href="{{ URL::to('/password/email') }}">Recuperar senha</a> -->
                                </div>
                            </div>
                        </form>
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        </div>
    </div>
@endsection
