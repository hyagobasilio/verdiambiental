@extends('login')
{{-- Content --}}
@section('content')
<div class="login-box-body">
    <img height="70" width="70" style="display:block;margin: 0 auto" src="{{ asset('/assets/images/verdi-ambiental.png') }}" alt="Verdi Ambiental">
    <!-- <p class="login-box-msg">Verdi Ambiental</p> -->

    <form method="POST" action="{{ URL::to('/auth/login') }}">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <div class="form-group has-feedback">
            <input type="email" class="form-control" name="email" value="{{ old('email') }}">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        </div>
        
        <div class="form-group has-feedback">

            <input type="password" class="form-control" placeholder="Senha" name="password">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <div class="row">
            <div class="col-xs-8">
              <div class="checkbox icheck">
                <label>
                  <input type="checkbox" name="remember"> Lembrar
                </label>
              </div>
            </div><!-- /.col -->
            <div class="col-xs-4">
              <button type="submit" class="btn btn-primary btn-block btn-flat">Logar</button>
            </div><!-- /.col -->
        </div>
    </form>

        

       <!--  <a href="#">I forgot my password</a><br>
       <a href="register.html" class="text-center">Register a new membership</a> -->

</div><!-- /.login-box-body -->
@endsection
