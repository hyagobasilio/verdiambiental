<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        @if(Auth::check())
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ $avatarThumb }}" class="img-circle" alt="{{ $userName }}">
            </div>
            <div class="pull-left info">
                <p>{{ $userName  }}</p>
                <!-- <a href="#"><i class="fa fa-circle text-success"></i> Online</a> -->
            </div>
        </div>

        <!-- sidebar menu: : style can be found in sidebar.less -->


        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">MENU PRINCIPAL</li>
            <!-- Optionally, you can add icons to the links -->
            <li><a href="{{ url('admin/dashboard') }}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
            <li><a href="{{ url('admin/cargo') }}"><i class="fa fa-suitcase"></i> <span>Cargos</span></a></li>
            <li><a href="{{ url('admin/cliente') }}"><i class="fa fa-user"></i> <span>Clientes</span></a></li>
            <li><a href="{{ url('admin/container') }}"><i class="fa fa-cubes"></i> <span>Caixas</span></a></li>
            <li><a href="{{ url('admin/contrato') }}"><i class="fa fa-file-text"></i> <span>Contratos</span></a></li>
            <li><a href="{{ url('admin/frota') }}"><i class="fa fa-truck"></i> <span>Frota</span></a></li>
            <li><a href="{{ url('admin/funcionario') }}"><i class="fa fa-users"></i> <span>Funcionários</span></a></li>
            <li class="treeview">
                <a href="#"><i class="fa fa-folder"></i><span> Ordem de Serviço</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu ">
                    <li><a href="{{ url('admin/ordemservico/create') }}"><i class="fa fa-circle-o text-green"></i> Cadastrar</a></li>
                    <li><a href="{{ url('admin/ordemservico') }}"><i class="fa fa-circle-o text-yellow"></i> Visualizar</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                <i class="fa fa-edit"></i><span> Fluxo de caixa</span>
                <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu ">
                    <li><a href="{{ url('admin/categoriamovimentacao') }}"><i class="fa fa-circle-o text-yellow"></i> Categoria Movimentação</a></li>
                    <li><a href="{{ url('admin/itemmovimentacao') }}"><i class="fa fa-circle-o text-yellow"></i> Item Movimentação</a></li>
                    <li><a href="{{ url('admin/movimentacao') }}"><i class="fa fa-circle-o text-yellow"></i> Movimentação</a></li>
                </ul>
            </li>
            @if(Auth::user()->admin)
            <li><a href="{{ url('admin/user') }}"><i class="fa fa-user"></i> <span>Usuários</span></a></li>
            @endif
            <li class="header">MENU SISTEMA</li>
            <li><a href="{{ url('admin/solicitacao') }}"><i class="fa fa-user"></i> <span>Solicitações de T.I</span></a></li>
          
        </ul><!-- /.sidebar-menu -->
        @endif
    </section>
    <!-- /.sidebar -->
</aside>