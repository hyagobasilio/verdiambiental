<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        
    </div>
    <!-- Default to the left -->
    <strong>Copyright © <?php echo date('Y'); ?> <a href="http://timeinfo.com.br/" target="_blank">TimeInfo</a>.</strong>.
</footer>